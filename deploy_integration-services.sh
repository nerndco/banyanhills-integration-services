echo "======================================================================================================="
echo "==========================================Integration Services============================================"
echo "======================================================================================================="
echo "=============================  Shuting down running application   ====================================="
read -n1 -r -p "Press any key to continue..." key
curl -i  -X POST -H "Accept: application/json" -H "Content-Type: application/json" http://127.0.0.1:9101/shutdown


cd workspace
echo "============================   Pullng code from bitbucket ============================================="
#read  -p "Enter your bitbucket username:" username
#echo $username

sudo rm -rf  integration-services
git clone https://$1@bitbucket.org/banyanhills/integration-services.git
cd  integration-services

echo "=============================  Building  jar =========================================================="
mvn clean generate-sources -Dmaven.test.skip=true package spring-boot:repackage

echo "=============================  Backing up the last deployed jar ======================================="
mv ~/services/integration-services-1.0.jar ~/backup/integration-services-1.0.jar_`date --iso-8601=seconds`
mv ~/services/integration-services.log ~/backup/integration-services.log_`date --iso-8601=seconds`


echo "=============================  Copying jar   =========================================================="
mv target/integration-services-1.0.jar      ~/services/

echo "============================   Starting application...================================================="
cd ~/services
nohup sudo java -Xmx512m -Xss256k -Djava.util.Arrays.useLegacyMergeSort=true -DCAMPAIGN_DEFAULT_CONFIG=file:///root/config/application.properties  -Dspring.profiles.active=dev -Deureka.datacenter=cloud -jar integration-services-1.0.jar > /dev/null 2>&1 &
