/**
 * 
 */
package campaign.api.gateway.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Yashpal.Singh
 *
 */
@Entity
public class UserRole {

	@Id
	private String id;
	private String userId;
    @ManyToOne
    @JoinColumn(name = "role_id")
	private Role role;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}
