package campaign.api.gateway;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.banyanhills.security.CanopySecureRestTemplate;
import com.banyanhills.security.annotation.EnableCanopySecurity;

@EnableCanopySecurity
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
@EnableCaching
@EnableScheduling
@PropertySource("${CAMPAIGN_DEFAULT_CONFIG}")
public class CampaignApiGatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(CampaignApiGatewayApplication.class, args);
	}
	

	@Value("${canopy.security.service.auhtenticationTokenName:canopy-security-token}")
	String tokenName;
	
	@LoadBalanced
	@Bean
	CanopySecureRestTemplate restTemplate() {
		return new CanopySecureRestTemplate(tokenName);
	}
}
