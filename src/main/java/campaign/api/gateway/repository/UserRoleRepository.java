/**
 * 
 */
package campaign.api.gateway.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import campaign.api.gateway.domain.UserRole;

/**
 * @author Yashpal.Singh
 *
 */
public interface UserRoleRepository extends CrudRepository<UserRole, String>{

	public List<UserRole> findByUserId(String userId);
	
}
