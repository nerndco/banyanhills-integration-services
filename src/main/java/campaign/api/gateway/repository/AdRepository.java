package campaign.api.gateway.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import campaign.api.gateway.domain.AdDetail;

public interface AdRepository extends CrudRepository<AdDetail,String> {
	public List<AdDetail> findAll();
	public AdDetail findById(Long id);
	public AdDetail findAdByMediaId(String mediaId);
	public AdDetail findAdByWidgetId(String widgetId);
}
