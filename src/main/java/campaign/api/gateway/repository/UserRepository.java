/**
 * 
 */
package campaign.api.gateway.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import campaign.api.gateway.domain.User;

/**
 * @author Yashpal.Singh
 *
 */
public interface UserRepository extends CrudRepository<User, String>{

	public List<User> findByUsername(String username);
	
}
