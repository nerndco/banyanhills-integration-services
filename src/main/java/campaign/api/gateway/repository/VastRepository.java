package campaign.api.gateway.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import campaign.api.gateway.domain.VASTDetail;

public interface VastRepository extends CrudRepository<VASTDetail,String> {
	public List<VASTDetail> findAll();
	public VASTDetail findById(Long id);
	public VASTDetail findVASTByMediaId(String mediaId);
	public VASTDetail findVASTByWidgetId(String widgetId);
}
