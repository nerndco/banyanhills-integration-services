/**
 * 
 */
package campaign.api.gateway.repository;

import org.springframework.data.repository.CrudRepository;

import campaign.api.gateway.domain.Role;

/**
 * @author Yashpal.Singh
 *
 */
public interface RoleRepository extends CrudRepository<Role, String> {

}
