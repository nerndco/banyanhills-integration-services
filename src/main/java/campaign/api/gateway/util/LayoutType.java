package campaign.api.gateway.util;

public enum LayoutType {
	TEMPLATE("TEMPLATE"), 
	LAYOUT("LAYOUT");

	private String layoutType;

	LayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    public String getLayoutType() {
        return layoutType;
    }
    
    @Override
    public String toString() {
        return layoutType;
    }
}
