package campaign.api.gateway.util

import groovy.util.logging.Slf4j
import org.springframework.http.HttpStatus
import org.springframework.http.client.ClientHttpResponse
import org.springframework.web.client.ResponseErrorHandler

@Slf4j
class LoggingResponseErrorHandler implements ResponseErrorHandler {
    @Override
    boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        def series = clientHttpResponse.getStatusCode().series()
        return (HttpStatus.Series.CLIENT_ERROR.equals(series) || HttpStatus.Series.SERVER_ERROR.equals(series))
    }

    @Override
    void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
        log.error("Response error: ${clientHttpResponse.statusCode} ${clientHttpResponse.statusText}")
    }
}
