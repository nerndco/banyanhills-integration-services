/**
 * 
 */
package campaign.api.gateway.util;

public class ApplicationConstants {
	public static final String PROMOTION_CREATE = "http://promotion-services/promotions/create";
	public static final String PROMOTION_STATE_CREATE = "http://promotion-services/promotions/state/create";
	public static final String PROMOTION_CATEGORY_CREATE = "http://promotion-services/promotions/category/create";
	public static final String PROMOTION_UPDATE = "http://promotion-services/promotions/update";
	public static final String PROMOTION_LIKERS_UPDATE = "http://promotion-services/promotions/likers/update";
	public static final String PROMOTION_DELETE = "http://promotion-services/promotions/delete";
	public static final String PROMOTION_READ = "http://promotion-services/promotions/all";
	public static final String PROMOTION_BY_ID = "http://promotion-services/promotions/search?id=";
	public static final String PROMOTION_STATES = "http://promotion-services/promotions/states";
	public static final String PROMOTION_COMMENTS = "http://promotion-services/promotions/comments/create";
	public static final String PROMOTION_CATEGORIES = "http://promotion-services/promotions/promotionCategories";
	public static final String CAMPAIGN_CREATE = "http://campaign-services/campaign/create";
	public static final String CAMPAIGN_STATE_CREATE = "http://campaign-services/campaign/states/create";
	public static final String CAMPAIGN_TYPE_CREATE = "http://campaign-services/campaign/campaignCategories/create";
	public static final String CAMPAIGN_PROMOTION_CREATE = "http://campaign-services/campaign/promotions/create";
	public static final String CAMPAIGN_UPDATE = "http://campaign-services/campaign/update";
	public static final String CAMPAIGN_DELETE = "http://campaign-services/campaign/delete";
	public static final String CAMPAIGN_READ = "http://campaign-services/campaign/campaigns";
	public static final String CAMPAIGN_UPDATE_STATE = "http://campaign-services/campaign/updateState";

	public static final String TYPE_READ = "http://campaign-services/campaign/campaignCategories";
	public static final String STATE_READ = "http://campaign-services/campaign/states";
	public static final String SEGMENT_READ = "http://subscriber-services/subscriber/api/listAllSegment";
	public static final String SUBSCRIBER_READ = "http://subscriber-services/subscriber/api/findAllMachine";
	public static final String SUBSCRIBER_BY_NAME="http://subscriber-services/subscriber/api/findMachineByDisplayName";
	public static final String SUBSCRIBER_BY_ID="http://subscriber-services/subscriber/api/findSubscriberById";
	public static final String SUBSCRIBER_BY_LOCATION="http://subscriber-services/subscriber/api/findMachineByLatitudeAndLongitude";
	public static final String SUBSCRIBER_BY_LOCNAME ="http://subscriber-services/subscriber/api/findMachineByLatitudeAndLongitudeAndDisplayName";
	public static final String SEGMENT_CREATE = "http://subscriber-services/subscriber/api/createSegment";
	public static final String CHANNEL_READ = "http://distribution-services/distribution/api/listAllChannel";
	public static final String CONTENT_READ = "http://content-services/content/api/listAllContent";
	public static final String DISTRIBUTION_CREATE = "http://distribution-services/distribution/api/createDistributionInfo";
	public static final String DISTRIBUTION_RUN = "http://distribution-services/distribution/api/runDistributionInfoSchedule";
	public static final String DISTRIBUTION_RUN_SCHEDULE = "http://distribution-services/distribution/api/runSchedule";

	public static final String DISTRIBUTION_DELETE = "http://distribution-services/distribution/api/deleteDistributionInfo";
	public static final String CHANNEL_CREATE = "http://distribution-services/distribution/api/createChannel";
	public static final String DISTRIBUTION_UPDATE = "http://distribution-services/distribution/api/updateDistributionInfo";
//	public static final String DISTRIBUTION_INFO_SEARCH = "http://distribution-services/distribution/api/searchDistributionInfo";
	public static final String DISTRIBUTION_INFO_BY_CAMPAIGN_ID = "http://distribution-services/distribution/api/getDistributionInfoByCampaignId";

	public static final String DISTRIBUTION_READ = "http://distribution-services/distribution/api/listAllDistributionInfo";
	public static final String APPROVER_LIST = "http://54.71.91.74/offerservices/api/user/getapproversbyentity";
	public static final String ECLIPSE_LOGIN_DETAILS = "http:/54.71.91.74/offerservices/api/login";
	public static final String CAMPAIGN_LOCATION_READ = "http://location-services/mobilelocation/api/listCampaignByLocation";
	public static final String CAMPAIGN_DISPLAY_READ_BY_ID = "http://location-services/location/api/searchSubscriberLocation";
	public static final String CAMPAIGN_CREATE_LOC_DISPLAY = "http://location-services/location/api/createSubscriberLocation";
//	public static final String CAMPAIGN_SEARCH_BY_ID = "http://campaign-services/campaign/search";
	public static final String CAMPAIGN_GET_BY_STATEID = "http://campaign-services/campaign/findCampaignsByStateId";
	
	public static final String CAMPAIGN_GET_BY_ID = "http://campaign-services/campaign/getCampaign";
	public static final String CAMPAIGN_LAYOUTS = "http://campaign-services/campaign/getLayout";
	public static final String CAMPAIGN_LAYOUT_BY_ID = "http://campaign-services/campaign/layout";
	public static final String CAMPAIGN_UPDATE_LAYOUT = "http://campaign-services/campaign/layout/update";
	public static final String CAMPAIGN_UPDATE_WIDGET = "http://campaign-services/campaign/widget/update";
	public static final String CAMPAIGN_GET_WIDGET_BY_ID = "http://campaign-services/campaign/getWidget";
	public static final String CAMPAIGN_SAVE_XLF = "http://campaign-services/campaign/layout/xlf";
	public static final String CAMPAIGN_CREATE_LAYOUT = "http://campaign-services/campaign/layout/create";
	public static final String CAMPAIGN_UPDATE_MEDIA = "http://campaign-services/campaign/media/update";
	public static final String CAMPAIGN_CLONE_LAYOUT = "http://campaign-services/campaign/layout/clone";
	public static final String CAMPAIGN_CREATE_REGION = "http://campaign-services/campaign/region/create";
	public static final String CAMPAIGN_CREATE_WIDGET = "http://campaign-services/campaign/widget/create";
	public static final String CAMPAIGN_GET_LAYOUTS_BY_TYPE = "http://campaign-services/campaign/layout/findByType";
	public static final String CAMPAIGN_DELETE_WIDGET_BY_ID = "http://campaign-services/campaign/widget/deleteById";
	public static final String CAMPAIGN_SAVE_HTML = "http://campaign-services/campaign/layout/html";
	public static final String CAMPAIGN_DELETE_LAYOUT = "http://campaign-services/campaign/layout/delete";
	public static final String CAMPAIGN_SAVE_TEXT = "http://campaign-services/campaign/widget/text";
	public static final String CAMPAIGN_RESET_LAYOUT = "http://campaign-services/campaign/layout/reset";

	public static final String PROMOTIONS_SEARCH_BY_ID = "http://promotion-services/promotions/search";
	public static final String ECLIPSE_GET_OFFER_NOTES = "http://54.71.91.74/offerservices/api/campaign/getrespoffersnotes";
	public static final String ECLIPSE_GET_USER_PROFILE = "http://54.71.91.74/offerservices/api/user/getselfprofiledeatils";
	public static final String ECLIPSE_GET_LIKES_DISLIKES = "http://54.71.91.74/offerservices/api/offer/likedislike";
	public static final String ECLIPSE_GET_OFFERCOMMENTS = "http://54.71.91.74/offerservices/api/offer/likedislike";
	public static final String ECLIPSE_GET_RETAILER_DETAIL = "http://54.71.91.74/offerservices/api/retailer/getretailer";
	public static final String ECLIPSE_GET_BRANDS = "http://54.71.91.74/offerservices/api/retailer/getbrands";
	public static final String CONTENT_SEARCH = "http://content-services/content/api/searchContent";
	public static final String CONTENT_UPLOAD = "http://content-services/content/api/uploadFiles";
	public static final String CONTENT_STREAM = "http://content-services/content/api/streamFile";
	public static final String FOLLOW_UNFOLLOW = "http://subscriber-services/mobilesubscriber/api/createSubscriberRetailerFollowUnFollow";
	public static final String UPLOAD_FOLLOW_UNFOLLOW = "http://subscriber-services/mobilesubscriber/api/updateSubscriberRetailerFollowUnFollow";
	
	public static final String GET_FOLLOW_UNFOLLOW = "http://subscriber-services/mobilesubscriber/api/searchSubscriberRetailerFollowUnFollow";
	public static final String GET_FOLLOW_UNFOLLOW_BY_SUBSCRIBERID_AND_RETAILERID = "http://subscriber-services/mobilesubscriber/api/findSubscriberRetailerFollowUnFollowBySubscriberIdAndRetailerId";

    public static final String SUBSCRIBER_UPDATE_MACHINE ="http://subscriber-services/subscriber/api/updateMachine";
}
