package campaign.api.gateway.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

public class MultipartUtility {
	
	@SuppressWarnings("unused")
	private static final Log log = LogFactory.getLog(MultipartUtility.class);

	/* -------------------------------------------------- */
	
	// TODO - the code calling this is still expecting that the "fname" exists under "catalina.home" - change that
	//        code to work with the new convertFileToMultipart(File file) implementation
	@Deprecated
    public static MultipartFile convertFileToMultipart(String fname) {
        String rootPath = System.getProperty("catalina.home");

        Path path = Paths.get(rootPath + "/" + fname);
        String name = fname;
        String originalFileName = fname;
        String contentType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);
        return result;
    }
	
	public static MultipartFile convertFileToMultipart(File file) throws IOException {			
		byte[] content = Files.readAllBytes(file.toPath());
			
		return new MockMultipartFile(file.getName(), file.getName(), MediaType.APPLICATION_OCTET_STREAM_VALUE, content);
	}

}