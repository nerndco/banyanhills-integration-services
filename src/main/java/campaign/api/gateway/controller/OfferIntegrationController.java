/**
 * 
 *//*
package campaign.api.gateway.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import campaign.api.gateway.dto.Campaign;
import campaign.api.gateway.dto.Categories;
import campaign.api.gateway.dto.Promotion;
import campaign.api.gateway.dto.PromotionComments;
import campaign.api.gateway.dto.PromotionLikers;
import campaign.api.gateway.dto.PromotionMapping;
import campaign.api.gateway.dto.campaign.CampaignLocation;
import campaign.api.gateway.dto.campaign.Location;
import campaign.api.gateway.dto.content.Content;
import campaign.api.gateway.dto.mobile.Brand;
import campaign.api.gateway.dto.mobile.CommentOfferServiceRequest;
import campaign.api.gateway.dto.mobile.CommentOfferServiceResponse;
import campaign.api.gateway.dto.mobile.CommentedBy;
import campaign.api.gateway.dto.mobile.Comments;
import campaign.api.gateway.dto.mobile.LikeDisLikeResponse;
import campaign.api.gateway.dto.mobile.LikeDislikeMetadata;
import campaign.api.gateway.dto.mobile.LikeDislikeRequest;
import campaign.api.gateway.dto.mobile.Likers;
import campaign.api.gateway.dto.mobile.NotesRequest;
import campaign.api.gateway.dto.mobile.NotesResponse;
import campaign.api.gateway.dto.mobile.Offer;
import campaign.api.gateway.dto.mobile.OfferCommentsMetaData;
import campaign.api.gateway.dto.mobile.OfferMedia;
import campaign.api.gateway.dto.mobile.OfferMetaData;
import campaign.api.gateway.dto.mobile.OfferRequest;
import campaign.api.gateway.dto.mobile.OfferResponse;
import campaign.api.gateway.dto.mobile.OfferType;
import campaign.api.gateway.dto.mobile.OfferTypeSubTypeMap;
import campaign.api.gateway.dto.mobile.Point;
import campaign.api.gateway.dto.mobile.Retailer;
import campaign.api.gateway.dto.mobile.RetailerMetaData;
import campaign.api.gateway.dto.mobile.Status;
import campaign.api.gateway.dto.mobile.SubscriberRetailerFollowUnFollow;
import campaign.api.gateway.dto.mobile.UserDetailRequest;
import campaign.api.gateway.dto.mobile.UserDetailsResponse;
import campaign.api.gateway.service.CampaignIntegrationService;
import campaign.api.gateway.service.ContentIntegrationService;
import campaign.api.gateway.service.EclipseMediaIntegrationService;
import campaign.api.gateway.service.LocationIntegrationService;
import campaign.api.gateway.service.PromotionIntegrationService;
import campaign.api.gateway.service.SubscriberIntegrationService;

*//**
 * @author Kritika.Srivastava
 *
 *//*
@RestController
@RequestMapping("/mobile/offer")
public class OfferIntegrationController {

	@Autowired
	LocationIntegrationService locationService;

	@Autowired
	CampaignIntegrationService campaignService;

	@Autowired
	PromotionIntegrationService promotionService;

	@Autowired
	EclipseMediaIntegrationService eclipseMediaService;

	@Autowired
	SubscriberIntegrationService subscriberService;

	@Autowired
	ContentIntegrationService contentService;

	static final Logger logger = LoggerFactory
			.getLogger(OfferIntegrationController.class);

	@RequestMapping(value = "/getofferformobile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public OfferResponse getOffersById(@RequestBody OfferRequest req) {
		logger.info("get offers by Id mobile starts");
		OfferResponse offer = new OfferResponse();
		OfferMetaData offerMetaData = new OfferMetaData();
		offer.setMetaData(offerMetaData);

		Long promotionId = Long.parseLong(req.getId());

		if (promotionId != null) {
			logger.info("get offers by Id " + promotionId);
			List<Promotion> promotionsList = promotionService
					.getPromotionById(promotionId);
			Offer promotions = new Offer();
			if (promotionsList.size() > 0) {

				offer.setResult(promotionsList.size());
				Promotion promotionData = promotionsList.get(0);

				if (promotionData.getLikers() != null
						&& promotionData.getLikers().size() > 0) {
					for (PromotionLikers likerData : promotionData.getLikers()) {
						if (likerData.getExternalId() != null
								&& likerData.getExternalId().toString()
										.equalsIgnoreCase(req.getUserId())) {
							if (likerData.getIsLiked()) {
								offerMetaData.setIsLiked(true);
							}
							break;
						}

					}

				}

				List<Content> contentList = contentService
						.getContentByPromotionId(promotionData.getId());
				List<OfferMedia> mediaLst = new ArrayList<OfferMedia>();

				if (contentList != null && contentList.size() > 0) {
					for (Content content : contentList) {
						OfferMedia offerMedia = new OfferMedia();
						offerMedia.setId(Long.parseLong(content.getId()));
						offerMedia.setMediaTypeId(content.getExtension());
						offerMedia.setMediaURL("In Progress");
						mediaLst.add(offerMedia);
					}
				}
				SubscriberRetailerFollowUnFollow subscriberRetailerFollowUnFollow = subscriberService.getFollowUnFollowBySubscriberIdAndRetailerId(Integer.parseInt(req.getUserId()), promotionData.getTenantId());
				if(subscriberRetailerFollowUnFollow != null) {
					promotions.setFnf(!subscriberRetailerFollowUnFollow.getUnSubscribed());
				}else {
					promotions.setFnf(false);
				}
				promotions.setOfferMedia(mediaLst);
				promotions.setName(promotionData.getName());
				promotions.setId(promotionData.getId());
				promotions.setDescription(promotionData.getDescription());
				promotions.setValidFrom(promotionData.getValidFrom());
				promotions.setValidTo(promotionData.getValidTo());
				promotions.setIsDeleted(promotionData.isDeleted());
				promotions.setCreatedByUserId(promotionData
						.getCreatedByUserId());
				promotions.setCreatedByUserName(promotions
						.getCreatedByUserName());
				Status status = new Status();
				status.setId(Long.parseLong(promotionData.getState().getId()
						.toString()));
				status.setName(promotionData.getState().getName());
				promotions.setStatus(status);

				for (Categories category : promotionData.getCategories()) {
					OfferType offerType = new OfferType();
					offerType.setOfferTypeId(category.getId());
					offerType.setOfferTypeName(category.getType());
					offerType.setOfferTypeDescription(category.getDetails());
					OfferTypeSubTypeMap offerTypeSubTypeMap = new OfferTypeSubTypeMap();
					offerTypeSubTypeMap.setOfferType(offerType);
					promotions.getOfferTypeSubTypeMap()
							.add(offerTypeSubTypeMap);
				}

				List<Comments> commentsList = new ArrayList<Comments>();

				if (promotionData.getPromotionComments() != null
						&& promotionData.getPromotionComments().size() > 0) {
					for (PromotionComments commentsData : promotionData
							.getPromotionComments()) {
						Comments comments = new Comments();
						comments.setId(commentsData.getId());
						comments.setText(commentsData.getText());
						comments.setCommentedDate(commentsData.getCommentDate());
						CommentedBy commentedBy = new CommentedBy();
						commentedBy.setFirstName(commentsData.getFirstName());
						commentedBy.setLastName(commentsData.getLastName());
						commentedBy.setTitle(commentsData.getTitle());
						comments.setCommentedBy(commentedBy);
						commentsList.add(comments);

					}

					offerMetaData.setComments(commentsList);
					offerMetaData.setCommentCount(commentsList.size());
				}

				RetailerMetaData retailerMetadata = eclipseMediaService
						.getRetailerDetails((long) promotionData.getTenantId());
				if (retailerMetadata != null) {
					if (retailerMetadata.getMetaData() != null) {
						Retailer retTmp = retailerMetadata.getMetaData();
						promotions.setRetailer(retTmp);
						campaign.api.gateway.dto.mobile.Location offerLoc = new campaign.api.gateway.dto.mobile.Location();
						offerLoc.setAddressLine1(retTmp.getAddressLine1());
						offerLoc.setAddressLine2(retTmp.getAddressLine2());
						offerLoc.setCity(retTmp.getCity());
						offerLoc.setCountryCode(retTmp.getCountryCode());
						offerLoc.setName(retTmp.getContactPerson());
						offerLoc.setPinCode(retTmp.getPinCode());
						offerLoc.setRadius(retTmp.getRadius());
						Point point = new Point();
						point.setY(28.6047605);
						point.setX(77.3696752);
						offerLoc.setPoint(point);
						List<campaign.api.gateway.dto.mobile.Location> locList = new ArrayList<campaign.api.gateway.dto.mobile.Location>();
						locList.add(offerLoc);
						promotions.setLocations(locList);
					}

				}
				if (promotionData.getLikers() != null
						&& promotionData.getLikers().size() > 0) {
					// offerMetaData.setIsLiked(true);
					Integer counter = 0;

					List<Likers> likers = new ArrayList<Likers>();
					for (PromotionLikers likerTemp : promotionData.getLikers()) {
						if (likerTemp.getIsLiked()) {
							counter++;
							Likers likerData = new Likers();
							likerData.setFirstName(likerTemp.getFirstName());
							likerData.setLastName(likerTemp.getLastName());
							likerData.setMiddleName(likerTemp.getMiddleName());
							likerData.setEmail(likerTemp.getEmail());
							likerData.setProfilePicUrl(likerTemp
									.getProfilePicUrl());
							likerData.setId(likerTemp.getId());
							likers.add(likerData);
						}

						offerMetaData.setLikers(likers);
					}
					promotions.setLikeCount(counter);
					if (counter > 0) {
						promotions.setIsLiked(true);

					}

				}

			}

			offerMetaData.setOffer(promotions);
		}
		logger.info("get offers by Id mobile ends");

		return offer;

	}

	@RequestMapping(value = "/campaign/getrespoffersnotes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public NotesResponse getOffersNotes(@RequestBody NotesRequest req,
			HttpServletRequest httpServletRequest) {
		logger.info("get offer notes starts");
		String authToken = httpServletRequest.getHeader("X-Auth-Token");

		logger.info("Auth token : " + authToken);

		Location location = new Location(Double.parseDouble(req.getLatitude()),
				Double.parseDouble(req.getLongitude()));
		logger.info("get eclipse media notes");

		NotesResponse notesResp = eclipseMediaService.getNotes(req, authToken);

		List<CampaignLocation> campaigns = locationService
				.getCampaignByLocation(location);

		for (CampaignLocation temp : campaigns) {

			if (temp.getCampaignRefId() != null) {
				logger.info("search campaign by id");

				List<Campaign> listOfCampaigns = campaignService
						.searchCampaign(temp.getCampaignRefId());
				System.out.println(listOfCampaigns);

				for (Campaign tempCampaign : listOfCampaigns) {
					if (tempCampaign.getPromotionMappings() != null
							&& tempCampaign.getPromotionMappings().size() > 0) {
						for (PromotionMapping offerings : tempCampaign
								.getPromotionMappings()) {
							Long promotionId = offerings.getPromotion_Id();

							if (promotionId != null) {
								List<Promotion> promotionsList = promotionService
										.getPromotionById(promotionId);
								Offer promotions = new Offer();

								if (promotionsList.size() > 0) {

									// offer.setResult(promotionsList.size());
									for (Promotion promTemp : promotionsList) {
										promotions.setName(promTemp.getName());
										promotions.setId(promTemp.getId());
										promotions.setDescription(promTemp
												.getDescription());
										promotions.setValidFrom(promTemp
												.getValidFrom());
										promotions.setValidTo(promTemp
												.getValidTo());
										promotions.setIsDeleted(promTemp
												.isDeleted());
										promotions.setCreatedByUserId(promTemp
												.getCreatedByUserId());
										promotions
												.setCreatedByUserName(promTemp
														.getCreatedByUserName());

										Status status = new Status();
										status.setId(Long.parseLong(promTemp
												.getState().getId().toString()));
										status.setName(promTemp.getState()
												.getName());
										promotions.setStatus(status);

										SubscriberRetailerFollowUnFollow subscriberRetailerFollowUnFollow = subscriberService.getFollowUnFollowBySubscriberIdAndRetailerId(Integer.parseInt(req.getUserId()), promTemp.getTenantId());
										if(subscriberRetailerFollowUnFollow != null) {
											promotions.setFnf(!subscriberRetailerFollowUnFollow.getUnSubscribed());
										}else{
											promotions.setFnf(false);
										}
											
										for (Categories category : promTemp
												.getCategories()) {
											OfferType offerType = new OfferType();
											offerType.setOfferTypeId(category
													.getId());
											offerType.setOfferTypeName(category
													.getType());
											offerType
													.setOfferTypeDescription(category
															.getDetails());
											OfferTypeSubTypeMap offerTypeSubTypeMap = new OfferTypeSubTypeMap();
											offerTypeSubTypeMap
													.setOfferType(offerType);
											promotions.getOfferTypeSubTypeMap()
													.add(offerTypeSubTypeMap);
										}

										
										 * if(promTemp.getPromotionComments() !=
										 * null &&
										 * promTemp.getPromotionComments(
										 * ).size() > 0) {
										 * promotions.setCommentCount
										 * (promTemp.getPromotionComments
										 * ().size()); }
										 
										logger.info("get retailer detail");

										RetailerMetaData retailerMetadata = eclipseMediaService
												.getRetailerDetails((long) promTemp
														.getTenantId());
										if (retailerMetadata != null) {
											if (retailerMetadata.getMetaData() != null) {
												Retailer retTmp = retailerMetadata
														.getMetaData();
												promotions.setRetailer(retTmp);
												campaign.api.gateway.dto.mobile.Location offerLoc = new campaign.api.gateway.dto.mobile.Location();
												offerLoc.setAddressLine1(retTmp
														.getAddressLine1());
												offerLoc.setAddressLine2(retTmp
														.getAddressLine2());
												offerLoc.setCity(retTmp
														.getCity());
												offerLoc.setCountryCode(retTmp
														.getCountryCode());
												offerLoc.setName(retTmp
														.getContactPerson());
												offerLoc.setPinCode(retTmp
														.getPinCode());
												offerLoc.setRadius(retTmp
														.getRadius());
												Point point = new Point();
												point.setY(28.6047605);
												point.setX(77.3696752);
												offerLoc.setPoint(point);
												List<campaign.api.gateway.dto.mobile.Location> locList = new ArrayList<campaign.api.gateway.dto.mobile.Location>();
												locList.add(offerLoc);
												promotions
														.setLocations(locList);
											}

										}

										Integer likeCount = 0;
										for (PromotionLikers promotionLikerData : promTemp
												.getLikers()) {
											if (promotionLikerData.getIsLiked())
												likeCount++;
										}
										promotions.setLikeCount(likeCount);
										if (promTemp.getLikers() != null
												&& promTemp.getLikers().size() > 0) {
											for (PromotionLikers likerData : promTemp
													.getLikers()) {
												if (likerData.getExternalId() != null
														&& likerData
																.getExternalId()
																.toString()
																.equalsIgnoreCase(
																		req.getUserId())) {
													if (likerData.getIsLiked()) {
														promotions
																.setIsLiked(true);
													}
												}

												break;
											}

										}

										notesResp.getMetaData().getOffers()
												.add(promotions);

									}

								}

							}

						}

					}
				}
			}

		}
		logger.info("get offers by Id mobile ends");

		return notesResp;
	}

	@RequestMapping(value = "/likedislike", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public LikeDisLikeResponse getLikesDislikes(
			@RequestBody LikeDislikeRequest request,
			HttpServletRequest httpServletRequest) {
		logger.info("like dislike mobile starts");

		String authToken = httpServletRequest.getHeader("X-Auth-Token");
		List<Promotion> promotionList = promotionService.getPromotionById(Long
				.parseLong(request.getOffer()));
		LikeDisLikeResponse response = null;
		if (promotionList != null && promotionList.size() > 0) {

			UserDetailRequest req = new UserDetailRequest();
			req.setId(request.getUserId());
			logger.info("like dislike get user detail");

			UserDetailsResponse userDetailsResponse = eclipseMediaService
					.getUserDetails(req, authToken);

			Promotion promotionData = promotionList.get(0);
			PromotionLikers liker = new PromotionLikers();

			if (promotionData.getLikers() != null) {
				Integer count = 0;
				for (PromotionLikers likerData : promotionData.getLikers()) {
					if (request.getUserId().equalsIgnoreCase(
							likerData.getExternalId().toString())) {
						liker.setId(likerData.getId());
						liker.setIsLiked(Boolean.parseBoolean(request.getLike()));
						liker.setFirstName(userDetailsResponse.getMetaData()
								.getFirstName());
						liker.setLastName(userDetailsResponse.getMetaData()
								.getLastName());
						liker.setExternalId(likerData.getExternalId());
						logger.info("like dislike update liker");

						promotionService.updatePromotionLikers(liker);
						count++;
						break;

					}
				}

				if (count == 0) {
					liker.setIsLiked(Boolean.parseBoolean(request.getLike()));
					liker.setFirstName(userDetailsResponse.getMetaData()
							.getFirstName());
					liker.setLastName(userDetailsResponse.getMetaData()
							.getLastName());
					liker.setExternalId(Long.parseLong(request.getUserId()));
					promotionData.getLikers().add(liker);
					logger.info("like dislike update promotion");

					promotionService.updatePromotion(promotionData);
				}
			} else {
				liker.setIsLiked(Boolean.parseBoolean(request.getLike()));
				liker.setFirstName(userDetailsResponse.getMetaData()
						.getFirstName());
				liker.setLastName(userDetailsResponse.getMetaData()
						.getLastName());
				liker.setExternalId(Long.parseLong(request.getUserId()));
				List<PromotionLikers> likerList = new ArrayList<PromotionLikers>();
				likerList.add(liker);
				promotionData.setLikers(likerList);
				promotionService.updatePromotion(promotionData);
			}

			LikeDislikeMetadata likeDislikeMetadata = new LikeDislikeMetadata();
			likeDislikeMetadata.setId(promotionData.getId());

			response = new LikeDisLikeResponse();

			if (request.getLike() != null
					&& request.getLike().equalsIgnoreCase("true")) {
				response.setMessage("");
				response.setMetaData(likeDislikeMetadata);
				response.setResult(new Long(1));
			} else {
				String message = "Offer " + request.getOffer()
						+ " disliked by " + request.getUserId();
				response.setMessage(message);
				response.setResult(new Long(1));
			}

		}
		logger.info("like dislike mobile ends");

		return response;

	}

	@RequestMapping(value = "/addoffercomment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CommentOfferServiceResponse addOfferComments(
			@RequestBody CommentOfferServiceRequest request,
			HttpServletRequest httpServletRequest) {
		logger.info("add offer comments starts");
		logger.info("search promotion by id");
		String authToken = httpServletRequest.getHeader("X-Auth-Token");
		List<Promotion> promotionList = promotionService.getPromotionById(Long
				.parseLong(request.getOffer()));
		OfferCommentsMetaData metaData = new OfferCommentsMetaData();
		CommentOfferServiceResponse response = new CommentOfferServiceResponse();
		if (promotionList != null && promotionList.size() > 0) {

			UserDetailRequest req = new UserDetailRequest();
			req.setId(request.getUserId());
			logger.info("get user detail");
			UserDetailsResponse userDetailsResponse = eclipseMediaService
					.getUserDetails(req, authToken);
			CommentedBy commentedBy = new CommentedBy();
			commentedBy.setId(userDetailsResponse.getMetaData().getId());
			commentedBy.setFirstName(userDetailsResponse.getMetaData()
					.getFirstName());
			commentedBy.setMiddleName(userDetailsResponse.getMetaData()
					.getMiddleName());
			commentedBy.setLastName(userDetailsResponse.getMetaData()
					.getLastName());
			commentedBy.setEmail(userDetailsResponse.getMetaData().getEmail());
			commentedBy.setProfilePicUrl(userDetailsResponse.getMetaData()
					.getProfilePicUrl());
			commentedBy.setFax(userDetailsResponse.getMetaData().getFax());
			metaData.setText(request.getText());
			metaData.setId(Long.parseLong(request.getOffer()));
			metaData.setCommentedBy(commentedBy);
			metaData.setCommentDate(new Date().toString());
			metaData.setCommentedBy(commentedBy);
			response.setResult((long) 1);

			Promotion promotionData = promotionList.get(0);
			PromotionComments comments = new PromotionComments();
			if (promotionData.getPromotionComments() != null
					&& promotionData.getPromotionComments().size() > 0) {
				Integer count = 0;
				for (PromotionComments commentsData : promotionData
						.getPromotionComments()) {
					if (commentsData.getExternalId() != null) {
						if (request.getUserId().equalsIgnoreCase(
								commentsData.getExternalId().toString())) {
							comments = commentsData;
							comments.setText(request.getText());
							logger.info("create comment");
							promotionService.createComment(comments);
							count++;
							break;

						}
					}

				}

				if (count == 0) {
					comments.setText(request.getText());
					comments.setFirstName(userDetailsResponse.getMetaData()
							.getFirstName());
					comments.setLastName(userDetailsResponse.getMetaData()
							.getLastName());
					comments.setEmail(userDetailsResponse.getMetaData()
							.getEmail());
					comments.setMiddleName(userDetailsResponse.getMetaData()
							.getMiddleName());
					comments.setCommentDate(new Date().toString());
					comments.setProfilePicUrl(userDetailsResponse.getMetaData()
							.getProfilePicUrl());
					comments.setExternalId(userDetailsResponse.getMetaData()
							.getId().toString());
					promotionData.getPromotionComments().add(comments);
					logger.info("update promotion 1");
					promotionService.updatePromotion(promotionData);
				}
			} else {
				comments.setText(request.getText());
				comments.setFirstName(userDetailsResponse.getMetaData()
						.getFirstName());
				comments.setLastName(userDetailsResponse.getMetaData()
						.getLastName());
				comments.setEmail(userDetailsResponse.getMetaData().getEmail());
				comments.setMiddleName(userDetailsResponse.getMetaData()
						.getMiddleName());
				comments.setCommentDate(new Date().toString());
				comments.setExternalId(userDetailsResponse.getMetaData()
						.getId().toString());
				comments.setProfilePicUrl(userDetailsResponse.getMetaData()
						.getProfilePicUrl());
				List<PromotionComments> commentsList = new ArrayList<PromotionComments>();
				commentsList.add(comments);
				promotionData.setPromotionComments(commentsList);
				logger.info("update promotion 2");
				promotionService.updatePromotion(promotionData);
			}
		}

		response.setMetaData(metaData);
		logger.info("add offer comments ends");
		return response;

	}

	@RequestMapping(value = "/fnf", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriberRetailerFollowUnFollow followUnfollow(
			@RequestBody SubscriberRetailerFollowUnFollow request,
			HttpServletRequest httpServletRequest) {
		logger.info("followUnfollow starts");
		SubscriberRetailerFollowUnFollow subscriberRetailerFollowUnFollow = null;
		try {
			subscriberRetailerFollowUnFollow = subscriberService.getFollowUnFollowBySubscriberIdAndRetailerId(request.getSubscriberId(), request.getRetailerId());
			if(subscriberRetailerFollowUnFollow == null){
				subscriberRetailerFollowUnFollow = subscriberService
					.createFollowUnFollow(request);
			}else {
				subscriberRetailerFollowUnFollow.setUnSubscribed(request.getUnSubscribed());
				subscriberRetailerFollowUnFollow = subscriberService
						.updateFollowUnFollow(subscriberRetailerFollowUnFollow);
			}

		} catch (Exception ex) {
			logger.info("Exception : " + ex.getMessage());
			ex.printStackTrace();
		}
		logger.info("followUnfollow ends");
		return subscriberRetailerFollowUnFollow;
	}

	@RequestMapping(value = "/getBrands", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Brand> getBrands(@RequestParam Integer userId,
			@RequestParam String timeStamp,
			HttpServletRequest httpServletRequest) {
		logger.info("getBrands starts");
		List<Brand> listBrands = null;
		try {
			listBrands = eclipseMediaService.getBrands(userId, timeStamp);

		} catch (Exception ex) {
			logger.info("Exception : " + ex.getMessage());
			ex.printStackTrace();
		}
		logger.info("getBrands ends");
		return listBrands;
	}
	
	@RequestMapping(value = "/getFnFBySubscriberIdAndRetailerId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriberRetailerFollowUnFollow getfollowUnFollowBySubscriberIdAndRetailerId(
			@RequestParam Integer subscriberId, @RequestParam Integer retailerId,
			HttpServletRequest httpServletRequest) {
		logger.info("getfollowUnFollowBySubscriberIdAndRetailerId starts");
		SubscriberRetailerFollowUnFollow subscriberRetailerFollowUnFollow = null;
		try{
			subscriberRetailerFollowUnFollow = subscriberService.getFollowUnFollowBySubscriberIdAndRetailerId(subscriberId, retailerId);

		} catch(Exception ex) {
			logger.info("Exception : " + ex.getMessage());
			ex.printStackTrace();
		}
		logger.info("getfollowUnFollowBySubscriberIdAndRetailerId ends");
		return subscriberRetailerFollowUnFollow;
	}
	
	@RequestMapping(value = "/getFnF", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<SubscriberRetailerFollowUnFollow> getfollowUnfollow(
			@RequestParam Integer subscriberId, @RequestParam Integer retailerId,
			HttpServletRequest httpServletRequest) {
		List<SubscriberRetailerFollowUnFollow> subscriberRetailerFollowUnFollow = null;
		try{
			subscriberRetailerFollowUnFollow = subscriberService.searchFollowUnFollow(retailerId, subscriberId);

		} catch(Exception ex) {
			logger.info("Exception : " + ex.getMessage());
			ex.printStackTrace();
		}
		return subscriberRetailerFollowUnFollow;
	}
}
*/