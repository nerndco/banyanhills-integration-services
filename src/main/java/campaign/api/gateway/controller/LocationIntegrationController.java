/**
 * 
 */
package campaign.api.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import campaign.api.gateway.dto.Machine;
import campaign.api.gateway.dto.location.SubscriberLocation;
import campaign.api.gateway.service.LocationIntegrationService;
import campaign.api.gateway.service.SubscriberIntegrationService;

/**
 * @author Kritika.Srivastava
 *
 */
@RestController
@RequestMapping("/locationService")
public class LocationIntegrationController {
	@Autowired
	LocationIntegrationService locationService;
	
	@Autowired
	SubscriberIntegrationService subscriberService;
	
	static final Logger logger = LoggerFactory.getLogger(LocationIntegrationController.class);
	
	@RequestMapping(value = "/getDisplaysByLocation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Machine> getDisplayByLocationId(HttpServletRequest httpServletRequest, @QueryParam("locationId") String locationId) {
		logger.debug("get displays by location : " + locationId);
		List<SubscriberLocation> displayList = null;
		List<Machine> machineList = new ArrayList<Machine>();

		displayList	 = locationService.getSubscriberByLocation(locationId);
		
		if(displayList != null && displayList.size() > 0) {
			for(SubscriberLocation display : displayList) {
				Machine machine = subscriberService.getSubscriberById(display.getSubscriberRefId());
				machineList.add(machine);
			}
		}
		
		
		return machineList;
	}
	
	@RequestMapping(value = "/createLocationDisplayMapping", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriberLocation createDisplayLocation(@RequestBody SubscriberLocation subscriberLocation, HttpServletRequest httpServletRequest) {
		SubscriberLocation response = null;
		
		response = locationService.createSubscriberByLocation(subscriberLocation);
		
		return response;
		
	}
}
