package campaign.api.gateway.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import campaign.api.gateway.dao.AdDaoImpl;
import campaign.api.gateway.dao.VastDaoImpl;
import campaign.api.gateway.domain.AdDetail;
import campaign.api.gateway.domain.VASTDetail;
import campaign.api.gateway.dto.AdProvider;
import campaign.api.gateway.dto.AdRequest;
import campaign.api.gateway.dto.ApproverData;
import campaign.api.gateway.dto.ApproverMapping;
import campaign.api.gateway.dto.Campaign;
import campaign.api.gateway.dto.CampaignMetadata;
import campaign.api.gateway.dto.CampaignResp;
import campaign.api.gateway.dto.Channel;
import campaign.api.gateway.dto.DeliverySegment;
import campaign.api.gateway.dto.DistributionInfo;
import campaign.api.gateway.dto.EventsMapping;
import campaign.api.gateway.dto.Machine;
import campaign.api.gateway.dto.SegmentParam;
import campaign.api.gateway.dto.State;
import campaign.api.gateway.dto.Text;
import campaign.api.gateway.dto.Type;
import campaign.api.gateway.dto.UserDetail;
import campaign.api.gateway.dto.campaign.CampaignResponse;
import campaign.api.gateway.dto.campaign.CreateCampaignRequest;
import campaign.api.gateway.dto.campaign.DeleteCampaignResponse;
import campaign.api.gateway.dto.content.Content;
import campaign.api.gateway.dto.content.FileInfo;
import campaign.api.gateway.dto.content.VASTRequest;
import campaign.api.gateway.dto.mobile.UserDetailRequest;
import campaign.api.gateway.dto.segment.Segment;
import campaign.api.gateway.dto.xibo.Layout;
import campaign.api.gateway.dto.xibo.ListEventRequest;
import campaign.api.gateway.dto.xibo.Media;
import campaign.api.gateway.dto.xibo.PlayList;
import campaign.api.gateway.dto.xibo.Regions;
import campaign.api.gateway.dto.xibo.UploadResponse;
import campaign.api.gateway.dto.xibo.Widget;
import campaign.api.gateway.dto.xibo.WidgetOption;
import campaign.api.gateway.dto.xibo.XiboSchedule;
import campaign.api.gateway.exception.CampaignManagementException;
import campaign.api.gateway.processor.ProcessAdRequest;
import campaign.api.gateway.processor.ProcessAdResponse;
import campaign.api.gateway.processor.openx.OpenXProcessor;
import campaign.api.gateway.processor.rubicon.RubiconProcessor;
import campaign.api.gateway.processor.springserve.SpringServeProcessor;
import campaign.api.gateway.service.CampaignCacheableService;
import campaign.api.gateway.service.CampaignIntegrationService;
import campaign.api.gateway.service.ContentIntegrationService;
import campaign.api.gateway.service.DistributionServiceIntegration;
import campaign.api.gateway.service.PromotionIntegrationService;
import campaign.api.gateway.service.SubscriberIntegrationService;
import campaign.api.gateway.service.XiboIntegrationService;
import campaign.api.gateway.util.CommonUtil;
import campaign.api.gateway.util.LayoutType;
import campaign.api.gateway.util.MultipartUtility;
import campaign.api.gateway.vast.VAST;
import campaign.api.gateway.vast.VAST.Ad.InLine.Creatives.Creative.Linear.TrackingEvents.Tracking;
import campaign.api.gateway.vast.VASTProcessor;

import com.banyanhills.security.User;

@RestController
@RequestMapping("/campaignService")
public class CampaignIntegrationController {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    CampaignCacheableService campaignCacheableService;

    @Autowired
    PromotionIntegrationService promotionService;

    @Autowired
    CampaignIntegrationService campaignService;

    @Autowired
    SubscriberIntegrationService subscriberService;

    @Autowired
    DistributionServiceIntegration distributionService;

    @Autowired
    XiboIntegrationService thirdPartyServices;

    @Autowired
    ContentIntegrationService contentService;

    @Autowired
    Environment environment;

    @Autowired
    VASTProcessor vastProcessor;

    @Autowired
    VastDaoImpl vastDaoImpl;

    @Autowired
    OpenXProcessor openXProcessor;

    @Autowired
    SpringServeProcessor springServeProcessor;

    @Autowired
    RubiconProcessor rubiconProcessor;

    @Autowired
    AdDaoImpl adDaoImpl;

    @Value("${campaign.content.stream.url}")
    private String contentStreamUrl;

    @Value("${campaign.ad.sendImpressionOnLoad}")
    private boolean sendImpressionOnLoad;

    static final Logger logger = LoggerFactory.getLogger(CampaignIntegrationController.class);
    
    /* -------------------------------------------------- */

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String campaignApiWelcome() {
        return "Welcome to Metadata Service";
    }

    @RequestMapping(value = "/fillContent", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String fillContent(@RequestBody VASTRequest vastRequest) throws Exception {
        String responseStr = "";

        if (CommonUtil.isNonEmptyAndNotNull(vastRequest.getDeviceId())
                && CommonUtil.isNonEmptyAndNotNull(vastRequest.getFromDate())
                && CommonUtil.isNonEmptyAndNotNull(vastRequest.getToDate())
                && CommonUtil.isNonEmptyAndNotNull(vastRequest.getLayoutId())) {
            logger.info("fillContent:" + vastRequest.getLayoutId());
            Map<String, Object> map = vastProcessor.processVastUrl(vastRequest.getVastTagURL());

            VAST vastObj = (VAST) map.get("vast");
            VASTDetail vast = new VASTDetail();
            vast.setAdId(vastObj.getAd().getId() != null ? vastObj.getAd().getId() : null);
            vast.setCreativeId(vastObj.getAd().getInLine().getCreatives()
                    .getCreative().getId() != null ? vastObj.getAd()
                    .getInLine().getCreatives().getCreative().getId() : null);
            vast.setOriginalMediaFileName(map.get("fileName") != null ? map.get("fileName").toString() : null);
            vast.setFromDate(vastRequest.getFromDate());
            vast.setToDate(vastRequest.getToDate());
            vast.setDisplayGroupId(vastRequest.getDeviceId());
            if (vastObj.getAd() != null) {
                List<Tracking> trackings = vastObj.getAd().getInLine()
                        .getCreatives().getCreative().getLinear()
                        .getTrackingEvents().getTracking();
                for (Tracking tracking : trackings) {
                    if (tracking.getEvent() != null && "complete".equals(tracking.getEvent())) {
                        vast.setCompleteUrl(tracking.getValue().getBytes());

                        break;
                    }
                }
            }

            try {
                Long widgetIdl = 0L;
                Long playListId = 0L;
                Layout layout = null;
                String token = thirdPartyServices.getAccessToken().getAccess_token();
                layout = thirdPartyServices.copyLayout(vastRequest.getLayoutId(), token);
                if (layout != null) {
                    vast.setLayoutId(String.valueOf(layout.getLayoutId()));
                    logger.info("layoutid:" + layout.getLayoutId());

                    Long campaignIdForLayout = layout.getCampaignId();
                    logger.info("campaignIdForLayout:" + campaignIdForLayout);

                    if (layout.getRegions() != null
                            && layout.getRegions().size() > 0
                            && layout.getRegions().get(0) != null
                            && layout.getRegions().get(0).getPlaylists() != null
                            && layout.getRegions().get(0).getPlaylists().size() > 0) {
                        playListId = layout.getRegions().get(0).getPlaylists().get(0).getPlaylistId();
                        logger.info("playListId:" + playListId);

                        List<Widget> widgets = layout.getRegions().get(0).getPlaylists().get(0).getWidgets();
                        if (widgets != null) {
                            for (Widget widget : widgets) {
                                widgetIdl = widget.getWidgetId();
                                thirdPartyServices.deleteMedia(widget.getWidgetId(), token); // delete widgets in layout
                            }
                        }
                    }

                    MultipartFile file = MultipartUtility.convertFileToMultipart(map.get("fileName").toString());
                    UploadResponse res = thirdPartyServices.uploadContent(file, playListId, token);

                    if (res != null && res.getFiles() != null
                            && res.getFiles().size() > 0
                            && res.getFiles().get(0) != null
                            && res.getFiles().get(0).getStoredas() != null) {
                        vast.setFileStoredAs(res.getFiles().get(0).getStoredas());
                        logger.info("storedas filename:" + res.getFiles().get(0).getStoredas());

                        String mediaId = "";
                        String[] tokens = res.getFiles().get(0).getStoredas().split("[.]");
                        if (tokens != null && tokens.length > 0) {
                            for (int i = 0; i < tokens.length - 1; i++) {
                                mediaId += tokens[i];
                            }

                            if (mediaId != null && mediaId != "") {
                                vast.setMediaId(mediaId);

                                logger.info("mediaId:" + mediaId);
                            }
                        }
                    }

                    if (res != null && res.getFiles() != null
                            && res.getFiles().get(0) != null
                            && res.getFiles().get(0).getError() != null
                            && !res.getFiles().get(0).getError().isEmpty()) {
                        List<Media> medias = thirdPartyServices.getMediaId(map.get("fileName").toString());
                        if (medias != null && medias.size() > 0) {
                            thirdPartyServices.assignMedia(playListId, medias.get(0).getMediaId(), widgetIdl, token);
                        }
                    }

                    if (campaignIdForLayout != null) {
                        XiboSchedule schedule = new XiboSchedule();
                        schedule.setFromDt(vastRequest.getFromDate());
                        schedule.setToDt(vastRequest.getToDate());
                        schedule.setFromDtLink(vastRequest.getFromDate());
                        schedule.setToDtLink(vastRequest.getToDate());
                        schedule.setDayPartId(1);
                        schedule.setEventTypeId("1");
                        schedule.setIsRecurring(false);
                        schedule.setIsPriority(1);
                        schedule.setDisplayOrder(1);
                        schedule.setCampaignId(campaignIdForLayout);

                        List<String> displays = new ArrayList<String>();
                        displays.add(vastRequest.getDeviceId());
                        schedule.setDisplayGroupIds(displays);
                        thirdPartyServices.scheduleLayout(schedule, token);

                        vastDaoImpl.save(vast);
                        responseStr = "success";
                    } else {
                        responseStr = "No layout found";
                    }
                } else {
                    responseStr = "No layout found";
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error in fillContent", e);
                responseStr = "error";
            }
        } else {
            responseStr = "Mandatory param missing in request";
        }

        return responseStr;
    }

    @RequestMapping(value = "/fillContent_timeline", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String fillContentTimeline(@RequestBody VASTRequest vastRequest) throws Exception {
        String responseStr = "";

        if (CommonUtil.isNonEmptyAndNotNull(vastRequest.getDeviceId())
                && CommonUtil.isNonEmptyAndNotNull(vastRequest.getLayoutId())) {
            logger.info("fillContent:" + vastRequest.getLayoutId());
            Map<String, Object> map = vastProcessor.processVastUrl(vastRequest.getVastTagURL());

            VAST vastObj = (VAST) map.get("vast");
            VASTDetail vast = new VASTDetail();
            vast.setAdId(vastObj.getAd().getId() != null ? vastObj.getAd().getId() : null);
            vast.setCreativeId(vastObj.getAd().getInLine().getCreatives()
                    .getCreative().getId() != null ? vastObj.getAd()
                    .getInLine().getCreatives().getCreative().getId() : null);
            vast.setOriginalMediaFileName(map.get("fileName") != null ? map.get("fileName").toString() : null);
            vast.setFromDate(vastRequest.getFromDate());
            vast.setToDate(vastRequest.getToDate());
            vast.setDisplayGroupId(vastRequest.getDeviceId());
            if (vastObj.getAd() != null) {
                List<Tracking> trackings = vastObj.getAd().getInLine()
                        .getCreatives().getCreative().getLinear()
                        .getTrackingEvents().getTracking();
                for (Tracking tracking : trackings) {
                    if (tracking.getEvent() != null && "complete".equals(tracking.getEvent())) {
                        vast.setCompleteUrl(tracking.getValue().getBytes());

                        break;
                    }
                }
            }

            try {
                Long widgetIdl = 0L;
                Long playListId = 0L;
                Layout layout = null;
                String token = thirdPartyServices.getAccessToken().getAccess_token();

                layout = thirdPartyServices.getLayout(vastRequest.getLayoutId(), token);
                if (layout != null) {
                    vast.setLayoutId(String.valueOf(layout.getLayoutId()));
                    logger.info("layoutid:" + layout.getLayoutId());

                    Long campaignIdForLayout = layout.getCampaignId();
                    logger.info("campaignIdForLayout:" + campaignIdForLayout);

                    if (layout.getRegions() != null
                            && layout.getRegions().size() > 0
                            && layout.getRegions().get(0) != null
                            && layout.getRegions().get(0).getPlaylists() != null
                            && layout.getRegions().get(0).getPlaylists().size() > 0) {
                        playListId = layout.getRegions().get(0).getPlaylists().get(0).getPlaylistId();
                        logger.info("playListId:" + playListId);

                        List<Widget> widgets = layout.getRegions().get(0).getPlaylists().get(0).getWidgets();
                        String oldMediaId = "";
                        if (widgets != null) {
                            for (Widget widget : widgets) {
                                widgetIdl = widget.getWidgetId();
                                if (widget.getMediaIds() != null && widget.getMediaIds().size() > 0) {
                                    oldMediaId = widget.getMediaIds().get(0);
                                }

                                if (widget != null) {
                                    List<WidgetOption> wOptions = widget.getWidgetOptions();
                                    if (wOptions != null) {
                                        for (WidgetOption option : wOptions) {
                                            if (option != null
                                                    && option.getOption() != null
                                                    && "name".equals(option.getOption())
                                                    && option.getValue() != null
                                                    && "emptySlot".equalsIgnoreCase(option.getValue())) {
                                                widgetIdl = option.getWidgetId();

                                                MultipartFile file = MultipartUtility.convertFileToMultipart(map.get("fileName").toString());
                                                UploadResponse res = thirdPartyServices.uploadContent(file, playListId, widgetIdl, oldMediaId, token);

                                                if (res != null
                                                        && res.getFiles() != null
                                                        && res.getFiles().size() > 0
                                                        && res.getFiles().get(0) != null
                                                        && res.getFiles().get(0).getStoredas() != null) {
                                                    vast.setFileStoredAs(res.getFiles().get(0).getStoredas());
                                                    logger.info("storedas filename:" + res.getFiles().get(0).getName());

                                                    String mediaId = "";
                                                    String[] tokens = res.getFiles().get(0).getStoredas().split("[.]");
                                                    if (tokens != null && tokens.length > 0) {
                                                        for (int i = 0; i < tokens.length - 1; i++) {
                                                            mediaId += tokens[i];
                                                        }
                                                        if (mediaId != null && mediaId != "") {
                                                            vast.setMediaId(mediaId);
                                                            logger.info("mediaId: " + mediaId);
                                                        }
                                                    }

                                                    thirdPartyServices.updateWidget(widgetIdl, res.getFiles().get(0).getName());
                                                }

                                                vastDaoImpl.save(vast);

                                                responseStr = "success";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    responseStr = "No layout found";
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error in fillContent", e);
                responseStr = "error";
            }
        } else {
            responseStr = "Mandatory param missing in request";
        }

        return responseStr;
    }

    @RequestMapping(value = "/completeEvent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String sendCompleteEvent(@RequestParam String mediaId) throws IOException, Exception {
        String responseStr = "";

        logger.info("sendCompleteEvent called for mediaId " + mediaId);

        if (mediaId != null) {
            VASTDetail vastDetail = vastDaoImpl.findByMediaId(mediaId);
            byte[] completeUrl = vastDetail != null ? vastDetail.getCompleteUrl() : null;
            if (vastDetail != null) {
                logger.info("Complete url " + completeUrl != null ? completeUrl.toString() : null);
                logger.info("OriginalMediaFileName " + vastDetail.getOriginalMediaFileName());

                byte[] completeUrlStream = vastDetail.getCompleteUrl();
                if (completeUrlStream != null) {
                    vastProcessor.sendGet(new String(completeUrlStream));
                    logger.info("sendCompleteEvent completed for mediaId " + mediaId);
                    responseStr = "success";
                }
            } else {
                responseStr = "No Complete URL found for mediaId " + mediaId;
            }
        } else {
            responseStr = "mediaId is null";
        }

        logger.info("response of sendCompleteEvent: " + responseStr);

        return responseStr;
    }

    @RequestMapping(value = "/adImpression", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String sendAdImpression(@RequestParam String mediaId) throws Exception {
        String responseStr = "";
        if (mediaId != null) {
            AdDetail adDetail = adDaoImpl.findByMediaId(mediaId);
            if (adDetail != null) {
                String[] impressionUrls = adDetail.getImpression().split("\\|");
                for (String impression : impressionUrls) {
                    logger.debug("Sending impression: " + impression);
                    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                        httpClient.execute(new HttpGet(impression));
                    } catch (Exception ex) {
                        logger.debug("problem calling impression url: " + impression);
                    }
                    logger.debug("impression completed for mediaId " + mediaId);
                    responseStr = "success";
                }
            } else {
                responseStr = "No impression URL found for mediaId " + mediaId;
            }
        } else {
            responseStr = "mediaId is null";
        }

        return responseStr;
    }

    @RequestMapping(value = "/campaign", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, List<Campaign>> getCampaigns(HttpServletRequest httpServletRequest) throws CampaignManagementException {
        logger.info("Get Campaigns starts");

        String userId = UserDetail.getUserId();
        UserDetailRequest request = new UserDetailRequest();
        request.setId(userId);
        Map<String, List<Campaign>> campaignMap = new HashMap<String, List<Campaign>>();

        try {
            Set<String> uniqueRoles = new TreeSet<String>();

            // getting roles from Canopy Security Services
            /*User user = (User) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();*/
//			for (SimpleGrantedAuthority grantedAuthority : user
//					.getAuthorities()) {
//				// uniqueRoles.add(grantedAuthority.getAuthority());
//			}
            uniqueRoles.add("approve campaign");
            logger.info(" Get Campaigns userId " + userId);
            //			List<State> states = campaignService.getState();
            //			Metadata metadata = new Metadata();

            for (String value : uniqueRoles) {
                if (value != null
                        && (value.equalsIgnoreCase("approve campaign") || value.equalsIgnoreCase("publish campaign"))) {


                    //					List<Campaign> publishedOrPausedStateCampaigns = campaignService.getAllCampaigns();
                    //					List<Campaign> expiredOrTerminatedOrCompletedStateCampaigns = campaignService.getAllCampaigns();

                    List<Campaign> myQueue = new ArrayList<Campaign>();
                    List<Campaign> others = new ArrayList<Campaign>();
                    List<Campaign> staged = new ArrayList<Campaign>();
                    List<Campaign> running = new ArrayList<Campaign>();
                    List<Campaign> completed = new ArrayList<Campaign>();

                    List<Campaign> allCampaigns = campaignService.getAllCampaigns();
                    List<Campaign> draftStateCampaigns = new ArrayList<Campaign>();
                    List<Campaign> reviewStateCampaigns = new ArrayList<Campaign>();
                    List<Campaign> approvedStateCampaigns = new ArrayList<Campaign>();

                    for (Campaign campaign : allCampaigns) {
                        if (campaign.getState() != null && campaign.getState().getName() != null
                                && "Draft".equalsIgnoreCase(campaign.getState().getName())) {
                            draftStateCampaigns.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && "In Review".equalsIgnoreCase(campaign.getState().getName())) {
                            reviewStateCampaigns.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && "Approved".equalsIgnoreCase(campaign.getState().getName())) {
                            approvedStateCampaigns.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && ("Published".equalsIgnoreCase(campaign.getState().getName()) || "Paused".equalsIgnoreCase(campaign.getState().getName()))) {
                            //							publishedOrPausedStateCampaigns.add(campaign);
                            running.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && ("Expired".equalsIgnoreCase(campaign.getState().getName())
                                || "Completed".equalsIgnoreCase(campaign.getState().getName())
                                || "Terminated".equalsIgnoreCase(campaign.getState().getName()))) {
                            //							expiredOrTerminatedOrCompletedStateCampaigns.add(campaign);
                            completed.add(campaign);

                        }
                    }

                    if (draftStateCampaigns != null) {
                        for (Campaign cmpTmp : draftStateCampaigns) {
                            if (cmpTmp.getCreatedByUserId() != null && userId.equalsIgnoreCase(cmpTmp.getCreatedByUserId())) {
                                myQueue.add(cmpTmp);
                            } else {
                                others.add(cmpTmp);
                            }
                        }
                    }

                    if (reviewStateCampaigns != null) {
                        for (Campaign cmpTmp : reviewStateCampaigns) {
                            if (cmpTmp.getApproverMappings() != null && cmpTmp.getApproverMappings().size() > 0) {
                                for (ApproverMapping approver : cmpTmp.getApproverMappings()) {
                                    if (approver != null && approver.getApproverId() != null && approver.getApproverId().toString().equalsIgnoreCase(userId)) {
                                        myQueue.add(cmpTmp);
                                    }
                                }
                            }
                        }
                    }

                    if (approvedStateCampaigns != null) {
                        for (Campaign cmpData : approvedStateCampaigns) {
                            if (cmpData.getEndDate() != null) {
                                Date date = new Date();
                                if (cmpData.getEndDate().before(date)) {
                                    campaignService.updateCampaignState("Expired", cmpData.getId());
                                    completed.add(cmpData);
                                } else {
                                    staged.add(cmpData);
                                }
                            }
                        }
                    }

                    campaignMap.put("MyQueue", myQueue);
                    campaignMap.put("Others", others);
                    campaignMap.put("Staged", staged);
                    campaignMap.put("Running", running);
                    campaignMap.put("Completed", completed);

                    break;
                } else if (value != null && value.equalsIgnoreCase("create campaign")) {
                    List<Campaign> myQueue = new ArrayList<Campaign>();
                    List<Campaign> others = new ArrayList<Campaign>();
                    List<Campaign> staged = new ArrayList<Campaign>();
                    List<Campaign> running = new ArrayList<Campaign>();
                    List<Campaign> completed = new ArrayList<Campaign>();

                    List<Campaign> allCampaigns = new ArrayList<Campaign>();
                    List<Campaign> draftStateCampaigns = new ArrayList<Campaign>();
                    List<Campaign> approvedStateCampaigns = new ArrayList<Campaign>();

                    for (Campaign campaign : allCampaigns) {
                        if (campaign.getState() != null && campaign.getState().getName() != null
                                && "Draft".equalsIgnoreCase(campaign.getState().getName())) {
                            draftStateCampaigns.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && "Approved".equalsIgnoreCase(campaign.getState().getName())) {
                            approvedStateCampaigns.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && ("Published".equalsIgnoreCase(campaign.getState().getName()) || "Paused".equalsIgnoreCase(campaign.getState().getName()))) {
                            running.add(campaign);
                        } else if (campaign.getState() != null && campaign.getState().getName() != null
                                && ("Expired".equalsIgnoreCase(campaign.getState().getName())
                                || "Completed".equalsIgnoreCase(campaign.getState().getName())
                                || "Terminated".equalsIgnoreCase(campaign.getState().getName()))) {
                            completed.add(campaign);
                        }
                    }

                    if (draftStateCampaigns != null) {
                        for (Campaign cmpTmp : draftStateCampaigns) {
                            if (cmpTmp.getCreatedByUserId() != null && userId.equalsIgnoreCase(cmpTmp.getCreatedByUserId())) {
                                myQueue.add(cmpTmp);
                            } else {
                                others.add(cmpTmp);
                            }
                        }
                    }

                    if (approvedStateCampaigns != null) {
                        for (Campaign cmpData : approvedStateCampaigns) {
                            if (cmpData.getEndDate() != null) {
                                Date date = new Date();
                                if (cmpData.getEndDate().before(date)) {
                                    campaignService.updateCampaignState("Expired", cmpData.getId());
                                    completed.add(cmpData);
                                } else {
                                    staged.add(cmpData);
                                }
                            }
                        }
                    }

                    campaignMap.put("MyQueue", myQueue);
                    campaignMap.put("Others", others);
                    campaignMap.put("Staged", staged);
                    campaignMap.put("Running", running);
                    campaignMap.put("Completed", completed);
                }
            }

            logger.info("Get Campaigns ends");
        } catch (Exception ex) {
            logger.error("Exception : ", ex);
            throw new CampaignManagementException("Error in getCampaigns:" + ex.getMessage());
        }

        return campaignMap;
    }

    @RequestMapping(value = "/getCampaignMetadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CampaignMetadata getCampaignMetadata() {
        CampaignMetadata campaignMetadata = new CampaignMetadata();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<State> states = campaignCacheableService.getState(user.getUsername());
        List<Segment> segments = campaignCacheableService.getAllSegements(user.getUsername());
        List<Channel> channels = campaignCacheableService.getAllChannels(user.getUsername());
        List<Layout> templates = campaignCacheableService.getLayoutsByType(LayoutType.TEMPLATE.toString(), user.getUsername());

        List<ApproverData> approverData = new ArrayList<ApproverData>();

        // TODO: Make it dynamic
        ApproverData data = new ApproverData();
        data.setId(42L);
        data.setFirstName("superuser");
        data.setUsername("superuser");
        approverData.add(data);

        if (channels != null && channels.size() > 0) {
            for (Channel channelData : channels) {
                if (channelData != null && channelData.getName() != null) {
                    if (channelData.getName().equalsIgnoreCase("digital signage")) {
                        channelData.setLayouts(templates);
                    }
                }
            }
        }

        campaignMetadata.setCampaignStates(states);
        // campaignMetadata.setCampaignTypes(types);
        campaignMetadata.setSegments(segments);
        campaignMetadata.setChannels(channels);
        campaignMetadata.setApproverData(approverData);
        logger.info("Get campaigns meta data ends");
        return campaignMetadata;
    }

    @RequestMapping(value = "/xibo/regions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Regions> getRegions(@QueryParam("layoutId") Long layoutId) throws CampaignManagementException {
        logger.info(" Get xibo regions start ");
        List<Regions> regions = thirdPartyServices.getRegions(layoutId)
                .getRegions();
        logger.info(" get xibo regions by layout id starts " + layoutId);
        String token = thirdPartyServices.getAccessToken().getAccess_token();
        for (Regions regionData : regions) {
            if (regionData.getPlaylists() != null && regionData.getPlaylists().size() > 0) {
                for (PlayList playList : regionData.getPlaylists()) {
                    if (playList.getWidgets() != null && playList.getWidgets().size() > 0) {
                        for (Widget widgetData : playList.getWidgets()) {
                            List<Media> mediaList = new ArrayList<Media>();
                            widgetData.setMedia(mediaList);
                            for (String mediaId : widgetData.getMediaIds()) {
                                if (mediaId != null) {
                                    logger.info(" get region media  " + mediaId);

                                    Media mediaData = thirdPartyServices.getMedia(Long.parseLong(mediaId));

                                    String contentUrl = environment.getProperty("xibo.api.media-content")
                                            + Long.parseLong(mediaId)
                                            + "?preview=1&access_token="
                                            + token;

                                    mediaData.setContent(contentUrl);
                                    mediaList.add(mediaData);
                                }
                            }
                        }
                    }
                }
            }
        }

        logger.info(" get xibo regions  ends ");

        return regions;
    }

    @RequestMapping(value = "/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public CampaignResponse create(@RequestBody CreateCampaignRequest entity, HttpServletRequest httpServletRequest) throws Exception {
        logger.info("create campaign starts");

        CampaignResponse campaignReponse = new CampaignResponse();
        Campaign respCampaign = null;
        try {
            if (entity.getCampaign() != null && entity.getCampaign().getId() == null) {

                entity.getCampaign().setCreatedByUserId(UserDetail.getUserId());
                entity.getCampaign().setCreatedByUserName(UserDetail.getUsername());

                respCampaign = campaignService.createCampaign(entity.getCampaign());

                if (entity.getDistributionInfo() != null && entity.getDistributionInfo().size() > 0) {
                    for (int i = 0; i < entity.getDistributionInfo().size(); i++) {
                        entity.getDistributionInfo().get(i).setCampaignId(respCampaign.getId());
                    }
                }
            } else {
                entity.getCampaign().setModifiedByUserId(UserDetail.getUserId());
                entity.getCampaign().setModifiedByUserName(UserDetail.getUsername());

                respCampaign = campaignService.updateCampaign(entity.getCampaign());
                if (entity.getDistributionInfo() != null && entity.getDistributionInfo().size() > 0) {
                    for (int i = 0; i < entity.getDistributionInfo().size(); i++) {
                        entity.getDistributionInfo().get(i).setCampaignId(respCampaign.getId());

                        List<String> layoutIds = entity.getDistributionInfo().get(i).getLayoutIds();
                        if (layoutIds != null && layoutIds.size() > 0) {
                            for (String layoutId : layoutIds) {
                                if (layoutId != null) {
                                    campaignService.saveXlf(Long.parseLong(layoutId));
                                    Layout layout = campaignService.getLayoutById(Long.valueOf(layoutId));
                                    layout.setCampaignId(entity.getDistributionInfo().get(i).getCampaignId());
                                    campaignService.updatelayout(layout);

                                }
                            }
                        }

                        // TO BE REMOVED AFTER TESTING AND UI INTEGRATION
                        String templateId = entity.getDistributionInfo().get(i).getTemplateId();
                        if (templateId != null) {
                            campaignService.saveXlf(Long.parseLong(templateId));
                            Layout layout = campaignService.getLayoutById(Long.valueOf(templateId));
                            layout.setCampaignId(entity.getDistributionInfo().get(i).getCampaignId());
                            campaignService.updatelayout(layout);
                        }
                    }
                }
            }

            DistributionInfo distribution = null;
            List<DistributionInfo> distributionList = new ArrayList<DistributionInfo>();
            if (entity.getDistributionInfo() != null && entity.getDistributionInfo().size() > 0) {
                for (int j = 0; j < entity.getDistributionInfo().size(); j++) {
                    if (entity.getDistributionInfo().get(j).getId() == null) {
                        distribution = distributionService.createDistribution(entity.getDistributionInfo().get(j));
                        distributionList.add(distribution);
                    } else {
                        distribution = distributionService.updateDistribution(entity.getDistributionInfo().get(j));
                        distributionList.add(distribution);
                    }
                }
            }

            campaignReponse.setCampaignId(respCampaign.getId());
            campaignReponse.setDistributionInfoId(distributionList);

        } catch (Exception ex) {
            logger.error("Error in create campaign ", ex);
            campaignReponse.setMessgae("Error in create campaign." + ex.getMessage());
        }

        logger.info("Create campaign ends " + campaignReponse);
        return campaignReponse;
    }

    @RequestMapping(value = "/update", consumes = "application/json", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public CampaignResponse updateCampaign(@RequestBody CreateCampaignRequest entity) throws Exception {
        logger.info("update campaign starts");

        CampaignResponse campaignReponse = new CampaignResponse();
        try {

            entity.getCampaign().setModifiedByUserId(UserDetail.getUserId());
            entity.getCampaign().setModifiedByUserName(UserDetail.getUsername());

            Campaign respCampaign = campaignService.updateCampaign(entity.getCampaign());
            List<DistributionInfo> distributionList = new ArrayList<DistributionInfo>();
            for (int i = 0; i < entity.getDistributionInfo().size(); i++) {
                DistributionInfo distribution = distributionService.updateDistribution(entity.getDistributionInfo().get(i));
                entity.getDistributionInfo().get(i).setChannel(entity.getDistributionInfo().get(i).getChannels());

                distributionList.add(distribution);
            }

            campaignReponse.setCampaignId(respCampaign.getId());
            campaignReponse.setDistributionInfoId(distributionList);
        } catch (Exception ex) {
            campaignReponse.setMessgae("Error in update campaign." + ex.getMessage());
            logger.error("Error in update campaign", ex);
        }

        logger.info("update campaign ends. " + campaignReponse);
        return campaignReponse;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public DeleteCampaignResponse deleteCampaign(@QueryParam("campaignId") Long campaignId,
                                                 HttpServletRequest httpServletRequest) {

        logger.info("delete campaign starts " + campaignId);
        Campaign campaign = campaignService.searchCampaignById(campaignId);
        String result = "Campaign with id " + campaignId + " deleted succesfully";
        DeleteCampaignResponse deleteCampaignResponse = new DeleteCampaignResponse();
        if (campaign != null) {
            campaign.setDeleted(true);
            try {
                campaignService.update(campaign);
            } catch (Exception ex) {
                logger.error("Error in delete campaign", ex);
                deleteCampaignResponse.setMessage(ex.getMessage());
            }
            deleteCampaignResponse.setResponse(result);
            logger.info("delete campaign  " + result);
        } else {
            result = "Campaign id " + campaignId + " does not exist";
            logger.info("delete campaign  " + result);
            deleteCampaignResponse.setResponse(result);
        }
        logger.info("delete campaign ends ");

        return deleteCampaignResponse;
    }

    @CacheEvict(cacheNames = "campaignSegments", allEntries = true)
    @RequestMapping(value = "/segment/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Segment create(@RequestBody Segment request) throws Exception {
        logger.info("create segment");
        Segment reponse = subscriberService.createSegment(request);

        return reponse;
    }

    @CacheEvict(cacheNames = "campaignStates", allEntries = true)
    @RequestMapping(value = "/state/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    State create(@RequestBody State request)
            throws Exception {
        logger.info("create campaign state");
        State reponse = campaignService.createState(request);
        return reponse;

    }

    @CacheEvict(cacheNames = "campaignChannels", allEntries = true)
    @RequestMapping(value = "/channel/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Channel create(@RequestBody Channel request)
            throws Exception {
        logger.info("create channel");
        Channel reponse = distributionService.createChannel(request);
        return reponse;
    }

    @RequestMapping(value = "/type/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Type create(@RequestBody Type request) throws Exception {
        logger.info("create camapign type");
        Type reponse = campaignService.createType(request);
        return reponse;

    }

    @RequestMapping(value = "/listContents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Content> getContentsList() {
        List<Content> contentList = contentService.getAllContents();
        return contentList;
    }

    @RequestMapping(value = "/events", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<EventsMapping> getEvents(@RequestBody ListEventRequest listEventRequest, HttpServletRequest httpServletRequest) throws ParseException {
        logger.info("list xibo events");
        List<DistributionInfo> distributionLst = distributionService.searchDistributionInfo((long) 0);

        List<EventsMapping> evntsList = new ArrayList<EventsMapping>();
        if (listEventRequest != null
                && listEventRequest.getDisplayGroupIds() != null
                && listEventRequest.getDisplayGroupIds().size() > 0) {
            for (String displayGrpId : listEventRequest.getDisplayGroupIds()) {
                if (distributionLst != null && distributionLst.size() > 0) {
                    for (DistributionInfo disTmp : distributionLst) {
                        if (disTmp != null && disTmp.getXiboSchedule() != null
                                && disTmp.getXiboSchedule().size() > 0) {
                            for (XiboSchedule xiboSchedule : disTmp.getXiboSchedule()) {
                                if (xiboSchedule != null
                                        && xiboSchedule.getDisplayGroupIds() != null
                                        && xiboSchedule.getDisplayGroupIds().size() > 0) {
                                    for (String dspGrpId : xiboSchedule.getDisplayGroupIds()) {
                                        logger.info("list xibo events get by display details  ");
                                        if (displayGrpId.equalsIgnoreCase("-1")) {
                                            logger.info("list xibo events get for all displays  ");
                                            SimpleDateFormat source = new SimpleDateFormat(
                                                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                            if (xiboSchedule.getFromDt() != null
                                                    && xiboSchedule.getToDt() != null) {
                                                Date stdtTemp = source.parse(xiboSchedule.getFromDt());
                                                Date todtTemp = source.parse(xiboSchedule.getToDt());

                                                if (listEventRequest.getFrom().before(stdtTemp)
                                                        && listEventRequest.getTo().after(todtTemp)) {
                                                    EventsMapping mapping = new EventsMapping();
                                                    mapping.setStart(String.valueOf(stdtTemp.getTime()));
                                                    mapping.setEnd(String.valueOf(todtTemp.getTime()));
                                                    mapping.setId(xiboSchedule
                                                            .getId());
                                                    mapping.setIsAllDay(false);
                                                    if (xiboSchedule.getEventId() != null
                                                            && !"".equalsIgnoreCase(xiboSchedule.getEventId())) {
                                                        if (xiboSchedule.getEventId() == null) {
                                                            mapping.setMeetingID(xiboSchedule.getId());
                                                        } else {
                                                            mapping.setMeetingID(Long.parseLong(xiboSchedule.getEventId()));
                                                        }

                                                    }
                                                    mapping.setTitle(xiboSchedule.getEventName());
                                                    String recurrenceRule = "";
                                                    if (xiboSchedule.getRecurrenceType() != null) {
                                                        recurrenceRule = "FREQ=" + xiboSchedule.getRecurrenceType().toUpperCase()
                                                                + ";COUNT="
                                                                + xiboSchedule.getRecurrenceDetail();
                                                        if (!xiboSchedule.getRecurrenceType().equalsIgnoreCase("Daily")) {
                                                            recurrenceRule = "FREQ="
                                                                    + xiboSchedule.getRecurrenceType().toUpperCase()
                                                                    + "LY;COUNT="
                                                                    + xiboSchedule.getRecurrenceDetail();
                                                        }
                                                    }
                                                    logger.info("recurrenceRule : " + recurrenceRule);
                                                    mapping.setRecurrenceRule(recurrenceRule);
                                                    evntsList.add(mapping);
                                                }
                                            }

                                        } else if (dspGrpId != null
                                                && dspGrpId.equalsIgnoreCase(displayGrpId)) {
                                            logger.info("list xibo events get selected display : " + displayGrpId);

                                            EventsMapping mapping = new EventsMapping();
                                            SimpleDateFormat source = new SimpleDateFormat(
                                                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                            if (xiboSchedule.getFromDt() != null && xiboSchedule.getToDt() != null) {

                                                Date stdtTemp = source.parse(xiboSchedule.getFromDt());
                                                Date todtTemp = source.parse(xiboSchedule.getToDt());

                                                if (listEventRequest.getFrom().before(stdtTemp)
                                                        && listEventRequest.getTo().after(todtTemp)) {
                                                    mapping.setStart(String.valueOf(stdtTemp.getTime()));
                                                    mapping.setEnd(String.valueOf(todtTemp.getTime()));
                                                    mapping.setIsAllDay(false);
                                                    mapping.setId(xiboSchedule.getId());
                                                    if (xiboSchedule.getEventId() != null
                                                            && !"".equalsIgnoreCase(xiboSchedule.getEventId())) {
                                                        if (xiboSchedule.getEventId() == null) {
                                                            mapping.setMeetingID(xiboSchedule.getId());
                                                        } else {
                                                            mapping.setMeetingID(Long.parseLong(xiboSchedule.getEventId()));
                                                        }
                                                    }
                                                    mapping.setTitle(xiboSchedule.getEventName());
                                                    String recurrenceRule = "";
                                                    if (xiboSchedule.getRecurrenceType() != null) {
                                                        recurrenceRule = "FREQ="
                                                                + xiboSchedule.getRecurrenceType().toUpperCase()
                                                                + ";COUNT="
                                                                + xiboSchedule.getRecurrenceDetail();
                                                        if (!xiboSchedule.getRecurrenceType().equalsIgnoreCase(
                                                                "Daily")) {
                                                            recurrenceRule = "FREQ="
                                                                    + xiboSchedule.getRecurrenceType().toUpperCase()
                                                                    + "LY;COUNT="
                                                                    + xiboSchedule.getRecurrenceDetail();
                                                        }
                                                    }
                                                    logger.info("recurrenceRule : " + recurrenceRule);
                                                    mapping.setRecurrenceRule(recurrenceRule);
                                                    evntsList.add(mapping);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        logger.info("list xibo events ends  ");

        return evntsList;
    }

    @RequestMapping(value = "/event/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteEvent(@RequestParam(value = "eventId") Long eventId) {
        logger.info("delete xibo events got starts  " + eventId);
        return distributionService.deleteDistributionInfoById(eventId);
    }

    @RequestMapping(value = "/displays", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Machine> getSubscribers(@RequestBody DeliverySegment segments) {
        return getDisplay(segments);
    }

    private List<Machine> getDisplay(DeliverySegment segments) {
        logger.info("get subscribers based on segmentation starts");
        List<Machine> susbcriberList = null;
        if (segments != null
                && segments.getSegmentRefName() != null
                && segments.getSegmentRefName().equalsIgnoreCase(
                "Display Segment")) {
            for (SegmentParam params : segments.getSegmentParams()) {
                if (params.getParamName() != null
                        && params.getParamName().equalsIgnoreCase(
                        "Display Name")) {
                    logger.info("get subscribers by name");
                    susbcriberList = subscriberService
                            .getSubscriberByName(params.getExpression());

                }
            }
        } else if (segments != null
                && segments.getSegmentRefName() != null
                && segments.getSegmentRefName().equalsIgnoreCase(
                "Location Segment")) {
            Double latitude = 0.0;
            Double longitude = 0.0;
            if (segments.getSegmentParams().size() == 1) {
                latitude = Double.parseDouble(segments.getSegmentParams()
                        .get(0).getExpression());
                longitude = Double.parseDouble(segments.getSegmentParams()
                        .get(1).getExpression());
            }
            logger.info("get subscribers by location");
            susbcriberList = subscriberService.getSubscriberByLocation(
                    latitude, longitude);
        }
        logger.info("get subscribers based on segmentation ends");
        return susbcriberList;

    }

    @RequestMapping(value = "/campaignDraft", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStateDraft(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStateDraft starts ");

        Campaign respCampaign = campaignService.updateCampaignState("Draft",
                Long.valueOf(campaignId));

        logger.info("updateCampaignStateDraft ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignRejected", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStateRejected(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStateRejected starts ");

        Campaign respCampaign = campaignService.updateCampaignState("Draft",
                Long.valueOf(campaignId));

        logger.info("updateCampaignStateRejected ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignReview", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStateReview(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStateReview starts ");

        Campaign respCampaign = campaignService.updateCampaignState(
                "In Review", Long.valueOf(campaignId));

        logger.info("updateCampaignStateReview ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignApproved", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStateApproved(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStateApproved starts ");

        Campaign respCampaign = campaignService.updateCampaignState("Approved",
                Long.parseLong(campaignId));

        logger.info(" updateCampaignStateApproved ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignPublished", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStatePublished(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStatePublished starts ");
        Campaign respCampaign = null;
        try {
            respCampaign = campaignService.updateCampaignState("Published", Long.parseLong(campaignId));
            if (respCampaign != null) {
                List<DistributionInfo> distributionLst = distributionService
                        .searchDistributionInfo(respCampaign.getId());
                if (distributionLst != null && distributionLst.size() > 0) {
                    for (DistributionInfo disTmp : distributionLst) {
                        distributionService.scheduleCampaignInfo(disTmp);
                    }
                }
            }

        } catch (Exception ex) {
            logger.error("Exception in updateCampaignStatePublished", ex);
        }

        logger.info(" updateCampaignStatePublished ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignExpired", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStateExpired(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStateExpired starts ");

        Campaign respCampaign = campaignService.updateCampaignState("Expired",
                Long.parseLong(campaignId));

        logger.info(" updateCampaignStateExpired ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignPaused", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStatePaused(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStatePaused starts ");

        Campaign respCampaign = campaignService.updateCampaignState("Paused",
                Long.parseLong(campaignId));

        logger.info(" updateCampaignStatePaused ends " + respCampaign);
        return respCampaign;
    }

    @RequestMapping(value = "/campaignTerminated", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Campaign updateCampaignStateTerminated(
            HttpServletRequest request,
            @RequestParam("campaignId") String campaignId) throws Exception {
        logger.info(" updateCampaignStateTerminated starts ");

        Campaign respCampaign = campaignService.updateCampaignState(
                "Terminated", Long.parseLong(campaignId));

        logger.info(" updateCampaignStateTerminated ends " + respCampaign);
        return respCampaign;
    }

    public List<Campaign> getCampaignByStates(Long stateId, Long approverId) {

        logger.info(" Get Campaigns search by states  ");
        List<Campaign> campaigns = campaignService.getCampaignByState(stateId);
        // Commented as we might not need it in new manage campaign UI

        // List<Campaign> campaigns =
        // campaignService.searchCampaignByState(stateId, approverId);
        // if (campaigns != null && campaigns.size() > 0) {
        // for (int i = 0; i < campaigns.size(); i++) {
        // logger.info(" Get Campaigns distribution information  "
        // + campaigns.get(i).getId());
        //
        // // TODO: implement search by campaign id.
        // List<DistributionInfo> distributionData = distributionService
        // .searchDistributionInfo(campaigns.get(i).getId());
        // logger.info("distributionData.size : "
        // + distributionData.size());
        //
        // if (distributionData != null && distributionData.size() > 0) {
        // for (int j = 0; j < distributionData.size(); j++) {
        // String layoutId = distributionData.get(j)
        // .getTemplateId();
        // logger.info("Layout Id : " + layoutId);
        // if (layoutId != null) {
        // Layout layoutData = thirdPartyServices
        // .getRegions(Long.parseLong(layoutId));
        // if (layoutData != null) {
        // distributionData.get(j).setTemplateName(
        // layoutData.getLayout());
        // distributionData.get(j).setTemplateHeight(
        // layoutData.getHeight().toString());
        // distributionData.get(j).setTemplateWidth(
        // layoutData.getWidth().toString());
        // distributionData.get(j).setStatus(
        // layoutData.getStatus());
        // }
        // }
        //
        // logger.info("distributionData.get(j).getSegments().size() : "
        // + distributionData.get(j).getSegments().size());
        // if (distributionData.get(j).getSegments().size() > 0) {
        // String displays = getDisplay(distributionData
        // .get(j).getSegments().get(0));
        // distributionData.get(j).setDisplays(displays);
        // }
        //
        // }
        // }
        //
        // campaigns.get(i).setDistributionInfo(distributionData);
        // // logger.info(" Get Campaigns promotions ");
        // // if (campaigns.get(i).getPromotionMappings() != null) {
        // // for (PromotionMapping promotions : campaigns.get(i)
        // // .getPromotionMappings()) {
        // //
        // // if (promotions.getPromotion_Id() != null) {
        // // Promotion promotion = new Promotion();
        // // promotion.setId(promotions.getPromotion_Id());
        // // logger.info(" Get Campaigns promotion information  ");
        // // List<Promotion> promotionsTmp = promotionService
        // // .searchPromotionById(
        // // tenantId.intValue(),
        // // promotions.getPromotion_Id());
        // // campaigns.get(i).setPromotions(promotionsTmp);
        // // }
        // //
        // // }
        // // }
        //
        // /*
        // * logger.info(" Get Campaigns : Approvers list "); if
        // * (campaigns.get(i).getApproverMappings() != null &&
        // * campaigns.get(i).getApproverMappings().size() > 0) {
        // * ApproverResponse approverList = eclipseMediaService
        // * .getApprover(authToken); List<ApproverData> approverLst = new
        // * ArrayList<ApproverData>(); for (ApproverMapping approverTmp :
        // * campaigns.get(i) .getApproverMappings()) { for (ApproverData
        // * approvermeta : approverList .getMetaData().getApprovers()) {
        // * if (approvermeta.getId() == approverTmp .getApproverId()) {
        // * ApproverData resp = approvermeta; approverLst.add(resp);
        // * break; } } }
        // *
        // * campaigns.get(i).setApproverData(approverLst); }
        // */
        // }
        // }

        return campaigns;
    }

    @RequestMapping(value = "/layout/clone", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public
    @ResponseBody
    Layout cloneLayout(@RequestBody String layoutId)
            throws Exception {
        Layout newLayout = null;
        logger.info(" cloneLayout starts ");
        if (layoutId != null) {
            newLayout = campaignService.cloneLayout(layoutId);
        }
        logger.info(" cloneLayout ends: newLayout " + newLayout);

        return newLayout;
    }

    @RequestMapping(value = "/update/machine", consumes = "application/json", method = RequestMethod.PUT, produces = "application/json")
    public
    @ResponseBody
    Machine updateMachine(@RequestBody Machine request)
            throws Exception {
        logger.info("create channel  ");
        Machine reponse = subscriberService.updateMachine(request);
        return reponse;

    }

    @RequestMapping(value = "/addWidgetAnduploadMedia", method = RequestMethod.POST)
    @ResponseBody
    public Widget addWidgetAnduploadMedia(
            @RequestParam(value = "file") MultipartFile file,
            @QueryParam("playListId") Long playListId
    ) {
        Widget response = null;
        if (file != null && playListId != null) {
            Widget widget = new Widget();

            String mediaType = extractContentType(file.getContentType());
            widget.setCalculatedDuration(10);
            widget.setDisplayOrder("1"); // TODO: in case of multiple widgets in a playlist, display order needs to be handled dynamically.
            widget.setDuration(10); // Default duration of media. TODO: need to find duration of video
            widget.setPlaylistId(playListId);
            widget.setType(mediaType);

            Widget widgetTemp = campaignService.createWidget(widget);
            response = uploadContentInWidget(file, widgetTemp);
        }
        return response;
    }

    @RequestMapping(value = "/widget/text", method = RequestMethod.POST)
    @ResponseBody
    public Widget saveTextContent(@RequestBody Text textRequest) {
        Widget response = null;
        if (textRequest != null) {
            response = campaignService.saveTextContent(textRequest);
        }
        return response;
    }

    @RequestMapping(value = "/uploadMedia", method = RequestMethod.POST)
    @ResponseBody
    public Widget uploadMedia(@RequestParam("file") MultipartFile file,
                              @QueryParam("widgetId") Long widgetId) {
        Widget response = null;
        if (file != null && widgetId != null && widgetId > 0) {
            Widget widget = campaignService.getWidgetById(widgetId);
            response = uploadContentInWidget(file, widget);
        }
        return response;
    }

    private Widget uploadContentInWidget(MultipartFile file, Widget widget) {
        Widget updatedWidget = null;
        try {
            if (file != null) {
                List<FileInfo> fileInfo = new ArrayList<FileInfo>();
                fileInfo = contentService.uploadContent(file);
                logger.info("fileInfo:" + fileInfo);

                if (fileInfo != null && fileInfo.size() > 0) {
                    String fileId = fileInfo.get(0).getFileId();
                    logger.info("fileId:" + fileId);

                    if (widget != null) {
                        String streamUrl = contentStreamUrl + "?fileId=" + fileId;
                        widget.setStreamUrl(streamUrl);

                        String contentType = fileInfo.get(0).getContentType();
                        widget.setType(extractContentType(contentType));

                        String thumbUrl = "";
                        if (fileInfo.get(0).getThumbId() != null && !"".equals(fileInfo.get(0).getThumbId())) {
                            thumbUrl = contentStreamUrl + "?fileId="
                                    + fileInfo.get(0).getThumbId();
                            widget.setThumbUrl(thumbUrl);
                        }


                        List<Media> medias = new ArrayList<Media>();
                        Media media = new Media();
                        media.setContentRefId(fileId);
                        media.setName(fileInfo.get(0).getFileName());
                        media.setFileName(fileInfo.get(0).getFileName());
                        media.setStreamUrl(streamUrl);
                        media.setThumbUrl(thumbUrl);
                        media.setMediaType(widget.getType());

                        if (widget.getType() != null && "video".equalsIgnoreCase(widget.getType())) {
                            media.setDuration(Double.valueOf(fileInfo.get(0).getDuration())); // set duration of video
                        } else {
                            media.setDuration(10d); // // set default duration of image as 10 seconds
                        }
                        medias.add(media);

                        if (widget.getType() != null && "video".equalsIgnoreCase(widget.getType())) {
                            widget.setDuration(Integer.valueOf(fileInfo.get(0).getDuration())); // set duration of video
                        }
                        widget.setMedia(medias);
                        updatedWidget = campaignService.updateWidget(widget);
                        logger.info("updatedWidget:" + updatedWidget);

                        if (updatedWidget != null) {
                            List<Media> mediaUpdated = updatedWidget.getMedia();
                            if (mediaUpdated != null && mediaUpdated.size() > 0) {
                                Media mediaT = mediaUpdated.get(0);
                                String[] tokens = mediaT.getFileName().split(
                                        "[.]");
                                if (tokens != null) {
                                    String extension = tokens[tokens.length - 1];
                                    mediaT.setStoredAs(mediaT.getMediaId()
                                            + "." + extension);
                                    campaignService.updateMedia(mediaT);
                                }
                            }
                        }
                        return updatedWidget;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error in upload media", e);
        }
        return updatedWidget;
    }

    private String extractContentType(String type) {
        String mediaType = type;
        if (type != null) {
            String token[] = type.split("[/]");
            if (token != null && token.length > 0) {
                mediaType = token[0];
                logger.info("Extracted media type from multipart:" + mediaType);
            }
        }
        return mediaType;
    }

    /**
     * Create a template
     *
     * @param layout
     * @return
     * @throws Exception
     */
    @CacheEvict(cacheNames = "campaignLayouts", allEntries = true)
    @RequestMapping(value = "/layout/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Layout createTemplate(@RequestBody Layout layout) {
        logger.info("create template starts");
        Layout response = null;
        try {
            logger.debug("createTemplate input param. layout:" + layout);
            if (layout != null) {
                layout.setType(LayoutType.TEMPLATE);
                response = campaignService.createLayout(layout);
            }
        } catch (Exception e) {
            logger.error("Error in create template.", e);
        }
        logger.info("create template response.result: " + response);

        return response;
    }

    /**
     * Create regions
     *
     * @param regions
     * @return
     */
    @RequestMapping(value = "/region/create", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public List<Regions> createRegion(@RequestBody List<Regions> regions) {
        logger.info("create region starts");
        List<Regions> result = null;
        try {
            logger.debug("createRegion input param. regions:" + regions);
            if (regions != null && regions.size() > 0) {
                result = campaignService.createRegion(regions);
            }
        } catch (Exception e) {
            logger.error("Error in creating region", e);
        }
        logger.info("create region ends. result: " + result);

        return result;
    }

    /**
     * Create a widget
     *
     * @param widget
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/widget/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Widget createWidgets(@RequestBody Widget widget) {
        logger.info("create widget starts");
        Widget result = null;
        try {
            logger.debug("createWidgets input param. layout:" + widget);
            if (widget != null) {
                result = campaignService.createWidget(widget);
            }
        } catch (Exception e) {
            logger.error("Error in creating widgets", e);
        }
        logger.info("create widget ends. result: " + result);

        return result;
    }

    /**
     * Update layout
     *
     * @param layout
     * @return
     */
    @CacheEvict(cacheNames = "campaignLayouts", allEntries = true)
    @RequestMapping(value = "/layout/update", method = RequestMethod.PUT)
    @ResponseBody
    public CampaignResp updateLayout(@RequestBody Layout layout) {
        logger.info("updateLayout starts");
        CampaignResp response = new CampaignResp();
        try {
            logger.debug("updateLayout input param. layout:" + layout);
            if (layout != null) {
                campaignService.updatelayout(layout);
                response.setMessage("success");
                response.setStatus("success");
            }
        } catch (Exception e) {
            logger.error("Error in updateLayout", e);
            response.setMessage("Error in updateLayout." + e.getMessage());
            response.setStatus("error");
        }
        logger.info(" updateLayout ends.");

        return response;
    }

    /**
     * Get layouts by type
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/layout/getByType", method = RequestMethod.GET)
    @ResponseBody
    public List<Layout> getLayoutsByType(@RequestParam String type) {
        logger.info("getLayoutsByType starts");
        List<Layout> layouts = null;
        try {
            logger.debug("getByType input param. type:" + type);
            if (type != null && !"".equals(type)) {
                layouts = campaignService.getLayoutsByType(type.trim());
            }
        } catch (Exception e) {
            logger.error("Error in getLayoutsByType", e);
        }
        logger.info(" getLayoutsByType ends. result: " + layouts);

        return layouts;
    }

    /**
     * Delete widget by id
     *
     * @param widgetId
     * @return
     */
    @RequestMapping(value = "/widget/deleteById", method = RequestMethod.DELETE)
    @ResponseBody
    public CampaignResponse deleteWidgetById(@RequestParam String widgetId) {
        logger.info(" deleteWidgetById starts ");
        CampaignResponse response = new CampaignResponse();
        try {
            logger.debug("deleteWidgetById input param. widgetId:" + widgetId);
            if (widgetId != null && !"".equals(widgetId)) {
                String msg = campaignService.deleteWidgetById(widgetId.trim());
                logger.debug("deleteWidgetById response msg:" + msg);
                response.setMessgae(msg);
            }
        } catch (Exception e) {
            logger.error("Error in deleteWidgetById", e);
            response.setMessgae("error");
        }
        logger.info(" deleteWidgetById ends. result:" + response);

        return response;
    }

    /**
     * Delete layout by id
     *
     * @param layoutId
     * @return
     */
    @CacheEvict(cacheNames = "campaignLayouts", allEntries = true)
    @RequestMapping(value = "/layout/delete", method = RequestMethod.DELETE)
    @ResponseBody
    public CampaignResp deleteLayout(@RequestParam String layoutId) {
        logger.info(" deleteLayout starts ");
        CampaignResp response = new CampaignResp();
        try {
            logger.debug("deleteLayout input param. layoutId:" + layoutId);
            if (layoutId != null && !"".equals(layoutId)) {
                String msg = campaignService.deleteLayout(layoutId.trim());
                logger.debug("deleteLayout response msg:" + msg);
                response.setStatus("success");
                response.setMessage(msg);
            }
        } catch (Exception e) {
            logger.error("Error in deleteLayout", e);
            response.setStatus("error");
            response.setMessage("Error in deleteLayout" + e != null ? e.getMessage() : "");
        }
        logger.info(" deleteLayout ends.");

        return response;
    }

    @CacheEvict(cacheNames = {"campaignStates", "campaignSegments", "campaignChannels", "campaignLayouts"}, allEntries = true)
    @RequestMapping(value = "evictcache", method = RequestMethod.GET)
    public void evictCache() {

    }


    @RequestMapping(value = "/layout/reset", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Layout resetLayout(@RequestBody String layoutId) {
        logger.info("resetLayout starts");
        Layout newLayout = null;
        try {
            logger.debug("resetLayout input param. layout:" + layoutId);
            if (layoutId != null) {
                newLayout = campaignService.resetLayout(layoutId);
            }
        } catch (Exception e) {
            logger.error("Error in reset layout", e);
        }
        logger.info("resetLayout. result: " + newLayout);
        return newLayout;
    }

    @RequestMapping(value = "/tidyLayouts", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void tidyLayouts(@RequestBody String layoutName) throws CampaignManagementException {
        logger.info("tidyLayouts starting");

        String token = thirdPartyServices.getAccessToken().getAccess_token();
        List<Layout> layouts = thirdPartyServices.getLayouts(layoutName, token);

        logger.debug("Found " + layouts.size() + " layouts to tidy up");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, -1);
        Date fourHoursAgo = calendar.getTime();
        List<Layout> tidyLayouts = new ArrayList<Layout>();
        for (Layout layout : layouts) {
            if (layout.getLayout().indexOf(layoutName) > 0 && layout.getCreatedDt().before(fourHoursAgo)) {
                tidyLayouts.add(layout);
            } else {
                logger.debug("Excluding layout " + layout.getLayout() + " from tidy routine, created date: " + layout.getCreatedDt());
            }
        }

        logger.info("Deleting " + tidyLayouts.size() + " layouts");

        for (Layout layout : tidyLayouts) {
            thirdPartyServices.deleteLayout(layout.getLayoutId(), token);
        }

        logger.info("Tidying Library");

        thirdPartyServices.libraryTidy(token);

        logger.info("tidyLayouts ending");
    }

    @RequestMapping(value = "/fillLayoutContent", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public CampaignResp fillLayoutContent(@RequestBody AdRequest adRequest) throws Exception, CampaignManagementException {
        logger.info("fillLayoutContent starts");

        CampaignResp response = new CampaignResp();
        if (CommonUtil.isNonEmptyAndNotNull(adRequest.getDeviceId())
                && CommonUtil.isNonEmptyAndNotNull(adRequest.getFromDate())
                && CommonUtil.isNonEmptyAndNotNull(adRequest.getToDate())
                && CommonUtil.isNonEmptyAndNotNull(adRequest.getLayoutId())) {
            logger.info("fillLayoutContent:" + adRequest.getLayoutId());

            AdProvider adProvider = adRequest.getAdProvider();
            ProcessAdRequest processAdRequest = new ProcessAdRequest();
            processAdRequest.setAdTagUrl(adRequest.getAdTagUrl());
            ProcessAdResponse processAdResponse;
            switch (adProvider) {
                case OpenX:
                    processAdResponse = openXProcessor.processAdRequest(processAdRequest);

                    break;
                case Rubicon:
                    String account = adRequest.getAdProviderAccount();
                    Environment environment = applicationContext.getEnvironment();

                    processAdRequest.setUsername(environment.getProperty(String.format("rubicon.%s.username", account)));
                    processAdRequest.setPassword(environment.getProperty(String.format("rubicon.%s.password", account)));
                    processAdRequest.setSiteName(environment.getProperty(String.format("rubicon.%s.siteName", account)));
                    processAdRequest.setSiteDomain(environment.getProperty(String.format("rubicon.%s.siteDomain", account)));
                    processAdRequest.setSitePage(environment.getProperty(String.format("rubicon.%s.sitePage", account)));
                    processAdRequest.setSitePublisherAccountId(Integer.valueOf(environment.getProperty(String.format("rubicon.%s.sitePublisherAccountId", account))));
                    processAdRequest.setSiteId(Integer.valueOf(environment.getProperty(String.format("rubicon.%s.siteId", account))));

                    // we won't have the "device" attributes configured per-device, so we'll have to default the values
                    processAdRequest.setDeviceIpAddress(environment.getProperty(String.format("rubicon.%s.deviceIpAddress", account)));
                    processAdRequest.setDeviceUserAgent(environment.getProperty(String.format("rubicon.%s.deviceUserAgent", account)));
                    processAdRequest.setDeviceZip(environment.getProperty(String.format("rubicon.%s.deviceZip", account)));

                    processAdResponse = rubiconProcessor.processAdRequest(processAdRequest);

                    break;
                case SpringServe:
                    processAdResponse = springServeProcessor.processAdRequest(processAdRequest);

                    break;
                default:
                    throw new NotImplementedException(String.format("No implementation exists to retrieve ads for source = %s", adProvider));
            }

            if (processAdResponse != null && processAdResponse.getMediaUrl() != null) {
                URL mediaUrl = processAdResponse.getMediaUrl();
                File mediaFile = downloadMediaFile(mediaUrl, processAdResponse.getRequestId().toString());

                long widgetId = 0;
                long playListId = 0;

                String token = thirdPartyServices.getAccessToken().getAccess_token();
                System.out.println(token);
                Layout layout = thirdPartyServices.copyLayout(adRequest.getLayoutId(), token);

                if (layout != null) {
                    logger.info("layoutid:" + layout.getLayoutId());

                    Long campaignIdForLayout = layout.getCampaignId();
                    logger.info("campaignIdForLayout:" + campaignIdForLayout);

                    if (layout.getRegions() != null
                            && layout.getRegions().size() > 0
                            && layout.getRegions().get(0) != null
                            && layout.getRegions().get(0).getPlaylists() != null
                            && layout.getRegions().get(0).getPlaylists().size() > 0) {
                        playListId = layout.getRegions().get(0).getPlaylists().get(0).getPlaylistId();
                        logger.info("playListId:" + playListId);

                        List<Widget> widgets = layout.getRegions().get(0).getPlaylists().get(0).getWidgets();
                        if (widgets != null) {
                            for (Widget widget : widgets) {
                                widgetId = widget.getWidgetId();
                                logger.info("Going to delete widget " + widgetId);
                                thirdPartyServices.deleteMedia(widget.getWidgetId(), token); // delete widgets in layout
                            }
                        }
                    }

                    AdDetail adDetail = new AdDetail();
                    adDetail.setAdid(processAdResponse.getAdId());
                    adDetail.setFromDate(adRequest.getFromDate());
                    adDetail.setToDate(adRequest.getToDate());
                    adDetail.setDisplayGroupId(adRequest.getDeviceId());

                    URL impressionUrl = processAdResponse.getImpressionUrl();
                    if (impressionUrl != null) {
                        adDetail.setImpression(impressionUrl.toString());
                    } else if (processAdResponse.getProofOfPlayUrls().length > 0) {
                        String impression = "";
                        for (int i = 0; i < processAdResponse.getProofOfPlayUrls().length; i++) {
                            impression += processAdResponse.getProofOfPlayUrls()[i].toString();
                            if (i != processAdResponse.getProofOfPlayUrls().length - 1) {
                                impression += "|";
                            }
                        }
                        adDetail.setImpression(impression);
                    }

                    // TODO - fix this; there should be a difference between a display and displayGroup
                    if (adRequest.getDevices() != null) {
                        adDetail.setDisplayGroups(adRequest.getDevices());
                    } else {
                        List<String> displayGroups = new ArrayList<String>();
                        displayGroups.add(adRequest.getDeviceId());

                        adDetail.setDisplayGroups(displayGroups);
                    }

                    if (layout != null) {
                        adDetail.setLayoutId(String.valueOf(layout.getLayoutId()));

                        MultipartFile multipartMediaFile = MultipartUtility.convertFileToMultipart(mediaFile);
                        logger.info("Going to upload media.");
                        UploadResponse uploadResponse = thirdPartyServices.uploadContent(multipartMediaFile, playListId, token);

                        List<campaign.api.gateway.dto.xibo.File> files = uploadResponse.getFiles();
                        if ((files != null) && (files.size() > 0)) {
                            if (files.size() > 1) {
                                throw new RuntimeException("It is unexpected for uploadResponse.getFiles() to be greater than 0");
                            }

                            campaign.api.gateway.dto.xibo.File file = files.get(0);
                            String error = file.getError();
                            if (StringUtils.isBlank(error)) {
                                String fileStoredAs = file.getStoredas();
                                adDetail.setFileStoredAs(fileStoredAs);
                                logger.debug("stored-as filename:" + fileStoredAs);

                                String mediaId = "";
                                String[] tokens = fileStoredAs.split("[.]");
                                if (tokens != null && tokens.length > 0) {
                                    for (int i = 0; i < tokens.length - 1; i++) {
                                        mediaId += tokens[i];
                                    }

                                    if (mediaId != null && mediaId != "") {
                                        adDetail.setMediaId(mediaId);
                                        logger.debug("mediaId:" + mediaId);
                                    }
                                }
                            } else {
                                throw new RuntimeException(error);
                            }
                        }
                    }

                    logger.info("Going to save adDetail in database");
                    adDaoImpl.save(adDetail);

                    if (campaignIdForLayout != null) {
                        XiboSchedule schedule = new XiboSchedule();
                        schedule.setFromDt(adRequest.getFromDate());
                        schedule.setToDt(adRequest.getToDate());
                        schedule.setFromDtLink(adRequest.getFromDate());
                        schedule.setToDtLink(adRequest.getToDate());
                        schedule.setDayPartId(1);
                        schedule.setEventTypeId("1");
                        schedule.setIsRecurring(false);
                        schedule.setIsPriority(1);
                        schedule.setDisplayOrder(0);
                        schedule.setCampaignId(campaignIdForLayout);
//                        schedule.setDisplayGroupIds(adRequest.getDevices());
                        // TODO - fix this; there should be a difference between a display and displayGroup
                        if (adRequest.getDevices() != null) {
                            schedule.setDisplayGroupIds(adRequest.getDevices());
                        } else {
                            List<String> displayGroupIds = new ArrayList<String>();
                            displayGroupIds.add(adRequest.getDeviceId());

                            schedule.setDisplayGroupIds(displayGroupIds);
                        }

                        logger.info("Going to schedule layout");
                        thirdPartyServices.scheduleLayout(schedule, token);

                        if (sendImpressionOnLoad) {
                            sendAdImpression(adDetail.getMediaId());
                        }

                        logger.debug("Successfully filled layout content");
                        response.setStatus("success");
                        response.setMessage("success");
                    }
                } else {
                    response.setStatus("error");
                    response.setMessage("Error in fillLayoutContent.");
                }
            } else {
                response.setStatus("error");
                response.setMessage("No ad found");
            }
        } else {
            logger.warn("Error in processign ad tag content: Mandatory param missing in request");
            response.setStatus("error");
            response.setMessage("Mandatory param missing in request");
        }
        logger.info("fillLayoutContent ends");

        return response;
    }

    private File downloadMediaFile(URL mediaUrl, String filename) throws IOException {
        ReadableByteChannel rbc = Channels.newChannel(mediaUrl.openStream());

        File file = File.createTempFile(filename, mediaUrl.getFile().substring(mediaUrl.getFile().lastIndexOf(".")));
        file.deleteOnExit();

        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();

        return file;
    }
}
