/**
 * 
 */
package campaign.api.gateway.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import campaign.api.gateway.dto.Categories;
import campaign.api.gateway.dto.Promotion;
import campaign.api.gateway.dto.PromotionMetaData;
import campaign.api.gateway.dto.State;
import campaign.api.gateway.service.EclipseMediaIntegrationService;
import campaign.api.gateway.service.PromotionIntegrationService;

/**
 * @author kritika.srivastava
 *
 */
@RestController
@RequestMapping("/offerService")
public class PromotionIntegrationController {

	@Autowired
	PromotionIntegrationService promotionService;

	@Autowired
	EclipseMediaIntegrationService eclipseMediaService;

	@RequestMapping(value = "/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Promotion create(@RequestBody Promotion entity)
			throws Exception {

		Promotion response = promotionService.createPromotion(entity);

		return response;
	}

	@RequestMapping(value = "/state/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody State create(@RequestBody State request)
			throws Exception {
		State reponse = promotionService.createState(request);
		return reponse;

	}

	@RequestMapping(value = "/type/create", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody Categories create(@RequestBody Categories request)
			throws Exception {
		Categories reponse = promotionService.createType(request);
		return reponse;

	}

	@RequestMapping(value = "/getPromotionsMetaData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PromotionMetaData getCampaignMetadata(HttpServletRequest httpServletRequest) {

		List<State> states = promotionService.getState();
		List<Categories> types = promotionService.getCategories();
	
		PromotionMetaData metaData = new PromotionMetaData();
		metaData.setPromotionStates(states);
		metaData.setPromotionTypes(types);

		return metaData;
	}

}
