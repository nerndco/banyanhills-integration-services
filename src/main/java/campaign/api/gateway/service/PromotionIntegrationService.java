/**
 * 
 */
package campaign.api.gateway.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import campaign.api.gateway.dto.Categories;
import campaign.api.gateway.dto.Promotion;
import campaign.api.gateway.dto.PromotionComments;
import campaign.api.gateway.dto.PromotionLikers;
import campaign.api.gateway.dto.State;
import campaign.api.gateway.util.ApplicationConstants;

/**
 * @author kritika.srivastava
 *
 */
@Service
public class PromotionIntegrationService {
	@Autowired
	RestTemplate restTemplate;

	/*
	 * @HystrixCommand(fallbackMethod = "stubOrders", commandProperties = {
	 * 
	 * @HystrixProperty(name = "execution.isolation.strategy", value =
	 * "SEMAPHORE") })
	 */
	
	static final Logger logger = LoggerFactory.getLogger(PromotionIntegrationService.class);

	public List<Promotion> getPromotions(Integer tenantId) {
		logger.info(" get promotions starts ");

		ResponseEntity<List<Promotion>> promData = restTemplate
				.exchange(ApplicationConstants.PROMOTION_READ+"?tenantId="+tenantId, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Promotion>>() {});

		List<Promotion> promotions = promData.getBody();
		
		logger.info(" get promotions ends ");

		return promotions;
	}
	
	public List<Promotion> getOffersMobile() {
		logger.info(" get promotion for mobile starts ");

		ResponseEntity<List<Promotion>> promData = restTemplate
				.exchange(ApplicationConstants.PROMOTION_READ, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Promotion>>() {});
	
		List<Promotion> promotions = promData.getBody();
		
		logger.info(" get promotion for mobile ends ");

		return promotions;
	}

	public List<Promotion> getPromotionById(Long entity) {
		logger.info(" get promotion by id starts ");
		String url = ApplicationConstants.PROMOTION_BY_ID+entity;
		logger.info("URL : "+ url);
		ResponseEntity<List<Promotion>> promData = restTemplate
				.exchange(url, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Promotion>>() {});

		List<Promotion> promotions = promData.getBody();
		
		logger.info(" get promotion by id ends ");

		return promotions;
	}
	
	@SuppressWarnings("unused")
	private String stubOrders(final String userId) {
		return "Invalid userid to fetch orders...";
	}

	public Promotion createPromotion(Promotion request) {
		logger.info(" create promotion starts ");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Promotion> entity = new HttpEntity<Promotion>(
				request, header);
		ResponseEntity<Promotion> response = restTemplate
				.exchange(ApplicationConstants.PROMOTION_CREATE, HttpMethod.POST, entity,
						Promotion.class);
		logger.info(" create promotion ends ");

		return response.getBody();
	}
	
	public State createState(State request) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<State> entity = new HttpEntity<State>(
				request, header);
		ResponseEntity<State> response = restTemplate.exchange(
				ApplicationConstants.PROMOTION_STATE_CREATE, HttpMethod.POST,entity, State.class);
		return response.getBody();

	}
	public Categories createType(Categories request) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Categories> entity = new HttpEntity<Categories>(
				request, header);
		ResponseEntity<Categories> response = restTemplate.exchange(
				ApplicationConstants.PROMOTION_CATEGORY_CREATE, HttpMethod.POST,entity, Categories.class);
		return response.getBody();

	}

	public PromotionLikers updatePromotionLikers(PromotionLikers request) {
		logger.info(" update promotion likers starts ");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<PromotionLikers> httpEntity = new HttpEntity<PromotionLikers>(request, header);
		ResponseEntity<PromotionLikers> response = restTemplate
				.exchange(ApplicationConstants.PROMOTION_LIKERS_UPDATE, HttpMethod.PUT, httpEntity,
						PromotionLikers.class);
		
		logger.info(" update promotion likers ends ");

		return response.getBody();

	}
	
	public Promotion updatePromotion(Promotion request) {
		logger.info(" update promotion starts ");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Promotion> httpEntity = new HttpEntity<Promotion>(request, header);
		ResponseEntity<Promotion> response = restTemplate
				.exchange(ApplicationConstants.PROMOTION_UPDATE, HttpMethod.PUT, httpEntity,
						Promotion.class);
		logger.info(" update promotion ends ");

		return response.getBody();

	}
	
	
	public String deletePromotion(Promotion request) {
		logger.info(" delete promotion by id starts ");

		HttpEntity<Promotion> httpEntity = new HttpEntity<Promotion>(request);
		ResponseEntity<String> response = restTemplate
				.exchange(ApplicationConstants.PROMOTION_DELETE, HttpMethod.DELETE, httpEntity,
						String.class);
		logger.info(" delete promotion by id starts ");

		return response.getBody();

	}
	
	
	public List<Promotion> searchPromotionById(Integer tenantId, Long id) {
		logger.info(" search promotion by id starts ");

		ResponseEntity<List<Promotion>> response = restTemplate.exchange(
				ApplicationConstants.PROMOTIONS_SEARCH_BY_ID+"?id="+id+"&tenantId="+tenantId, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Promotion>>() {});
		logger.info(" search promotion by id ends ");

		return response.getBody();
		
		
	}
	
	
	public List<Categories> getCategories() {
		ResponseEntity<List<Categories>> typesList = restTemplate
				.exchange(ApplicationConstants.PROMOTION_CATEGORIES, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Categories>>() {});
		return typesList.getBody();

	}

	public List<State> getState() {
		ResponseEntity<List<State>> stateList = restTemplate
				.exchange(ApplicationConstants.PROMOTION_STATES, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<State>>() {});
		return stateList.getBody();

	}

	public PromotionComments createComment(PromotionComments comments) {
		logger.info(" create comment starts ");

		HttpHeaders header = new HttpHeaders();
	//	header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<PromotionComments> httpEntity = new HttpEntity<PromotionComments>(comments, header);
		
		ResponseEntity<PromotionComments> stateList = restTemplate
				.exchange(ApplicationConstants.PROMOTION_COMMENTS, HttpMethod.POST, httpEntity,
						PromotionComments.class);
		
		logger.info(" create comment ends ");

		return stateList.getBody();

	}
}
