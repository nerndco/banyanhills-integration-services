package campaign.api.gateway.service

import java.util.List;

import groovy.util.logging.Slf4j

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

import campaign.api.gateway.dto.Channel;
import campaign.api.gateway.dto.State
import campaign.api.gateway.dto.segment.Segment
import campaign.api.gateway.dto.xibo.Layout;
import campaign.api.gateway.util.LayoutType;

/**
 * Have to make separate service for @Cacheable methods, reason being ..
 *
 * Only external method calls coming in through the proxy are intercepted. This
 * means that self-invocation, in effect, a method within the target object
 * calling another method of the target object, will not lead to an actual cache
 * interception at runtime even if the invoked method is marked with @Cacheable.
 *
 * @author Arvind.Chauhan
 *
 */
@Service
@Slf4j
class CampaignCacheableService {
	
	
	@Autowired CampaignIntegrationService campaignService;
	@Autowired SubscriberIntegrationService subscriberService;
	@Autowired DistributionServiceIntegration distributionService;
	
	@Cacheable("campaignStates")
	public List<State> getState(String username){
		log.info("Getting campaign state");
		return campaignService.getState();
	}
	
	@Cacheable("campaignSegments")
	public List<Segment> getAllSegements(String username){
		log.info("Getting campaign segment");
		return subscriberService.getAllSegements();
	}
	
	@Cacheable("campaignChannels")
	public List<Channel> getAllChannels(String username){
		log.info("Getting campaign Channel");
		return distributionService.getAllChannels();
	}
	
	@Cacheable("campaignLayouts")
	public List<Layout> getLayoutsByType(String layoutType, String username){
		log.info("Getting campaign Layout");
		return campaignService.getLayoutsByType(layoutType);
	}
}
