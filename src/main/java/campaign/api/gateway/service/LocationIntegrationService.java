/**
 * 
 */
package campaign.api.gateway.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import campaign.api.gateway.dto.Type;
import campaign.api.gateway.dto.campaign.CampaignLocation;
import campaign.api.gateway.dto.campaign.Location;
import campaign.api.gateway.dto.location.SubscriberLocation;
import campaign.api.gateway.util.ApplicationConstants;

/**
 * @author kritika.srivastava
 *
 */
@Service
public class LocationIntegrationService {
	@Autowired
	private RestTemplate restTemplate;
	
	static final Logger logger = LoggerFactory.getLogger(LocationIntegrationService.class);
	
	public List<CampaignLocation> getCampaignByLocation(Location location) {
		logger.info("get location by location " + location.getLatitude() + " : " + location.getLongitude() );
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Location> locationEntity = new HttpEntity<Location>(
				location, header);
		ResponseEntity<List<CampaignLocation>> campaignList = restTemplate
				.exchange(ApplicationConstants.CAMPAIGN_LOCATION_READ, HttpMethod.POST, locationEntity,
						new ParameterizedTypeReference<List<CampaignLocation>>() {});
		logger.info("location details : " + campaignList.getBody());
		return campaignList.getBody();
		
	}
	
	public List<SubscriberLocation> getSubscriberByLocation(String locationId) {
		logger.debug("get subscriber by location " + locationId );
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		ResponseEntity<List<SubscriberLocation>> subscribeList = restTemplate
				.exchange(ApplicationConstants.CAMPAIGN_DISPLAY_READ_BY_ID+"?locationRefId="+locationId, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<SubscriberLocation>>() {});
		logger.debug("location details : " + subscribeList.getBody());
		return subscribeList.getBody();
		
	}
	
	
	public SubscriberLocation createSubscriberByLocation(SubscriberLocation subscriberLocation) {
		logger.debug("Create subscriber location");
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<SubscriberLocation> entity = new HttpEntity<SubscriberLocation>(
				subscriberLocation, header);
		ResponseEntity<SubscriberLocation> subscriber = restTemplate
				.exchange(ApplicationConstants.CAMPAIGN_CREATE_LOC_DISPLAY, HttpMethod.POST, entity,
						SubscriberLocation.class);
		logger.debug("Create subscriber details : " + subscriber.getBody());
		return subscriber.getBody();
		
	}
	
}
