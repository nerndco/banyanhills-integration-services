/**
 *
 */
package campaign.api.gateway.service;

import campaign.api.gateway.dto.AccessToken;
import campaign.api.gateway.dto.Campaign;
import campaign.api.gateway.dto.xibo.*;
import campaign.api.gateway.exception.CampaignManagementException;
import campaign.api.gateway.util.LoggingResponseErrorHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class XiboIntegrationService {

    RestTemplate restTemplate = new RestTemplate();
    @Autowired
    Environment environment;

    @Value("${xibo.api.upload}")
    private String xibo_api_upload;

    @Value("${xibo.api.authorize.token}")
    private String xibo_api_authorize_token;

    @Value("${xibo.api.layout}")
    private String xibo_api_layout;

    @Value("${xibo.api.media-content}")
    private String xibo_api_media_content;

    @Value("${xibo.api.display}")
    private String xibo_api_display;

    @Value("${xibo.api.library.media}")
    private String xibo_api_library_media;

    @Value("${xibo.api.campaign}")
    private String xibo_api_campaign;

    @Value("${xibo.api.schedule}")
    private String xibo_api_schedule;

    @Value("${xibo.api.library.name}")
    private String xibo_api_library_name;

    @Value("${xibo.api.playlist.widget.delete}")
    private String xibo_api_playlist_widget_delete;

    @Value("${xibo.api.playlist.library.assign}")
    private String xibo_api_playlist_library_assign;

    @Value("${xibo.api.event}")
    private String xibo_api_event;

    @Value("${xibo.api.deleteevent}")
    private String xibo_api_deleteevent;

    @Value("${xibo.api.playlist.widget.update}")
    private String xibo_api_playlist_widget_update;

    @Value("${xibo.api.copyLayout}")
    private String xibo_api_copyLayout;

    @Value("${xibo.api.getLayout}")
    private String xibo_api_getLayout;

    @Value("${xibo.api.deleteLayout}")
    private String xibo_api_deleteLayout;

    @Value("${xibo.api.tidyLibrary}")
    private String xibo_api_tidyLibrary;

    @Value("${xibo.client.id}")
    private String xibo_client_id;

    @Value("${xibo.client.secret}")
    private String xibo_client_secret;


    private static final Logger logger = LoggerFactory.getLogger(XiboIntegrationService.class);

    public AccessToken getAccessToken() throws CampaignManagementException {
        logger.trace("Get Access token");
        AccessToken token_json = null;
        ResponseEntity<String> accessToken = null;

        MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();

        logger.debug("Get Access token : client_id " + xibo_client_id + " client_secret : " + xibo_client_secret);
        params.add("client_id", xibo_client_id);
        params.add("client_secret", xibo_client_secret);
        params.add("grant_type", "client_credentials");

        HttpHeaders header = new HttpHeaders();
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(params, header);

        logger.debug("URL " + xibo_api_authorize_token);
        try {
            accessToken = restTemplate.exchange(xibo_api_authorize_token, HttpMethod.POST, entity,
                    String.class);
        } catch (Exception e) {
            logger.error("Error in getAccessToken", e);
            logger.debug("Try one more time to get access token");
            try {
                accessToken = restTemplate.exchange(xibo_api_authorize_token, HttpMethod.POST, entity,
                        String.class);
            } catch (Exception ex) {
                logger.error("Error in second try of getAccessToken", ex);
                throw new CampaignManagementException("Error in getAccessToken. " + ex.getMessage());
            }
        }

        if (accessToken != null) {
            logger.debug("accessToken " + accessToken.getBody());
            Gson gson = new Gson();
            token_json = gson.fromJson(accessToken.getBody(), AccessToken.class);
        } else {
            logger.error("accessToken is null");
        }

        return token_json;
    }

    public List<Layout> getLayout() throws Exception {
        String accessToken = getAccessToken().getAccess_token();
        logger.debug("Access Token " + accessToken);

        ResponseEntity<String> response = restTemplate.exchange(xibo_api_layout
                + accessToken + "&length=200&embed=regions,playlists,widgets", HttpMethod.GET, null, String.class);

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        Type type = new TypeToken<List<Layout>>() {
        }.getType();
        List<Layout> layouts = gson.fromJson(response.getBody(), type);

        logger.debug("Respose : " + response);
        return layouts;
    }

    public Layout getRegions(Long layoutId) {
        List<Layout> layouts = null;
        try {
            String accessToken = getAccessToken().getAccess_token();
            ResponseEntity<String> response = restTemplate.exchange(xibo_api_layout
                            + accessToken + "&layoutId=" + layoutId
                            + "&embed=regions,playlists,widgets", HttpMethod.GET, null,
                    String.class);

            if (response != null) {
                logger.debug(response.getBody());
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
                Type type = new TypeToken<List<Layout>>() {
                }.getType();

                layouts = gson.fromJson(response.getBody(), type);

                if (layouts != null && layouts.size() > 0)
                    return layouts.get(0);
                else
                    return null;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Error in getRegions", ex);
            return null;
        }
    }


    public String getMediaContent(Long mediaId) throws Exception {
        String accessToken = getAccessToken().getAccess_token();
        String url = xibo_api_media_content + mediaId;
        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);

        HttpEntity<String> assignMedia = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, assignMedia,
                String.class);
        logger.debug(response.getBody());
        return response.getBody();
    }

    public Layout getDisplay(Long displayId) throws Exception {
        logger.debug("Get xibo displays : " + displayId);
        String accessToken = getAccessToken().getAccess_token();
        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);

        MultiValueMap<String, String> createForm = new LinkedMultiValueMap<String, String>();
        createForm.add("name", displayId + "");

        logger.debug("Get xibo displays url : " + xibo_api_display);
        HttpEntity<MultiValueMap<String, String>> assignMedia = new HttpEntity<MultiValueMap<String, String>>(
                createForm, header);
        ResponseEntity<String> response = restTemplate.exchange(xibo_api_display, HttpMethod.GET, assignMedia,
                String.class);
        logger.debug("get display : " + response.getBody());
        return null;
    }

    public Media getMedia(Long mediaId) throws CampaignManagementException {
        String accessToken = getAccessToken().getAccess_token();
        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);

        HttpEntity<String> mediaEntity = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(xibo_api_library_media + mediaId,
                HttpMethod.GET, mediaEntity, String.class);
        logger.debug("Respose : " + response);

        Gson gson = new Gson();

        Type type = new TypeToken<List<Media>>() {
        }.getType();

        List<Media> media = gson.fromJson(response.getBody(), type);

        return media.get(0);
    }

    public void createCampaign(Campaign campaign) throws Exception {
        String accessToken = getAccessToken().getAccess_token();

        logger.debug("Access Token " + accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        MultiValueMap<String, String> createForm = new LinkedMultiValueMap<String, String>();
        createForm.add("name", campaign.getName());

        HttpEntity<MultiValueMap<String, String>> createCampaign = new HttpEntity<MultiValueMap<String, String>>(
                createForm, header);
        ResponseEntity<String> response = restTemplate.exchange(xibo_api_campaign,
                HttpMethod.POST, createCampaign, String.class);
        logger.debug("Respose : " + response);
    }

    public String scheduleLayout(XiboSchedule schedule, String accessToken)
            throws ParseException, CampaignManagementException {
        ResponseEntity<String> response = null;
        try {
            if (accessToken == null || "".equals(accessToken)) {
                accessToken = getAccessToken().getAccess_token();
            }
            HttpHeaders header = new HttpHeaders();
            header.set("Authorization", "Bearer " + accessToken);
            header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            SimpleDateFormat source = new SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date stdtTemp = source.parse(schedule.getFromDt());
            Date todtTemp = source.parse(schedule.getToDt());
            Date stdtlnkTemp = source.parse(schedule.getFromDtLink());
            Date todtlnkTemp = source.parse(schedule.getToDtLink());

            SimpleDateFormat dfrmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            String fromDt = dfrmt.format(stdtTemp);
            String fromDtLink = dfrmt.format(stdtlnkTemp);
            String toDt = dfrmt.format(todtTemp);
            String toDtLink = dfrmt.format(todtlnkTemp);
            MultiValueMap<String, String> scheduleForm = new LinkedMultiValueMap<String, String>();
            scheduleForm.add("eventTypeId", schedule.getEventTypeId());

            for (String displayGroupId : schedule.getDisplayGroupIds()) {
                scheduleForm.add("displayGroupIds[]", displayGroupId);
            }

            scheduleForm.add("fromDt", fromDt);
            scheduleForm.add("fromDtLink", fromDtLink);
            scheduleForm.add("toDt", toDt);
            scheduleForm.add("toDtLink", toDtLink);
            scheduleForm.add("campaignId", String.valueOf(schedule.getCampaignId()));
            scheduleForm.add("isPriority", schedule.getIsPriority().toString());
            scheduleForm.add("displayOrder", schedule.getDisplayOrder().toString());

            logger.debug("fromDt : " + fromDtLink + " : to dtLink  : " + toDtLink);
            if (schedule.getIsRecurring()) {

                Date recurrTemp = source.parse(schedule.getRecurrenceRange());
                String recurrDt = dfrmt.format(recurrTemp);

                Date recurrTemplnk = source.parse(schedule.getRecurrenceRangeLink());
                String recurrDtlnk = dfrmt.format(recurrTemplnk);

                scheduleForm.add("recurrenceType", schedule.getRecurrenceType());
                scheduleForm.add("recurrenceDetail", schedule.getRecurrenceDetail());
                scheduleForm.add("recurrenceRange", recurrDt);
                scheduleForm.add("recurrenceRangeLink", recurrDtlnk);
            }

            HttpEntity<MultiValueMap<String, String>> createCampaign = new HttpEntity<MultiValueMap<String, String>>(
                    scheduleForm, header);
            if (schedule.getEventId() != null) {
                //				restTemplate.getMessageConverters().add( new FormHttpMessageConverter() );
                xibo_api_schedule = xibo_api_schedule + "/" + schedule.getEventId();

                response = restTemplate.exchange(xibo_api_schedule,
                        HttpMethod.PUT, createCampaign, String.class);
            } else {
                response = restTemplate.exchange(xibo_api_schedule,
                        HttpMethod.POST, createCampaign, String.class);
            }
        } catch (Exception e) {
            logger.error("Error in schedule layout", e);
            throw new CampaignManagementException("Error in schedule layout." + e.getMessage());
        }
        logger.debug("Response of schedule layout: " + response);
        return response.getBody();
    }

    public UploadResponse uploadContent(MultipartFile file, Long playListId, String accessToken)
            throws IOException, CampaignManagementException {
        logger.debug("uploadContent starts. Params: Accesstoken:" + accessToken + ", playListId:" + playListId + ", multipartFile:" + file);

        if (accessToken == null || "".equals(accessToken)) {
            accessToken = getAccessToken().getAccess_token();
        }

        UploadResponse uploadResponse = null;
        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.set("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
        header.set("Content-Disposition", new Date().getTime() + "_" + file.getOriginalFilename());
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();

        Resource res = new ByteArrayResource(file.getBytes()) {
            @Override
            public String getFilename() {

                return (new Date().getTime() + "_" + file.getOriginalFilename());
            }
        };
        params.add("files[0]", res);
        params.add("name[0]", new Date().getTime() + "_" + file.getOriginalFilename());
        params.add("playlistId", playListId);

        HttpEntity<MultiValueMap<String, Object>> upload = new HttpEntity<MultiValueMap<String, Object>>(
                params, header);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(xibo_api_upload,
                    HttpMethod.POST, upload, String.class);
            logger.debug("uploadMedia response from xibo api:" + response);
        } catch (Exception e) {
            logger.error("Error in upload media", e);
            throw new CampaignManagementException("Error in upload media." + e.getMessage());
        }
        if (response != null) {
            Gson gson = new Gson();
            uploadResponse = gson.fromJson(response.getBody(), UploadResponse.class);
        } else {
            logger.error("upload media response is null");
        }
        logger.debug("uploadContent ends.");
        return uploadResponse;
    }

    public List<Media> getMediaId(String mediaName) throws Exception {
        String accessToken = getAccessToken().getAccess_token();
        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> mediaEntity = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(
                xibo_api_library_name + mediaName, HttpMethod.GET, mediaEntity, String.class);
        logger.debug("Respose : " + response);

        Gson gson = new Gson();

        Type type = new TypeToken<List<Media>>() {
        }.getType();

        List<Media> media = gson.fromJson(response.getBody(), type);

        return media;
    }

    public void deleteMedia(Long widgetId, String accessToken) throws CampaignManagementException {
        ResponseEntity<String> response = null;

        if (accessToken == null || "".equals(accessToken)) {
            accessToken = getAccessToken().getAccess_token();
        }
        logger.debug("Access Token " + accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity deleteMedia = new HttpEntity<>(header);
        try {
            response = restTemplate.exchange(xibo_api_playlist_widget_delete + widgetId,
                    HttpMethod.DELETE, deleteMedia, String.class);
        } catch (Exception e) {
            logger.error("Error in deleteing widget", e);
            throw new CampaignManagementException("Error in deleteing widget" + e.getMessage());
        }

        logger.debug("Respose of deleteMedia: " + response);
    }

    public void assignMedia(Long playListId, Long mediaId, Long widgetId, String accessToken)
            throws CampaignManagementException {
        logger.debug("In assignMedia.Params-- mediaId: " + mediaId + ",widgetId: " + widgetId + ", accessToken:" + accessToken);

        if (accessToken == null || "".equals(accessToken)) {
            accessToken = getAccessToken().getAccess_token();
            logger.debug("Got new Access Token " + accessToken);
        }

        if (mediaId != null && widgetId != null) {

            ResponseEntity<String> response = null;
            HttpHeaders header = new HttpHeaders();
            header.set("Authorization", "Bearer " + accessToken);
            header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

            MultiValueMap<String, Integer> assign = new LinkedMultiValueMap<String, Integer>();
            assign.add("media[0]", Integer.parseInt(mediaId.toString()));

            HttpEntity<MultiValueMap<String, Integer>> assignMedia = new HttpEntity<MultiValueMap<String, Integer>>(
                    assign, header);

            try {
                response = restTemplate.exchange(xibo_api_playlist_library_assign
                        + playListId, HttpMethod.POST, assignMedia, String.class);
            } catch (Exception ex) {
                logger.error("Error in assign media", ex);
                throw new CampaignManagementException("Error in assign media" + ex.getMessage());
            }
            logger.debug("Response of assignMedia : " + response);
        }
    }

    public Events getEvents(ListEventRequest listEventRequest) throws Exception {
        String accessToken = getAccessToken().getAccess_token();

        Long startTime = listEventRequest.getFrom().getTime() / 1000;
        Long endTime = listEventRequest.getTo().getTime() / 1000;

        startTime = startTime * 1000;
        endTime = endTime * 1000;
        xibo_api_event += "?access_token=" + accessToken + "&displayGroupIds[]=" + listEventRequest.getDisplayGroupIds().get(0)
                + "&from=" + startTime + "&to=" + endTime;
        ResponseEntity<String> response = restTemplate.exchange(xibo_api_event, HttpMethod.GET, null,
                String.class);
        logger.debug(response.getBody());
        Gson gson = new Gson();
        Events resp = gson.fromJson(response.getBody(), Events.class);
        return resp;
    }

    public void deleteEvents(Long eventId) throws Exception {
        String accessToken = getAccessToken().getAccess_token();
        logger.debug("Access Token " + accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);

        HttpEntity deleteEvent = new HttpEntity(header);
        String url = xibo_api_deleteevent + eventId;
        logger.debug(url);
        restTemplate.exchange(url, HttpMethod.DELETE, deleteEvent, Void.class);
    }

    public Layout getLayout(String id, String accessToken) throws CampaignManagementException {

        if (accessToken == null || "".equals(accessToken)) {
            accessToken = getAccessToken().getAccess_token();
            logger.debug("Got new Access Token " + accessToken);
        }

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> layoutEntity = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(
                xibo_api_getLayout + id + "&embed=regions,playlists,widgets&length=2000", HttpMethod.GET, layoutEntity, String.class);
        logger.debug("Response : " + response);

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

        Type type = new TypeToken<List<Layout>>() {
        }.getType();
        List<Layout> layouts = gson.fromJson(response.getBody(), type);
        return layouts.get(0);
    }

    public List<Layout> getLayouts(String name, String accessToken) throws CampaignManagementException {
        logger.debug("Getting Layouts: " + name);

        accessToken = validateAccessToken(accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> layoutEntity = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(
                xibo_api_getLayout + "&embed=regions,playlists,widgets&length=2000&layout=" + name,
                HttpMethod.GET, layoutEntity, String.class);

        logger.debug("Get Layouts Response : " + response.getBody());

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        Type type = new TypeToken<List<Layout>>() {
        }.getType();
        List<Layout> layouts = gson.fromJson(response.getBody(), type);
        return layouts;
    }

    public void deleteLayout(Long id, String accessToken) throws CampaignManagementException {
        logger.debug("Deleting Layout: " + id);

        accessToken = validateAccessToken(accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> layoutEntity = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(xibo_api_deleteLayout + id, HttpMethod.DELETE, layoutEntity, String.class);

        logger.debug("Delete Layout Response : " + response.getStatusCode());
    }

    public void libraryTidy(String accessToken) throws CampaignManagementException {
        logger.debug("Calling Library Tidy");

        accessToken = validateAccessToken(accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<String> httpEntity = new HttpEntity<String>(header);
        ResponseEntity<String> response = restTemplate.exchange(xibo_api_tidyLibrary, HttpMethod.DELETE, httpEntity, String.class);

        logger.debug("Library Tidy Response : " + response.getStatusCode());
    }

    private String validateAccessToken(String accessToken) throws CampaignManagementException {
        if (accessToken == null || "".equals(accessToken)) {
            accessToken = getAccessToken().getAccess_token();
            logger.debug("Got new Access Token " + accessToken);
        }

        return accessToken;
    }

    public Layout copyLayout(String layoutid, String accessToken) throws CampaignManagementException {
        logger.debug("copyLayout layoutid: " + layoutid + ", accessToken:" + accessToken);

        accessToken = validateAccessToken(accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        MultiValueMap<String, String> assign = new LinkedMultiValueMap<String, String>();
        assign.add("layoutId", layoutid);
        assign.add("name", new Date().getTime() + "_" + layoutid);
        assign.add("copyMediaFiles", "0");
        logger.debug("copyLayout Params: " + assign);

        HttpEntity<MultiValueMap<String, String>> copyLayout = new HttpEntity<MultiValueMap<String, String>>(assign, header);

        logger.debug("copyLayout url: " + xibo_api_copyLayout);
        try {
//            ResponseEntity<Layout> response = restTemplate.exchange(
//                    xibo_api_copyLayout + layoutid, HttpMethod.POST, copyLayout, Layout.class);
            restTemplate.setErrorHandler(new LoggingResponseErrorHandler());
            ResponseEntity<String> responseStr = restTemplate.exchange(
                    xibo_api_copyLayout + layoutid, HttpMethod.POST, copyLayout, String.class);
            if (responseStr != null) {
                logger.debug("Response : " + responseStr.getBody());
                Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
                Layout layout = gson.fromJson(responseStr.getBody(), Layout.class);
                return layout;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("Error in copy layout", e);
            throw new CampaignManagementException("Error in copy layout" + e.getMessage());
        }
    }

    public String updateWidget(Long widgetId, String newFileName) throws CampaignManagementException {
        String accessToken = getAccessToken().getAccess_token();
        logger.debug("updateWidget Access Token " + accessToken);

        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + accessToken);
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        MultiValueMap<String, String> createForm = new LinkedMultiValueMap<String, String>();
        createForm.add("name", newFileName);

        HttpEntity<MultiValueMap<String, String>> createCampaign = new HttpEntity<MultiValueMap<String, String>>(
                createForm, header);

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(
                    xibo_api_playlist_widget_update + widgetId, HttpMethod.PUT, createCampaign, String.class);
            logger.debug("updateWidget Respose : " + response);
        } catch (RestClientException e) {
            e.printStackTrace();
            logger.error("Error in updateWidgte", e);
        }
        return response != null ? response.getBody() : null;
    }

    public UploadResponse uploadContent(MultipartFile file, Long playListId, Long widgetId, String oldMediaId, String token)
            throws IOException, CampaignManagementException {
        if (token == null || "".equals(token)) {
            token = getAccessToken().getAccess_token();
            logger.debug("Got new Access Token " + token);
        }
        HttpHeaders header = new HttpHeaders();
        header.set("Authorization", "Bearer " + token);
        header.set("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
        header.set("Content-Disposition", file.getName());
        header.set("filename", new Date().getTime() + file.getOriginalFilename());
        header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();

        Resource res = new ByteArrayResource(file.getBytes()) {
            @Override
            public String getFilename() {
                return (new Date().getTime() + "_" + file.getOriginalFilename());
            }
        };
        params.add("files[0]", res);
        params.add("name[0]", new Date().getTime() + "_" + file.getOriginalFilename());
        params.add("widgetId", widgetId);
        params.add("oldMediaId", oldMediaId);

        HttpEntity<MultiValueMap<String, Object>> upload = new HttpEntity<MultiValueMap<String, Object>>(
                params, header);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(xibo_api_upload,
                    HttpMethod.POST, upload, String.class);
            logger.debug("uploadContent Respose : " + response);
        } catch (RestClientException e) {
            logger.error("Error in uploadContent", e);
        }
        UploadResponse uploadResponse = null;
        if (response != null) {
            logger.debug("uploadContent response:::" + response.getBody());
            Gson gson = new Gson();
            uploadResponse = gson.fromJson(response.getBody(),
                    UploadResponse.class);
            logger.debug("uploadContent response object:::" + uploadResponse);
        }
        return uploadResponse;
    }
}
