/**
 * 
 */
package campaign.api.gateway.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import campaign.api.gateway.dto.ApproverRequest;
import campaign.api.gateway.dto.ApproverResponse;
import campaign.api.gateway.dto.mobile.Brand;
import campaign.api.gateway.dto.mobile.BrandsMetaData;
import campaign.api.gateway.dto.mobile.NotesRequest;
import campaign.api.gateway.dto.mobile.NotesResponse;
import campaign.api.gateway.dto.mobile.RetailerMetaData;
import campaign.api.gateway.dto.mobile.SubscriberRetailerFollowUnFollow;
import campaign.api.gateway.dto.mobile.UserDetailRequest;
import campaign.api.gateway.dto.mobile.UserDetailsResponse;
import campaign.api.gateway.dto.thirdparty.EMLoginResponse;
import campaign.api.gateway.dto.thirdparty.EclipseMediaLogin;
import campaign.api.gateway.util.ApplicationConstants;

import com.google.gson.Gson;

/**
 * @author Kritika.Srivastava
 *
 */
@Service
public class EclipseMediaIntegrationService {
	RestTemplate restTemplate = new RestTemplate();
	@Autowired
	Environment environment;
	@Autowired
	SubscriberIntegrationService subscriberService;

	static final Logger logger = LoggerFactory
			.getLogger(EclipseMediaIntegrationService.class);

	public ApproverResponse getApprover(String token) {

		Gson gson = new Gson();

		ApproverRequest request = new ApproverRequest(7, "3");
		HttpHeaders header = new HttpHeaders();
		header.set("X-Auth-Token", token);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<ApproverRequest> entity = new HttpEntity<ApproverRequest>(
				request, header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.APPROVER_LIST, HttpMethod.POST, entity,
				String.class);

		ApproverResponse approverResp = gson.fromJson(response.getBody(),
				ApproverResponse.class);

		logger.debug("Respose : " + response.getBody());
		return approverResp;
	}

	public NotesResponse getNotes(NotesRequest request, String token) {
		logger.info("Notes req service : " + token);
		Gson gson = new Gson();
		HttpHeaders header = new HttpHeaders();
		header.set("X-Auth-Token", token);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<NotesRequest> entity = new HttpEntity<NotesRequest>(request,
				header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.ECLIPSE_GET_OFFER_NOTES, HttpMethod.POST,
				entity, String.class);

		NotesResponse notesResp = gson.fromJson(response.getBody(),
				NotesResponse.class);

		logger.info("Respose : " + response.getBody());
		return notesResp;
	}

	public UserDetailsResponse getUserDetails(UserDetailRequest request,
			String token) {
		/*
		 * String token = getLogin();
		 */
		HttpHeaders header = new HttpHeaders();
		header.set("X-Auth-Token", token);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<UserDetailRequest> entity = new HttpEntity<UserDetailRequest>(
				request, header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.ECLIPSE_GET_USER_PROFILE, HttpMethod.POST,
				entity, String.class);

		Gson gson = new Gson();
		UserDetailsResponse userResponse = gson.fromJson(response.getBody(),
				UserDetailsResponse.class);

		logger.debug("Respose : " + response.getBody());

		return userResponse;
	}

	/*
	  public String getLogin() { EclipseMediaLogin login = new
	  EclipseMediaLogin(); login.setUsername("adam");
	  login.setPassword("admin"); HttpHeaders header = new HttpHeaders();
	  header.setContentType(MediaType.APPLICATION_JSON);
	  HttpEntity<EclipseMediaLogin> entitylogin = new
	  HttpEntity<EclipseMediaLogin>( login, header);
	  
	  ResponseEntity<String> loginResp = restTemplate.exchange(
	  ApplicationConstants.ECLIPSE_LOGIN_DETAILS, HttpMethod.POST, entitylogin,
	  String.class); Gson gson = new Gson();
	  
	  EMLoginResponse token_json = gson.fromJson(loginResp.getBody(),
	  EMLoginResponse.class); return token_json.getMetadata().getToken();
	  
	 }
	 */
	public RetailerMetaData getRetailerDetails(Long retailerID) {
		String token = getToken();

		HttpHeaders header = new HttpHeaders();
		header.set("X-Auth-Token", token);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		@SuppressWarnings("unchecked")
		HttpEntity entity = new HttpEntity(header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.ECLIPSE_GET_RETAILER_DETAIL + "/"
						+ retailerID, HttpMethod.POST, entity, String.class);
		logger.info("retailer detail : " + response.getBody());
		Gson gson = new Gson();
		RetailerMetaData retailerDetail = gson.fromJson(response.getBody(),
				RetailerMetaData.class);

		return retailerDetail;

	}

	public List<Brand> getBrands(Integer userId, String timeStamp) {

		String token = getToken();

		HttpHeaders header = new HttpHeaders();
		header.set("X-Auth-Token", token);
		header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<?> entity = new HttpEntity<String>(header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.ECLIPSE_GET_BRANDS + "?timeStamp="
						+ timeStamp, HttpMethod.GET, entity, String.class);
		logger.info("retailer detail : " + response.getBody());
		Gson gson = new Gson();
		BrandsMetaData retailerDetail = gson.fromJson(response.getBody(),
				BrandsMetaData.class);

		List<Brand> listBrands = retailerDetail.getMetaData().getRetailers();
		List<Brand> updatedListBrands = new ArrayList<Brand>();
		SubscriberRetailerFollowUnFollow subscriberRetailerFollowUnFollow = null;
		for (Brand brand : listBrands) {
			logger.info("Brand before Update: " + brand.getId() + " : "
					+ brand.getFuf());
			try {
				subscriberRetailerFollowUnFollow = subscriberService
						.getFollowUnFollowBySubscriberIdAndRetailerId(userId, brand
								.getId().intValue());
				if (subscriberRetailerFollowUnFollow == null) {
					brand.setFuf(false);
				} else {
					brand.setFuf(!subscriberRetailerFollowUnFollow.getUnSubscribed());
				}
				updatedListBrands.add(brand);
			} catch (Exception ex) {
				logger.info("Exception : " + ex.getMessage());
				ex.printStackTrace();
			}
			logger.info("Brand After Update: " + brand.getId() + " : "
					+ brand.getFuf());
		}

		return updatedListBrands;
	}

	public String getToken() {
		EclipseMediaLogin login = new EclipseMediaLogin();
		login.setUsername("steve");
		login.setPassword("admin");
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<EclipseMediaLogin> entitylogin = new HttpEntity<EclipseMediaLogin>(
				login, header);

		ResponseEntity<String> loginResp = restTemplate.exchange(
				ApplicationConstants.ECLIPSE_LOGIN_DETAILS, HttpMethod.POST,
				entitylogin, String.class);
		Gson gson = new Gson();

		EMLoginResponse token_json = gson.fromJson(loginResp.getBody(),
				EMLoginResponse.class);
		String token = token_json.getMetadata().getToken();
		return token;
	}
}
