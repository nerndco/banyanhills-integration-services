/**
 * 
 */
package campaign.api.gateway.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import campaign.api.gateway.dto.Machine;
import campaign.api.gateway.dto.mobile.SubscriberRetailerFollowUnFollow;
import campaign.api.gateway.dto.segment.Segment;
import campaign.api.gateway.util.ApplicationConstants;

/**
 * @author Kritika.Srivastava
 *
 */
@Service
public class SubscriberIntegrationService {

	@Autowired
	private RestTemplate restTemplate;

	public List<Segment> getAllSegements() {
		ResponseEntity<List<Segment>> segmentData = restTemplate.exchange(
				ApplicationConstants.SEGMENT_READ, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Segment>>() {
				});

		List<Segment> segment = segmentData.getBody();
		return segment;
	}

	public Segment createSegment(Segment request) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Segment> httpEntity = new HttpEntity<Segment>(request, header);
		ResponseEntity<Segment> response = restTemplate
				.exchange(ApplicationConstants.SEGMENT_CREATE, HttpMethod.POST, httpEntity,
						Segment.class);
		return response.getBody();
	}


	public SubscriberRetailerFollowUnFollow createFollowUnFollow(SubscriberRetailerFollowUnFollow request) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<SubscriberRetailerFollowUnFollow> httpEntity = new HttpEntity<SubscriberRetailerFollowUnFollow>(request, header);
		ResponseEntity<SubscriberRetailerFollowUnFollow> response = restTemplate
				.exchange(ApplicationConstants.FOLLOW_UNFOLLOW, HttpMethod.POST, httpEntity,
						SubscriberRetailerFollowUnFollow.class);
		return response.getBody();
	}

	public SubscriberRetailerFollowUnFollow updateFollowUnFollow(SubscriberRetailerFollowUnFollow request) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<SubscriberRetailerFollowUnFollow> httpEntity = new HttpEntity<SubscriberRetailerFollowUnFollow>(request, header);
		ResponseEntity<SubscriberRetailerFollowUnFollow> response = restTemplate
				.exchange(ApplicationConstants.UPLOAD_FOLLOW_UNFOLLOW, HttpMethod.PUT, httpEntity,
						SubscriberRetailerFollowUnFollow.class);
		return response.getBody();
	}

	public List<SubscriberRetailerFollowUnFollow> searchFollowUnFollow(Integer retailerId, Integer subscriberId) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<String>(header);
		ResponseEntity<List<SubscriberRetailerFollowUnFollow>> response = restTemplate
				.exchange(ApplicationConstants.GET_FOLLOW_UNFOLLOW+"?retailerId="+retailerId+"&subscriberId="+subscriberId, HttpMethod.GET, httpEntity,
						new ParameterizedTypeReference<List<SubscriberRetailerFollowUnFollow>>() {});
		return response.getBody();
	}

	public SubscriberRetailerFollowUnFollow getFollowUnFollowBySubscriberIdAndRetailerId(Integer subscriberId, Integer retailerId) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> httpEntity = new HttpEntity<String>(header);
		ResponseEntity<SubscriberRetailerFollowUnFollow> response = restTemplate
				.exchange(ApplicationConstants.GET_FOLLOW_UNFOLLOW_BY_SUBSCRIBERID_AND_RETAILERID+"?retailerId="+retailerId+"&subscriberId="+subscriberId, HttpMethod.GET, httpEntity,
						SubscriberRetailerFollowUnFollow.class);
		return response.getBody();
	}


	public List<Machine> getAllSubscribers() {
		List<Machine> machines = null;
		ResponseEntity<List<Machine>> subscriberData = restTemplate.exchange(
				ApplicationConstants.SUBSCRIBER_READ, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Machine>>() {});
		if(subscriberData != null){
			machines = subscriberData.getBody();
		}
		return machines;
	}

	public List<Machine> getSubscriberByName(String displayName) {
		List<Machine> machines = null;
		ResponseEntity<List<Machine>> subscriberData = restTemplate.exchange(
				ApplicationConstants.SUBSCRIBER_BY_NAME+"?displayName="+displayName, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Machine>>() {});

		if(subscriberData != null){
			machines = subscriberData.getBody();
		}
		return machines;
	}

	public Machine getSubscriberById(String id) {
		Machine machine = null;
		ResponseEntity<Machine> subscriberData = restTemplate.exchange(
				ApplicationConstants.SUBSCRIBER_BY_ID+"?id="+id, HttpMethod.GET, null,
				Machine.class);
		if(subscriberData != null){
			machine = subscriberData.getBody();
		}
		return machine;
	}

	public List<Machine> getSubscriberByLocation(Double latitude, Double longitude) {
		List<Machine> machines = null;
		ResponseEntity<List<Machine>> subscriberData = restTemplate.exchange(
				ApplicationConstants.SUBSCRIBER_BY_LOCATION+"?latitude="+latitude+"&longitude="+longitude, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Machine>>() {});
		if(subscriberData != null){
			machines = subscriberData.getBody();
		}
		return machines;
	}

	public List<Machine> getSubscriberByLocationAndName(Double latitude, Double longitude, String displayName) {
		List<Machine> machines = null;
		ResponseEntity<List<Machine>> subscriberData = restTemplate.exchange(
				ApplicationConstants.SUBSCRIBER_BY_LOCNAME+"?latitude="+latitude+"&longitude="+longitude+"&displayName="+displayName, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Machine>>() {});
		if(subscriberData != null){
			machines = subscriberData.getBody();
		}
		return machines;
	}

	public Machine updateMachine(Machine machine) {
		HttpHeaders header = new HttpHeaders();
		header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Machine> entity = new HttpEntity<Machine>(machine, header);

		ResponseEntity<Machine> response = restTemplate.exchange(
				ApplicationConstants.SUBSCRIBER_UPDATE_MACHINE,
				HttpMethod.PUT, entity, Machine.class);

		System.out.println("Response " + response.getBody());
		return response.getBody();

	}

}
