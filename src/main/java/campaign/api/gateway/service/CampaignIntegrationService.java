/**
 * 
 */
package campaign.api.gateway.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import campaign.api.gateway.dto.Campaign;
import campaign.api.gateway.dto.CampaignRequest;
import campaign.api.gateway.dto.CampaignResp;
import campaign.api.gateway.dto.PromotionMapping;
import campaign.api.gateway.dto.State;
import campaign.api.gateway.dto.Text;
import campaign.api.gateway.dto.Type;
import campaign.api.gateway.dto.campaign.CreateCampaignData;
import campaign.api.gateway.dto.xibo.Layout;
import campaign.api.gateway.dto.xibo.Media;
import campaign.api.gateway.dto.xibo.Regions;
import campaign.api.gateway.dto.xibo.Widget;
import campaign.api.gateway.util.ApplicationConstants;

@Service
public class CampaignIntegrationService {
	static final Logger logger = LoggerFactory.getLogger(CampaignIntegrationService.class);

	@Autowired
	RestTemplate restTemplate;

	public List<Campaign> getAllCampaigns() {
		logger.info("get campaign service call starts");
		ResponseEntity<List<Campaign>> campaignList = restTemplate
				.exchange(ApplicationConstants.CAMPAIGN_READ, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Campaign>>() {});

		List<Campaign> campaigns = campaignList.getBody();
		logger.info("get campaign service call ends");

		return campaigns;
	}

	public Campaign createCampaign(CreateCampaignData request) {
		logger.info("create campaign service call starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<CreateCampaignData> entity = new HttpEntity<CreateCampaignData>(
				request, header);
		ResponseEntity<Campaign> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_CREATE, HttpMethod.POST,entity, Campaign.class);
		logger.info("create campaign service call ends");

		return response.getBody();

	}
	public State createState(State request) {
		logger.info("create state service call starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<State> entity = new HttpEntity<State>(
				request, header);
		ResponseEntity<State> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_STATE_CREATE, HttpMethod.POST,entity, State.class);
		logger.info("create state service call ends");

		return response.getBody();

	}
	public Type createType(Type request) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Type> entity = new HttpEntity<Type>(
				request, header);
		ResponseEntity<Type> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_TYPE_CREATE, HttpMethod.POST,entity, Type.class);
		return response.getBody();

	}
	public List<Type> getTypes() {
		ResponseEntity<List<Type>> typesList = restTemplate
				.exchange(ApplicationConstants.TYPE_READ, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Type>>() {});
		return typesList.getBody();

	}

	public List<State> getState() {
		ResponseEntity<List<State>> stateList = restTemplate
				.exchange(ApplicationConstants.STATE_READ, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<State>>() {});
		return stateList.getBody();

	}

	public Campaign updateCampaign(CreateCampaignData request) {
		logger.info("update campaign service call starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<CreateCampaignData> entity = new HttpEntity<CreateCampaignData>(request, header);
		ResponseEntity<Campaign> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_UPDATE, HttpMethod.PUT, entity,
				Campaign.class);
		logger.info("update campaign service call ends");

		return response.getBody();

	}

	public Campaign update(Campaign request) {
		logger.info("update campaign service call starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Campaign> entity = new HttpEntity<Campaign>(request, header);
		ResponseEntity<Campaign> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_UPDATE, HttpMethod.PUT, entity,
				Campaign.class);
		logger.info("update campaign service call ends");

		return response.getBody();

	}

	public String deleteCampaign(Campaign request) {
		logger.info("delete campaign service call starts");

		HttpEntity<Campaign> entity = new HttpEntity<Campaign>(request);
		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_DELETE, HttpMethod.DELETE,
				entity, String.class);
		logger.info("delete campaign service call ends");

		return response.getBody();

	}

	public void createCampaignPromotionMapping(PromotionMapping request) {
		logger.info(" create campaign promotion mapping service call starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<PromotionMapping> entity = new HttpEntity<PromotionMapping>(request, header);
		ResponseEntity<PromotionMapping> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_PROMOTION_CREATE, HttpMethod.POST,entity,
				PromotionMapping.class);
		logger.info(" create campaign promotion mapping service call ends");

	}


	public Campaign searchCampaignById(Long id) {
		logger.info(" search campaign  by id starts " + id);
		//TODO: Include isDeleted=false in search query
		//		ResponseEntity<Campaign> response = restTemplate.exchange(
		//				ApplicationConstants.CAMPAIGN_GET_BY_ID+"?id="+id+"&isDeleted=false", HttpMethod.GET, null,
		//				new ParameterizedTypeReference<Campaign>() {});
		ResponseEntity<Campaign> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_GET_BY_ID+"?id="+id, HttpMethod.GET, null,
				new ParameterizedTypeReference<Campaign>() {});
		logger.info(" search campaign  by id ends");

		return response.getBody();


	}

	//	public List<Campaign> searchCampaign(Long id) {
	//		logger.info(" search campaign  by id starts");
	//
	//		ResponseEntity<List<Campaign>> response = restTemplate.exchange(
	//				ApplicationConstants.CAMPAIGN_SEARCH_BY_ID+"?id="+id+"&isDeleted=false", HttpMethod.GET, null,
	//				new ParameterizedTypeReference<List<Campaign>>() {});
	//		logger.info(" search campaign  by id ends");
	//
	//		return response.getBody();
	//
	//
	//	}

	public List<Layout> getLayout() {
		logger.info(" list all layouts");

		ResponseEntity<List<Layout>> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_LAYOUTS, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Layout>>() {});
		logger.info(" list all layouts ends");

		return response.getBody();


	}



	//	public List<Campaign> searchCampaignByState(Long id,Long approverId) {
	//		logger.info(" search campaign  by state starts");
	//
	//
	//		String uri = ApplicationConstants.CAMPAIGN_SEARCH_BY_ID+"?stateId="+id;
	//		ResponseEntity<List<Campaign>> response = restTemplate.exchange(uri
	//				, HttpMethod.GET, null,
	//				new ParameterizedTypeReference<List<Campaign>>() {});
	//		logger.info(" search campaign  by state ends");
	//
	//		return response.getBody();
	//
	//
	//	}


	public List<Campaign> getCampaignByState(Long id) {
		logger.info(" search campaign  by state starts");


		String uri = ApplicationConstants.CAMPAIGN_GET_BY_STATEID+"?stateId="+id;
		ResponseEntity<List<Campaign>> response = restTemplate.exchange(uri
				, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Campaign>>() {});
		logger.info(" search campaign  by state ends");

		return response.getBody();
	}

	public Campaign updateCampaignState(String stateName, Long cmpgnId) {
		logger.info(" updateCampaignState starts");
		Campaign campaign = null;
		ResponseEntity<Campaign> response = null;
		CampaignRequest request = new CampaignRequest();
		request.setId(cmpgnId);
		request.setStateName(stateName);
		
		HttpEntity<CampaignRequest> entity = new HttpEntity<CampaignRequest>(request);
		try{
			response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_UPDATE_STATE, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Campaign>() {});
		}catch(RestClientException e){
			logger.error("Error in updating campaign state", e);
		}
		if(response != null){
			campaign = response.getBody();
		}
		logger.info("updateCampaignState ends");
		return campaign;
	}

	public Layout getLayoutById(Long id) {
		logger.info("get layout by id");
		ResponseEntity<Layout> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_LAYOUT_BY_ID + "?id="+id, HttpMethod.GET, null,
				new ParameterizedTypeReference<Layout>() {});
		logger.info("get layout by id ends");
		return response.getBody();
	}

	public Layout updatelayout(Layout layout) {
		logger.info("update layout");
		HttpEntity<Layout> entity = new HttpEntity<Layout>(layout);

		ResponseEntity<Layout> response = restTemplate.exchange(
				ApplicationConstants.CAMPAIGN_UPDATE_LAYOUT, HttpMethod.PUT, entity,
				new ParameterizedTypeReference<Layout>() {});
		logger.info("update layout ends");
		return response.getBody();
	}

	public Widget getWidgetById(Long id) {
		logger.info("getWidgetById: " + id);
		Widget widget =  null;
		try{
			ResponseEntity<Widget> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_GET_WIDGET_BY_ID + "?id="+id, HttpMethod.GET, null,
					new ParameterizedTypeReference<Widget>() {});
			widget = response.getBody();
		}catch(RestClientException e){
			logger.error("Error in getWidgetById", e);
		}
		logger.info("get Widget by id ends");
		return widget;
	}

	public Widget updateWidget(Widget widget) {
		logger.info("update Widget");
		Widget widgetUpdated =  null;
		HttpEntity<Widget> entity = new HttpEntity<Widget>(widget);
		try{
			ResponseEntity<Widget> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_UPDATE_WIDGET, HttpMethod.PUT, entity,
					new ParameterizedTypeReference<Widget>() {});
			widgetUpdated = response.getBody();
		}
		catch(RestClientException e){
			logger.error("Error in updateWidget", e);
		}
		logger.info("update Widget ends");
		return widgetUpdated;
	}

	public Layout saveXlf(Long id) {
		logger.info("save xlf for layout");
		Layout layout = null;
		try{
			HttpEntity<Long> entity = new HttpEntity<Long>(id);

			ResponseEntity<Layout> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_SAVE_XLF, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Layout>() {});
			layout = response.getBody();
		}catch(RestClientException e){
			logger.error("Error in saveXlf", e);
		}
		catch(Exception e){
			logger.error("Error in saveXlf", e);
		}
		logger.info("save xlf for layout ends");
		return layout;
	}


	public Layout createLayout(Layout layout) {
		logger.info("create layout");
		Layout layoutNew = null;
		HttpEntity<Layout> entity = new HttpEntity<Layout>(layout);
		try{
			ResponseEntity<Layout> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_CREATE_LAYOUT, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Layout>() {});
			if(response != null){
				layoutNew = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in createLayout", e);
		}

		logger.info("create layout ends");
		return layoutNew;
	}


	public Media updateMedia(Media media) {
		logger.info("update media");
		Media mediaUpdated = null;
		HttpEntity<Media> entity = new HttpEntity<Media>(media);
		try{
			ResponseEntity<Media> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_UPDATE_MEDIA, HttpMethod.PUT, entity,
					new ParameterizedTypeReference<Media>() {});
			if(response != null){
				mediaUpdated = response.getBody();
			}

		}catch(RestClientException e){
			logger.error("Error in updateMedia", e);
		}
		logger.info("update media ends");
		return mediaUpdated;
	}

	public Layout cloneLayout(String layout) {
		logger.info("cloneLayout starts");
		HttpEntity<String> entity = new HttpEntity<String>(layout);
		Layout clonedLayout = null;
		try{	
			ResponseEntity<Layout> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_CLONE_LAYOUT, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Layout>() {});
			if(response != null){
				clonedLayout = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in cloneLayout", e);
		}
		logger.info("cloneLayout  ends");
		return clonedLayout;
	}


	public List<Regions> createRegion(List<Regions> layout) {
		logger.info("create regions");
		List<Regions> regions = null;
		HttpEntity<List<Regions>> entity = new HttpEntity<List<Regions>>(layout);
		try{
			ResponseEntity<List<Regions>> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_CREATE_REGION, HttpMethod.POST, entity,
					new ParameterizedTypeReference<List<Regions>>() {});
			if(response != null){
				regions = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in cloneLayout", e);
		}
		logger.info("create regions ends");
		return regions;
	}


	public Widget createWidget(Widget widget) {
		logger.info("create Widget");
		Widget widgetNew = null;
		HttpEntity<Widget> entity = new HttpEntity<Widget>(widget);
		try{
			ResponseEntity<Widget> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_CREATE_WIDGET, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Widget>() {});
			logger.info("create Widget api response"+response);
			if(response != null){
				widgetNew = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in createWidget", e);
		}
		logger.info("create Widget ends.");
		return widgetNew;
	}

	public List<Layout> getLayoutsByType(String type) {
		logger.info("getLayoutsByType starts");
		List<Layout> layouts = null;
		try{
			ResponseEntity<List<Layout>> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_GET_LAYOUTS_BY_TYPE + "?type=" + type, HttpMethod.GET, null, 
					new ParameterizedTypeReference<List<Layout>>() {});
			if(response != null){
				layouts = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in getLayoutsByType", e);
		}
		logger.info("getLayoutsByType ends");
		return layouts;
	}

	public String deleteWidgetById(String id) {
		logger.info("deleteWidgetById starts");
		String responseStr = null;
		try{
			ResponseEntity<String> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_DELETE_WIDGET_BY_ID + "?widgetId=" + id, HttpMethod.DELETE, null, 
					new ParameterizedTypeReference<String>() {});
			if(response != null){
				responseStr = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in deleteWidgetById", e);
		}
		logger.info("deleteWidgetById ends");
		return responseStr;
	}

	public CampaignResp saveHtml(Long layoutId) {
		logger.info("save Html starts");
		CampaignResp resp = new CampaignResp();
		HttpEntity<Long> entity = new HttpEntity<Long>(layoutId);
		try{
			ResponseEntity<CampaignResp> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_SAVE_HTML, HttpMethod.POST, entity,
					new ParameterizedTypeReference<CampaignResp>() {});
			if(response != null){
				resp = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in createLayout", e);
		}

		logger.info("save Html ends");
		return resp;
	}


	public String deleteLayout(String id) {
		logger.info("deleteLayout starts");
		String responseStr = null;
		try{
			ResponseEntity<String> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_DELETE_LAYOUT + "?layoutId=" + id, HttpMethod.DELETE, null, 
					new ParameterizedTypeReference<String>() {});
			if(response != null){
				responseStr = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in deleteLayout", e);
		}
		logger.info("deleteLayout ends");
		return responseStr;
	}

	public Widget saveTextContent(Text textRequest) {
		logger.info("save text starts");
		Widget widget = null;
		HttpEntity<Text> entity = new HttpEntity<Text>(textRequest);
		try{
			ResponseEntity<Widget> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_SAVE_TEXT, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Widget>() {});
			if(response != null){
				widget = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in saveText", e);
		}

		logger.info("save text ends");
		return widget;
	}

	public Layout resetLayout(String layoutId) {
		logger.info("resetLayout starts");
		Layout layout = null;
		HttpEntity<String> entity = new HttpEntity<String>(layoutId);
		try{
			ResponseEntity<Layout> response = restTemplate.exchange(
					ApplicationConstants.CAMPAIGN_RESET_LAYOUT, HttpMethod.POST, entity,
					new ParameterizedTypeReference<Layout>() {});
			if(response != null){
				layout = response.getBody();
			}
		}catch(RestClientException e){
			logger.error("Error in resetLayout", e);
		}

		logger.info("resetLayout ends. layout:" + layout);
		return layout;
	}

}
