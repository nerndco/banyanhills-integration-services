/**
 * 
 */
package campaign.api.gateway.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import campaign.api.gateway.dto.Channel;
import campaign.api.gateway.dto.DistributionInfo;
import campaign.api.gateway.util.ApplicationConstants;

/**
 * @author Kritika.Srivastava
 *
 */
@Service
public class DistributionServiceIntegration {

	@Autowired
	RestTemplate restTemplate;
	static final Logger logger = LoggerFactory.getLogger(DistributionServiceIntegration.class);


	public List<Channel> getAllChannels() {
		logger.info(" list channels starts");

		ResponseEntity<List<Channel>> channelData = restTemplate
				.exchange(ApplicationConstants.CHANNEL_READ, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Channel>>() {});

		List<Channel> channel = channelData.getBody();
		logger.info(" list channels ends");

		return channel;
	}

	public List<DistributionInfo> getAllDistributions(String tenantId) {
		logger.info(" get distribution starts");

		ResponseEntity<List<DistributionInfo>> distributionData = restTemplate
				.exchange(ApplicationConstants.DISTRIBUTION_READ+"?tenantId="+tenantId, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<DistributionInfo>>() {});

		List<DistributionInfo> distribution = distributionData.getBody();
		logger.info(" get distribution ends");

		return distribution;
	}

	public DistributionInfo createDistribution(DistributionInfo request) {
		logger.info(" create distribution starts");

		HttpHeaders header = new HttpHeaders();
		//header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<DistributionInfo> entity = new HttpEntity<DistributionInfo>(
				request, header);

		ResponseEntity<DistributionInfo> response = restTemplate.exchange(
				ApplicationConstants.DISTRIBUTION_CREATE, HttpMethod.POST, entity,
				DistributionInfo.class);
		/*ResponseEntity<DistributionInfo> response = restTemplate
				.postForEntity(ApplicationConstants.DISTRIBUTION_CREATE, request, DistributionInfo.class);
		 */
		logger.info(" create distribution ends");

		return response.getBody();
	}

	public DistributionInfo updateDistribution(DistributionInfo request) {
		logger.info(" update distribution starts");

		HttpHeaders header = new HttpHeaders();
		//header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<DistributionInfo> entity = new HttpEntity<DistributionInfo>(
				request, header);

		ResponseEntity<DistributionInfo> response = restTemplate.exchange(
				ApplicationConstants.DISTRIBUTION_UPDATE, HttpMethod.PUT, entity,
				DistributionInfo.class);

		logger.info(" update distribution ends");

		return response.getBody();
	}


	public Channel createChannel(Channel request) {
		HttpHeaders header = new HttpHeaders();
		//header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Channel> entity = new HttpEntity<Channel>(
				request, header);

		ResponseEntity<Channel> response = restTemplate.exchange(
				ApplicationConstants.CHANNEL_CREATE, HttpMethod.POST, entity,
				Channel.class);
		/*ResponseEntity<DistributionInfo> response = restTemplate
				.postForEntity(ApplicationConstants.DISTRIBUTION_CREATE, request, DistributionInfo.class);
		 */

		return response.getBody();
	}

	public List<DistributionInfo> searchDistributionInfo(Long campaignId) {
		logger.info(" search distribution by campaignid  starts" + campaignId);
		List<DistributionInfo> distribution = null;
		String url = ApplicationConstants.DISTRIBUTION_INFO_BY_CAMPAIGN_ID;
		if(campaignId > 0) {
			url = url + "?campaignId="+campaignId;
			ResponseEntity<List<DistributionInfo>> distributionData = restTemplate
					.exchange(url, HttpMethod.GET, null,
							new ParameterizedTypeReference<List<DistributionInfo>>() {});

			distribution = distributionData.getBody();
		}
		logger.info("search distribution by campaignid ends");
		return distribution;
	}

	public String scheduleCampaign(DistributionInfo request) {
		logger.info(" schedule distribution starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<DistributionInfo> entity = new HttpEntity<DistributionInfo>(
				request, header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.DISTRIBUTION_RUN, HttpMethod.POST, entity,
				String.class);

		logger.info(" schedule distribution ends");

		return response.getBody();
	}

	public String scheduleCampaignInfo(DistributionInfo request) {
		logger.info(" scheduleCampaignInfo distribution starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<DistributionInfo> entity = new HttpEntity<DistributionInfo>(
				request, header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.DISTRIBUTION_RUN_SCHEDULE, HttpMethod.POST, entity,
				String.class);

		logger.info(" scheduleCampaignInfo distribution ends");

		return response.getBody();
	}

	public String deleteDistributionInfoById(Long id) {
		logger.info(" deleteDistributionInfoById starts");

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Long> entity = new HttpEntity<Long>(id,header);

		ResponseEntity<String> response = restTemplate.exchange(
				ApplicationConstants.DISTRIBUTION_DELETE, HttpMethod.DELETE, entity,
				String.class);

		logger.info(" deleteDistributionInfoById ends");

		return response.getBody();
	}
}
