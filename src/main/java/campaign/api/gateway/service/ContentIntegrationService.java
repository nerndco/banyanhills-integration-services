/**
 * 
 */
package campaign.api.gateway.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import campaign.api.gateway.dto.content.Content;
import campaign.api.gateway.dto.content.FileInfo;
import campaign.api.gateway.util.ApplicationConstants;

/**
 * @author kritika.srivastava
 *
 */
@Service
public class ContentIntegrationService {

	@Autowired
	RestTemplate restTemplate;
	static final Logger logger = LoggerFactory.getLogger(ContentIntegrationService.class);

	public List<Content> getAllContents() {
		ResponseEntity<List<Content>> contentData = restTemplate
				.exchange(ApplicationConstants.CONTENT_READ, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Content>>() {});

		List<Content> contentList = contentData.getBody();
		return contentList;
	}

	public List<Content> getContentByPromotionId(Long promotionId) {
		logger.info("get content by promotion ref id");
		ResponseEntity<List<Content>> contentData = restTemplate
				.exchange(ApplicationConstants.CONTENT_SEARCH+"?promotionRefId="+promotionId, HttpMethod.GET, null,
						new ParameterizedTypeReference<List<Content>>() {});

		List<Content> contentList = contentData.getBody();
		return contentList;
	}

	public List<FileInfo> uploadContent(String fname){

		MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();

		Resource res = new ByteArrayResource(fname.getBytes()) {
			@Override
			public String getFilename() {

				return (fname);
			}
		};
		parts.add("files[0]", res);
		parts.add("name[0]", new Date().getTime()+"_"+fname);
		//		parts.add("name[0]", fname);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.set("Content-Disposition", new Date().getTime()+"_"+fname);
		//		headers.set("Content-Disposition", fname);

		FormHttpMessageConverter formConvertor = new FormHttpMessageConverter();
		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
		ResourceHttpMessageConverter fileConverter = new ResourceHttpMessageConverter();
		BufferedImageHttpMessageConverter imageConvertor = new BufferedImageHttpMessageConverter();

		restTemplate.getMessageConverters().add(stringHttpMessageConverter);
		restTemplate.getMessageConverters().add(formConvertor);
		restTemplate.getMessageConverters().add(fileConverter);
		restTemplate.getMessageConverters().add(imageConvertor);
		restTemplate.getMessageConverters().add(
				new MappingJackson2HttpMessageConverter());
		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(parts, headers);
		ResponseEntity<List<FileInfo>> contentData = restTemplate
				.exchange(ApplicationConstants.CONTENT_UPLOAD, HttpMethod.POST, request,
						new ParameterizedTypeReference<List<FileInfo>>() {});

		List<FileInfo> fileInfo = contentData.getBody();
		return fileInfo;
	}

	public List<FileInfo> uploadContent(MultipartFile file){
		logger.info("uploadContent");
		Resource res = null;
			try {
				res = new ByteArrayResource(file.getBytes()) {
					@Override
					public String getFilename() {

						return (new Date().getTime()+"_"+file.getOriginalFilename());
					}
				};
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		HttpHeaders header = new HttpHeaders();
		header.set("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
		header.set("Content-Disposition", file.getName());
		header.set("filename", new Date().getTime()+file.getOriginalFilename());
		header.set("tenantId","7"); // TODO: remove tenantId

		MultiValueMap<String, Object> params = new LinkedMultiValueMap<String, Object>();
		params.add("file", res);
		params.add("name", new Date().getTime()+"_"+file.getOriginalFilename());

		HttpEntity<MultiValueMap<String, Object>> upload = new HttpEntity<MultiValueMap<String, Object>>(
				params, header);
		FormHttpMessageConverter formConvertor = new FormHttpMessageConverter();
		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter();
		ResourceHttpMessageConverter fileConverter = new ResourceHttpMessageConverter();
		BufferedImageHttpMessageConverter imageConvertor = new BufferedImageHttpMessageConverter();

		restTemplate.getMessageConverters().add(stringHttpMessageConverter);
		restTemplate.getMessageConverters().add(formConvertor);
		restTemplate.getMessageConverters().add(fileConverter);
		restTemplate.getMessageConverters().add(imageConvertor);
		restTemplate.getMessageConverters().add(
				new MappingJackson2HttpMessageConverter());

		ResponseEntity<List<FileInfo>> contentData = restTemplate
				.exchange(ApplicationConstants.CONTENT_UPLOAD, HttpMethod.POST, upload,
						new ParameterizedTypeReference<List<FileInfo>>() {});

		return contentData.getBody();

		//		List<Content> contentList = contentData.getBody();
		//return contentList;
	}

}
