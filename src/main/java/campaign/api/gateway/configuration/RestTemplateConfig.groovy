package campaign.api.gateway.configuration

import campaign.api.gateway.util.LoggingResponseErrorHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class RestTemplateConfig {
    @Bean
    RestTemplate restTemplate() {
        final RestTemplate restTemplate = new RestTemplate()
        restTemplate.errorHandler = new LoggingResponseErrorHandler()
        return restTemplate
    }
}
