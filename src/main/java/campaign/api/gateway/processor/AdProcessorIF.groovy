package campaign.api.gateway.processor;

public interface AdProcessorIF {

	ProcessAdResponse processAdRequest(ProcessAdRequest request);
}
