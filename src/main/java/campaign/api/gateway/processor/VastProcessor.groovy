package campaign.api.gateway.processor

import groovy.util.logging.Slf4j

import org.apache.commons.lang3.StringUtils

@Slf4j
abstract class VastProcessor 
{
	protected ProcessAdResponse processVast(String vastXmlAsString, UUID requestId) {
		log.debug(vastXmlAsString)

		XmlSlurper xmlSlurper = new XmlSlurper()
		def xml = xmlSlurper.parseText(URLDecoder.decode(vastXmlAsString, 'UTF-8'))

		ProcessAdResponse adResponse = new ProcessAdResponse(requestId: requestId, proofOfPlayUrls: [])
		try {
			adResponse.adId = xml.Ad.@id?.text() as Long
			log.debug("Found Ad ID: ${adResponse.adId}")
		} catch (NumberFormatException e) {
			adResponse.adId = Math.floor(Math.random() * 1000)
			log.debug("Setting a random ad ID, this must be a test ad. Adid: ${adResponse.adId}")
		}

		def proofOfPlayUrl = ((xml.Ad.Wrapper.isEmpty() ? xml.Ad.InLine : xml.Ad.Wrapper).Creatives.Creative[0].Linear.TrackingEvents.Tracking.find {
			it.@event.text() == 'complete'
		}.text())

		log.debug("Found proof of play url: ${proofOfPlayUrl}")

		// modified this assertion - it is fine to have a blank proof of play
		// assert StringUtils.isNotBlank(proofOfPlayUrl)
		if (StringUtils.isNotBlank(proofOfPlayUrl)) {
			log.debug("Setting proof of play URL")
			adResponse.proofOfPlayUrls = adResponse.proofOfPlayUrls + new URL(proofOfPlayUrl)
		}

		log.debug("Adding impression urls to list of proof of play urls")

		(xml.Ad.Wrapper.isEmpty() ? xml.Ad.InLine : xml.Ad.Wrapper).Impression.each {
			if (it.text()) adResponse.proofOfPlayUrls = adResponse.proofOfPlayUrls + new URL(it.text())
		}

		log.debug("Current pop urls: ${adResponse.proofOfPlayUrls}")

		def mediaFile = (xml.Ad.InLine.isEmpty() ? null : xml.Ad.InLine.Creatives.Creative[0].Linear.MediaFiles.MediaFile.find {
			isSupportedType(it.@type.text())
		}.text())
		log.debug("MediaFile = ${mediaFile}")

		def mediaUrl = mediaFile ? new URL(mediaFile) : null
		log.debug("MediaURL = ${mediaUrl}")

		def vastAdTagUri = xml.Ad?.Wrapper?.VASTAdTagURI?.text()
		if (vastAdTagUri) {
			assert mediaUrl == null

			ProcessAdResponse subsequentAdResponse = processVast(new URL(vastAdTagUri).text, requestId)

			adResponse.proofOfPlayUrls = adResponse.proofOfPlayUrls + subsequentAdResponse.proofOfPlayUrls
			adResponse.mediaUrl = subsequentAdResponse.mediaUrl
		} else {
			adResponse.mediaUrl = mediaUrl
		}

		assert adResponse.mediaUrl != null

		log.debug("Returning adResponse: ${adResponse}")

		return adResponse
	}
	
	protected abstract boolean isSupportedType(String type);
}
