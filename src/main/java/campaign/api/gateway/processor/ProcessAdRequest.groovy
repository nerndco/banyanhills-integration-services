package campaign.api.gateway.processor

import org.springframework.beans.factory.annotation.Value

class ProcessAdRequest {
	String username
	String password
	
	String siteName
	String siteDomain
	String sitePage
	int sitePublisherAccountId
	int siteId
	
	String deviceIpAddress
	String deviceUserAgent
	String deviceZip
	
	String adTagUrl
}
