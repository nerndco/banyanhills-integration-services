package campaign.api.gateway.processor.rubicon

import feign.Headers
import feign.Param
import feign.RequestLine

@Headers([ 'Content-Type: application/json', 'User-Agent: {ua}' ])
interface ExchangeApiClient {

	@RequestLine('POST')
	Map<?, ?> post(@Param('ua') ua, Map<?, ?> body)
}
