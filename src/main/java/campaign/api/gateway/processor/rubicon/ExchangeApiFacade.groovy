package campaign.api.gateway.processor.rubicon

import java.util.Map

interface ExchangeApiFacade {
	Map<?, ?> postRequest(username, password, userAgent, request)
}
