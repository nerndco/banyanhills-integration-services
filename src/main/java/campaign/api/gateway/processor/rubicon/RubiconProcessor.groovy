package campaign.api.gateway.processor.rubicon

import campaign.api.gateway.processor.AdProcessorIF
import campaign.api.gateway.processor.ProcessAdRequest
import campaign.api.gateway.processor.ProcessAdResponse
import campaign.api.gateway.processor.VastProcessor
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
@Slf4j
class RubiconProcessor extends VastProcessor implements AdProcessorIF {

    @Autowired
    private ExchangeApiFacade exchangeApiFacade

    private static final List SUPPORTED_MIME_TYPES = [
//		'video/x-ms-wmv',
//		'video/x-flv',
'video/mp4'
    ]

    ProcessAdResponse processAdRequest(ProcessAdRequest adRequest) {
        UUID requestId = UUID.randomUUID()

        def request = [
                id    : requestId.toString(),
                imp   : [buildImp(requestId)],
                site  : [
                        name     : adRequest.siteName,
                        domain   : adRequest.siteDomain,
                        page     : adRequest.sitePage,
                        publisher: [
                                ext: [
                                        rp: [
                                                account_id: adRequest.sitePublisherAccountId
                                        ]
                                ]
                        ],
                        ext      : [
                                rp: [
                                        site_id: adRequest.siteId
                                ]
                        ]
                ],
                device: [
                        ip : adRequest.deviceIpAddress,
                        ua : adRequest.deviceUserAgent,
                        geo: [
                                zip: adRequest.deviceZip
                        ]
                ]
        ]

        def rubiconResponse = exchangeApiFacade.postRequest(adRequest.username, adRequest.password, adRequest.deviceUserAgent, request)

        return processRubiconResponse(rubiconResponse, requestId)
    }

    private buildImp(requestId) {
        return [
                id   : requestId.toString(),
                video: [
                        mimes         : SUPPORTED_MIME_TYPES,
                        minduration   : 15,
                        maxduration   : 30,
                        playbackmethod: [1, 3],
                        boxingallowed : 0,
                        ext           : [
                                skip: 0,
                                rp  : [
                                        size_id: 201
                                ]
                        ]
                ],
//			banner : [
//				id : requestId,
//				ext : [
//					rp : [
//						size_id : 65,
//						mime : 'text/html'
//					] 
//				]				
//			],
                ext  : [
                        rp: [
                                zone_id: 589760,   // for video
//					zone_id : 539246,   // for banner
                                enc    : 'url'
                        ]
                ]
        ]
    }

    private ProcessAdResponse processRubiconResponse(Map<?, ?> rubiconResponse, UUID requestId) {
        // "x?.find { true }" will return the first element in x, if x is not null or empty; otherwise null
        //log.debug("Rubicon response: ${rubiconResponse}")
        if (rubiconResponse.statuscode != 10) {
            def adm = rubiconResponse.seatbid?.find { true }?.bid?.find { true }?.adm

            if (!adm) {
                log.warn("Invalid Rubicon Response: ${rubiconResponse}")
                return null
            }

            return processVast(adm, requestId)
        } else {
            log.debug("Status Code 10: No ads returned")
            return null
        }
    }

    protected boolean isSupportedType(String type) {
        return SUPPORTED_MIME_TYPES.contains(type)
    }

    // for testing/development purposes
    public static void main(String[] args) {
        RubiconProcessor rubiconProcessor = new RubiconProcessor()
//		rubiconProcessor.exchangeApiFacade = new ExchangeApiFacadeStub()
        rubiconProcessor.exchangeApiFacade = new ExchangeApiFacadeReal(target: 'http://staged-by.rubiconproject.com/a/api/exchange.json')
//		rubiconProcessor.exchangeApiFacade = new ExchangeApiFacadeReal(target : 'http://exapi-us-east.rubiconproject.com/a/api/exchange.json')

        ProcessAdRequest adRequest = new ProcessAdRequest(
                username: System.getProperty('rubicon.perpetualmedia.username'),
                password: System.getProperty('rubicon.perpetualmedia.password'),
                siteName: 'pm-site1',
                siteDomain: 'perpetualmedia.com',
                sitePage: 'http://www.perpetualmedia.com',
                sitePublisherAccountId: 15810,
                siteId: 124680,
                deviceIpAddress: '50.20.0.62',
                deviceUserAgent: 'perpetualmedia/1.0',
                deviceZip: '30022'
        )

        log.debug rubiconProcessor.processAdRequest(adRequest).dump()
    }
}
