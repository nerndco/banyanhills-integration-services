package campaign.api.gateway.processor.rubicon

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

import feign.Feign
import feign.Logger
import feign.auth.BasicAuthRequestInterceptor
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import feign.slf4j.Slf4jLogger

@Component
@Profile('!stub')
class ExchangeApiFacadeReal implements ExchangeApiFacade {
	
	@Value('${rubicon.target}')
	private String target
	
	/* -------------------------------------------------- */

	Map<?, ?> postRequest(username, password, userAgent, request) {
//		assert false, 'Using Real'
		
		def exchangeApiClient = Feign.builder()
				.encoder(new JacksonEncoder())
				.decoder(new JacksonDecoder())
				.requestInterceptor(new BasicAuthRequestInterceptor(username, password))
				.logger(new Slf4jLogger())
				.logLevel(Logger.Level.FULL)
				.target(ExchangeApiClient, target)

		return exchangeApiClient.post(userAgent, request)
	}
}
