package campaign.api.gateway.processor

import groovy.transform.ToString

@ToString(includeNames = true, includeFields = true)
class ProcessAdResponse {
    UUID requestId

    Long adId

    URL[] proofOfPlayUrls
    URL impressionUrl
    URL mediaUrl
}
