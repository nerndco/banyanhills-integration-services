package campaign.api.gateway.processor.springserve

import groovy.util.logging.Slf4j

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import campaign.api.gateway.processor.AdProcessorIF
import campaign.api.gateway.processor.ProcessAdRequest
import campaign.api.gateway.processor.ProcessAdResponse
import campaign.api.gateway.processor.VastProcessor

@Component
@Slf4j
class SpringServeProcessor extends VastProcessor implements AdProcessorIF 
{
	@Value('${springserve.target}')
	private URL url
	
	private static final List SUPPORTED_MIME_TYPES = [
		'video/mp4'
	]
	
	@Override
	public ProcessAdResponse processAdRequest(ProcessAdRequest request) 
	{
		URL requestUrl;
		if (StringUtils.isNotEmpty(request.adTagUrl)) {
			requestUrl = new URL(request.adTagUrl);
		}
		else {
			requestUrl = url;
		}
		
		def response = requestUrl.getText();
		
		log.debug("Response from call ${requestUrl} to SpringServe: ${response}")
		
		UUID requestId = UUID.randomUUID()
		
		return processVast(response, requestId);
	}
	
	protected boolean isSupportedType(String type) {
		return SUPPORTED_MIME_TYPES.contains(type)
	}

}
