package campaign.api.gateway.processor.openx

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import campaign.api.gateway.processor.AdProcessorIF
import campaign.api.gateway.processor.ProcessAdRequest
import campaign.api.gateway.processor.ProcessAdResponse
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j

@Component
@Slf4j
public class OpenXProcessor implements AdProcessorIF {
    static final Logger logger = LoggerFactory.getLogger(OpenXProcessor.class)
	
	@Value('${openx.target}')
	private URL url

	/* -------------------------------------------------- */

    @Override
    public ProcessAdResponse processAdRequest(ProcessAdRequest request) {
		ProcessAdResponse adResponse = new ProcessAdResponse(requestId: UUID.randomUUID(), proofOfPlayUrls: [])
		
		def response = url.getText()
		
		logger.debug("Response from call to OpenX: ${response}")
		
		def json = new JsonSlurper().parseText(response)
		
		// get the first "creative" of the first "ad" 
		def ads = json.ads.ad as List
		if (ads.size() > 0) {
			def ad = ads.getAt(0)
			if (ad) {
				adResponse.adId = ad.adid
				def creative = (ad.creative as List)?.getAt(0)
				if (creative) {
					adResponse.mediaUrl = new URL(creative.media)
					adResponse.impressionUrl = new URL(creative.tracking.impression)
				}
			}
		}
		
		return adResponse
    }
    
	// for testing/development purposes
 	static void main(String[] args) {
 		OpenXProcessor openXProcessor = new OpenXProcessor()
		openXProcessor.url = new URL('http://perpetual-d.openx.net/w/1.0/arj?auid=538735510&cb=987654321')

 		ProcessAdRequest adRequest = new ProcessAdRequest()

 		log.debug openXProcessor.processAdRequest(adRequest).dump()
	}
}


