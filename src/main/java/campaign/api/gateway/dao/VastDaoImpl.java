/**
 * 
 */
package campaign.api.gateway.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import campaign.api.gateway.domain.VASTDetail;
import campaign.api.gateway.repository.VastRepository;

/**
 * @author kritika.srivastava
 *
 */
@Transactional
@Service
public class VastDaoImpl{
	
	@Autowired
	VastRepository repository;

	public VASTDetail save(VASTDetail entity) {
		VASTDetail result = repository.save(entity);
		return result;
	}

	public VASTDetail findById(Long id) {
		VASTDetail result = repository.findById(id);
		return result;
	}

	public VASTDetail update(VASTDetail entity) {
		VASTDetail result = repository.save(entity);
		return result;
	}
	
	public VASTDetail findByMediaId(String mediaId) {
		VASTDetail result = repository.findVASTByMediaId(mediaId);
		return result;
	}
	
	public VASTDetail findByWidgetId(String widgetId) {
		VASTDetail result = repository.findVASTByWidgetId(widgetId);
		return result;
	}

}
