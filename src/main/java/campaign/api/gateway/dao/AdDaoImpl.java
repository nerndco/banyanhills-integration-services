/**
 * 
 */
package campaign.api.gateway.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import campaign.api.gateway.domain.AdDetail;
import campaign.api.gateway.repository.AdRepository;

@Transactional
@Service
public class AdDaoImpl{
	
	@Autowired
	AdRepository repository;

	public AdDetail save(AdDetail entity) {
		AdDetail result = repository.save(entity);
		return result;
	}

	public AdDetail findById(Long id) {
		AdDetail result = repository.findById(id);
		return result;
	}

	public AdDetail update(AdDetail entity) {
		AdDetail result = repository.save(entity);
		return result;
	}
	
	public AdDetail findByMediaId(String mediaId) {
		AdDetail result = repository.findAdByMediaId(mediaId);
		return result;
	}
	
	public AdDetail findByWidgetId(String widgetId) {
		AdDetail result = repository.findAdByWidgetId(widgetId);
		return result;
	}

}
