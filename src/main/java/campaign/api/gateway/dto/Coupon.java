/**
 * 
 */
package campaign.api.gateway.dto;

import java.util.Date;

/**
 * @author Kritika.Srivastava
 *
 */
public class Coupon {
	private Date allocatedOn ;
	private String allocatedTo;
	private String code;
	private Long id;
	private Integer quantity;
	private Date validFrom;
	private Date validTo;
	private Double value;
	/**
	 * @return the allocatedOn
	 */
	public Date getAllocatedOn() {
		return allocatedOn;
	}
	/**
	 * @param allocatedOn the allocatedOn to set
	 */
	public void setAllocatedOn(Date allocatedOn) {
		this.allocatedOn = allocatedOn;
	}
	/**
	 * @return the allocatedTo
	 */
	public String getAllocatedTo() {
		return allocatedTo;
	}
	/**
	 * @param allocatedTo the allocatedTo to set
	 */
	public void setAllocatedTo(String allocatedTo) {
		this.allocatedTo = allocatedTo;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}
	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}
	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	/**
	 * @return the value
	 */
	public Double getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Double value) {
		this.value = value;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Coupon [allocatedOn=" + allocatedOn + ", allocatedTo="
				+ allocatedTo + ", code=" + code + ", id=" + id + ", quantity="
				+ quantity + ", validFrom=" + validFrom + ", validTo="
				+ validTo + ", value=" + value + "]";
	}
}
