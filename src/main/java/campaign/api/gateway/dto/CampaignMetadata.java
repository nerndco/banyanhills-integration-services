/**
 * 
 */
package campaign.api.gateway.dto;

import java.util.List;

import campaign.api.gateway.dto.segment.Segment;

/**
 * @author kritika.srivastava
 *
 */
public class CampaignMetadata {
   private List<Type> campaignTypes;
   private List<State> campaignStates;
   private List<Segment> segments;
   private List<Channel> channels;
   private List<Promotion> promotions;
   private List<ApproverData> approverData;
   private String message;
/**
 * @return the campaignTypes
 */
public List<Type> getCampaignTypes() {
	return campaignTypes;
}
/**
 * @param campaignTypes the campaignTypes to set
 */
public void setCampaignTypes(List<Type> campaignTypes) {
	this.campaignTypes = campaignTypes;
}
/**
 * @return the campaignStates
 */
public List<State> getCampaignStates() {
	return campaignStates;
}
/**
 * @param campaignStates the campaignStates to set
 */
public void setCampaignStates(List<State> campaignStates) {
	this.campaignStates = campaignStates;
}
/**
 * @return the segments
 */
public List<Segment> getSegments() {
	return segments;
}
/**
 * @param segments the segments to set
 */
public void setSegments(List<Segment> segments) {
	this.segments = segments;
}
/**
 * @return the channels
 */
public List<Channel> getChannels() {
	return channels;
}
/**
 * @param channels the channels to set
 */
public void setChannels(List<Channel> channels) {
	this.channels = channels;
}
/**
 * @return the promotions
 */
public List<Promotion> getPromotions() {
	return promotions;
}
/**
 * @param promotions the promotions to set
 */
public void setPromotions(List<Promotion> promotions) {
	this.promotions = promotions;
}
/**
 * @return the approverData
 */
public List<ApproverData> getApproverData() {
	return approverData;
}
/**
 * @param approverData the approverData to set
 */
public void setApproverData(List<ApproverData> approverData) {
	this.approverData = approverData;
}
/**
 * @return the message
 */
public String getMessage() {
	return message;
}
/**
 * @param message the message to set
 */
public void setMessage(String message) {
	this.message = message;
}

}
