package campaign.api.gateway.dto;

import java.util.List;

public class DeliverySegment {

	private Long id;
	private Integer segmentRefId;
	private String segmentRefName;
	private List<SegmentParam> segmentParams;

	/**
	 * @return the segmentRefId
	 */
	public Integer getSegmentRefId() {
		return segmentRefId;
	}

	/**
	 * @param segmentRefId
	 *            the segmentRefId to set
	 */
	public void setSegmentRefId(Integer segmentRefId) {
		this.segmentRefId = segmentRefId;
	}

	/**
	 * @return the segmentRefName
	 */
	public String getSegmentRefName() {
		return segmentRefName;
	}

	/**
	 * @param segmentRefName
	 *            the segmentRefName to set
	 */
	public void setSegmentRefName(String segmentRefName) {
		this.segmentRefName = segmentRefName;
	}

	/**
	 * @return the segmentParams
	 */
	public List<SegmentParam> getSegmentParams() {
		return segmentParams;
	}

	/**
	 * @param segmentParams
	 *            the segmentParams to set
	 */
	public void setSegmentParams(List<SegmentParam> segmentParams) {
		this.segmentParams = segmentParams;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 */
	public DeliverySegment() {
	}

	/**
	 * @param segmentRefId
	 * @param segmentRefName
	 * @param segmentParams
	 */
	public DeliverySegment(Integer segmentRefId, String segmentRefName,
			List<SegmentParam> segmentParams) {
		super();
		this.segmentRefId = segmentRefId;
		this.segmentRefName = segmentRefName;
		this.segmentParams = segmentParams;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeliverySegment [id=" + id + ", segmentRefId=" + segmentRefId
				+ ", segmentRefName=" + segmentRefName + ", segmentParams="
				+ segmentParams + "]";
	}

}
