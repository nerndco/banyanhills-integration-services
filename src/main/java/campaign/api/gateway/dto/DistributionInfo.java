package campaign.api.gateway.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import campaign.api.gateway.dto.xibo.XiboSchedule;

public class DistributionInfo {

	private Long id;

	private String name;
	private Long campaignId;
	private String searchTags;
	private List<Schedule> scheludes;
	private List<DeliverySegment> segments;
	private Channel Channels;
	private Channel channel;
	// facebook wall, twitter, push toQueue,proximity,pushNotification;
	private List<XiboSchedule> xiboSchedule;
	private String createdByUserId;
	private String createdByUserName;
	private Date createdByOn;
	private String modifiedByUserId;
	private String modifiedByUserName;
	private Date modifiedByOn;
	private String hostAppName;
	private String templateId;
	private String templateName;
	private String templateHeight;
	private String templateWidth;
	private Long status;
	private String displays;
	private List<String> layoutIds = new ArrayList<String>();
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the searchTags
	 */
	public String getSearchTags() {
		return searchTags;
	}

	/**
	 * @param searchTags
	 *            the searchTags to set
	 */
	public void setSearchTags(String searchTags) {
		this.searchTags = searchTags;
	}

	/**
	 * @return the scheludes
	 */
	public List<Schedule> getScheludes() {
		return scheludes;
	}

	/**
	 * @param scheludes
	 *            the scheludes to set
	 */
	public void setScheludes(List<Schedule> scheludes) {
		this.scheludes = scheludes;
	}

	/**
	 * @return the segments
	 */
	public List<DeliverySegment> getSegments() {
		return segments;
	}

	/**
	 * @param segments
	 *            the segments to set
	 */
	public void setSegments(List<DeliverySegment> segments) {
		this.segments = segments;
	}

	/**
	 * @return the channels
	 */
	public Channel getChannels() {
		return Channels;
	}

	/**
	 * @param channels
	 *            the channels to set
	 */
	public void setChannels(Channel channels) {
		Channels = channels;
	}

	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}

	/**
	 * @param createdByUserId
	 *            the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}

	/**
	 * @param createdByUserName
	 *            the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	/**
	 * @return the createdByOn
	 */
	public Date getCreatedByOn() {
		return createdByOn;
	}

	/**
	 * @param createdByOn
	 *            the createdByOn to set
	 */
	public void setCreatedByOn(Date createdByOn) {
		this.createdByOn = createdByOn;
	}

	/**
	 * @return the modifiedByUserId
	 */
	public String getModifiedByUserId() {
		return modifiedByUserId;
	}

	/**
	 * @param modifiedByUserId
	 *            the modifiedByUserId to set
	 */
	public void setModifiedByUserId(String modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}

	/**
	 * @return the modifiedByUserName
	 */
	public String getModifiedByUserName() {
		return modifiedByUserName;
	}

	/**
	 * @param modifiedByUserName
	 *            the modifiedByUserName to set
	 */
	public void setModifiedByUserName(String modifiedByUserName) {
		this.modifiedByUserName = modifiedByUserName;
	}

	/**
	 * @return the modifiedByOn
	 */
	public Date getModifiedByOn() {
		return modifiedByOn;
	}

	/**
	 * @param modifiedByOn
	 *            the modifiedByOn to set
	 */
	public void setModifiedByOn(Date modifiedByOn) {
		this.modifiedByOn = modifiedByOn;
	}

	/**
	 * @return the hostAppName
	 */
	public String getHostAppName() {
		return hostAppName;
	}

	/**
	 * @param hostAppName
	 *            the hostAppName to set
	 */
	public void setHostAppName(String hostAppName) {
		this.hostAppName = hostAppName;
	}

	/**
	 * @return the campaignId
	 */
	public Long getCampaignId() {
		return campaignId;
	}

	/**
	 * 
	 */
	public DistributionInfo() {
	}

	/**
	 * @param name
	 * @param campaignId
	 * @param searchTags
	 * @param scheludes
	 * @param segments
	 * @param channels
	 * @param createdByUserId
	 * @param createdByUserName
	 * @param createdByOn
	 * @param modifiedByUserId
	 * @param modifiedByUserName
	 * @param modifiedByOn
	 * @param tenantId
	 * @param tenantName
	 * @param hostAppName
	 */
	public DistributionInfo(String name, Long campaignId, String searchTags,
			List<Schedule> scheludes, List<DeliverySegment> segments,
			Channel channels, String createdByUserId,
			String createdByUserName, Date createdByOn,
			String modifiedByUserId, String modifiedByUserName,
			Date modifiedByOn,
			String hostAppName) {
		super();
		this.name = name;
		this.campaignId = campaignId;
		this.searchTags = searchTags;
		this.scheludes = scheludes;
		this.segments = segments;
		Channels = channels;
		this.createdByUserId = createdByUserId;
		this.createdByUserName = createdByUserName;
		this.createdByOn = createdByOn;
		this.modifiedByUserId = modifiedByUserId;
		this.modifiedByUserName = modifiedByUserName;
		this.modifiedByOn = modifiedByOn;
		this.hostAppName = hostAppName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DistributionInfo [id=" + id + ", name=" + name
				+ ", campaignId=" + campaignId + ", searchTags=" + searchTags
				+ ", scheludes=" + scheludes + ", segments=" + segments
				+ ", Channels=" + Channels + ", createdByUserId="
				+ createdByUserId + ", createdByUserName=" + createdByUserName
				+ ", createdByOn=" + createdByOn + ", modifiedByUserId="
				+ modifiedByUserId + ", modifiedByUserName="
				+ modifiedByUserName + ", modifiedByOn=" + modifiedByOn
				+ ", hostAppName=" + hostAppName + "]";
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the xiboSchedule
	 */
	public List<XiboSchedule> getXiboSchedule() {
		return xiboSchedule;
	}

	/**
	 * @param xiboSchedule the xiboSchedule to set
	 */
	public void setXiboSchedule(List<XiboSchedule> xiboSchedule) {
		this.xiboSchedule = xiboSchedule;
	}

	/**
	 * @return the channel
	 */
	public Channel getChannel() {
		return channel;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	/**
	 * @return the templateId
	 */
	public String getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the templateHeight
	 */
	public String getTemplateHeight() {
		return templateHeight;
	}

	/**
	 * @param templateHeight the templateHeight to set
	 */
	public void setTemplateHeight(String templateHeight) {
		this.templateHeight = templateHeight;
	}

	/**
	 * @return the templateWidth
	 */
	public String getTemplateWidth() {
		return templateWidth;
	}

	/**
	 * @param templateWidth the templateWidth to set
	 */
	public void setTemplateWidth(String templateWidth) {
		this.templateWidth = templateWidth;
	}

	/**
	 * @return the status
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return the displays
	 */
	public String getDisplays() {
		return displays;
	}

	/**
	 * @param displays the displays to set
	 */
	public void setDisplays(String displays) {
		this.displays = displays;
	}

	public List<String> getLayoutIds() {
		return layoutIds;
	}

	public void setLayoutIds(List<String> layoutIds) {
		this.layoutIds = layoutIds;
	}


}
