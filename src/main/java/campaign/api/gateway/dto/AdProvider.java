package campaign.api.gateway.dto;

public enum AdProvider {
	OpenX, Rubicon, SpringServe;
}
