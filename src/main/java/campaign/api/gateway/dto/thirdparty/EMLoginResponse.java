/**
 * 
 */
package campaign.api.gateway.dto.thirdparty;

/**
 * @author kritika.srivastava
 *
 */
public class EMLoginResponse {
	private String message;
	private Metadata metaData;
	private Integer result; 
	

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the metadata
	 */
	public Metadata getMetadata() {
		return metaData;
	}

	/**
	 * @param metadata the metadata to set
	 */
	public void setMetadata(Metadata metadata) {
		this.metaData = metadata;
	}
	/**
	 * @return the result
	 */
	public Integer getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(Integer result) {
		this.result = result;
	}
}
