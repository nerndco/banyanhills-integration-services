/**
 * 
 */
package campaign.api.gateway.dto;

/**
 * @author Kritika.Srivastava
 *
 */
public class ApproverRequest {
	public ApproverRequest(Integer retailerId, String entityTypeId) {
		super();
		this.retailerId = retailerId;
		this.entityTypeId = entityTypeId;
	}
	private Integer retailerId;
	private String entityTypeId;
	/**
	 * @return the retailerId
	 */
	public Integer getRetailerId() {
		return retailerId;
	}
	/**
	 * @param retailerId the retailerId to set
	 */
	public void setRetailerId(Integer retailerId) {
		this.retailerId = retailerId;
	}
	/**
	 * @return the entityTypeId
	 */
	public String getEntityTypeId() {
		return entityTypeId;
	}
	/**
	 * @param entityTypeId the entityTypeId to set
	 */
	public void setEntityTypeId(String entityTypeId) {
		this.entityTypeId = entityTypeId;
	}
}
