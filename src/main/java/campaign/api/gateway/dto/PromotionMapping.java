/**
 * 
 */
package campaign.api.gateway.dto;


/**
 * @author Kritika.Srivastava
 *
 */
public class PromotionMapping {
	private Long id;
	private Long promotion_Id;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the promotion_Id
	 */
	public Long getPromotion_Id() {
		return promotion_Id;
	}
	/**
	 * @param promotion_Id the promotion_Id to set
	 */
	public void setPromotion_Id(Long promotion_Id) {
		this.promotion_Id = promotion_Id;
	}
	
}
