package campaign.api.gateway.dto;

import java.util.Date;
import java.util.List;

public class Schedule {

	private Long id;
	private String name;
	private String cronExpression;
	private Boolean isActive;
	private List<Job> jobs;
	private Date validFrom;
	private Date validTo;
	private String createdByUserId;
	private String createdByUserName;
	private Date createdByOn;
	private String modifiedByUserId;
	private String modifiedByUserName;
	private Date modifiedByOn;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cronExpression
	 */
	public String getCronExpression() {
		return cronExpression;
	}

	/**
	 * @param cronExpression
	 *            the cronExpression to set
	 */
	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	/**
	 * @return the isActive
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive
	 *            the isActive to set
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the jobs
	 */
	public List<Job> getJobs() {
		return jobs;
	}

	/**
	 * @param jobs
	 *            the jobs to set
	 */
	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}

	/**
	 * @param createdByUserId
	 *            the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}

	/**
	 * @param createdByUserName
	 *            the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	/**
	 * @return the createdByOn
	 */
	public Date getCreatedByOn() {
		return createdByOn;
	}

	/**
	 * @param createdByOn
	 *            the createdByOn to set
	 */
	public void setCreatedByOn(Date createdByOn) {
		this.createdByOn = createdByOn;
	}

	/**
	 * @return the modifiedByUserId
	 */
	public String getModifiedByUserId() {
		return modifiedByUserId;
	}

	/**
	 * @param modifiedByUserId
	 *            the modifiedByUserId to set
	 */
	public void setModifiedByUserId(String modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}

	/**
	 * @return the modifiedByUserName
	 */
	public String getModifiedByUserName() {
		return modifiedByUserName;
	}

	/**
	 * @param modifiedByUserName
	 *            the modifiedByUserName to set
	 */
	public void setModifiedByUserName(String modifiedByUserName) {
		this.modifiedByUserName = modifiedByUserName;
	}

	/**
	 * @return the modifiedByOn
	 */
	public Date getModifiedByOn() {
		return modifiedByOn;
	}

	/**
	 * @param modifiedByOn
	 *            the modifiedByOn to set
	 */
	public void setModifiedByOn(Date modifiedByOn) {
		this.modifiedByOn = modifiedByOn;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 */
	public Schedule() {
	}

	/**
	 * @param name
	 * @param cronExpression
	 * @param isActive
	 * @param jobs
	 * @param validFrom
	 * @param validTo
	 * @param createdByUserId
	 * @param createdByUserName
	 * @param createdByOn
	 * @param modifiedByUserId
	 * @param modifiedByUserName
	 * @param modifiedByOn
	 */
	public Schedule(String name, String cronExpression, Boolean isActive,
			List<Job> jobs, Date validFrom, Date validTo,
			String createdByUserId, String createdByUserName, Date createdByOn,
			String modifiedByUserId, String modifiedByUserName,
			Date modifiedByOn) {
		super();
		this.name = name;
		this.cronExpression = cronExpression;
		this.isActive = isActive;
		this.jobs = jobs;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.createdByUserId = createdByUserId;
		this.createdByUserName = createdByUserName;
		this.createdByOn = createdByOn;
		this.modifiedByUserId = modifiedByUserId;
		this.modifiedByUserName = modifiedByUserName;
		this.modifiedByOn = modifiedByOn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "XiboSchedule [id=" + id + ", name=" + name + ", cronExpression="
				+ cronExpression + ", isActive=" + isActive + ", jobs=" + jobs
				+ ", validFrom=" + validFrom + ", validTo=" + validTo
				+ ", createdByUserId=" + createdByUserId
				+ ", createdByUserName=" + createdByUserName + ", createdByOn="
				+ createdByOn + ", modifiedByUserId=" + modifiedByUserId
				+ ", modifiedByUserName=" + modifiedByUserName
				+ ", modifiedByOn=" + modifiedByOn + "]";
	}

}
