package campaign.api.gateway.dto.content;

import java.io.Serializable;



public class VASTRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private String campaignId;
	private String layoutId;
	private String vastTagURL;
	private String fromDate;
	private String toDate;
	private String deviceId;
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(String layoutId) {
		this.layoutId = layoutId;
	}
	public String getVastTagURL() {
		return vastTagURL;
	}
	public void setVastTagURL(String vastTagURL) {
		this.vastTagURL = vastTagURL;
	}


}
