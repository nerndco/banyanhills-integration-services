package campaign.api.gateway.dto.content;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable{

	private static final long serialVersionUID = 2178404850932069925L;
	
	private String id;
	private String text;
	private String commentByUserName;
	private Date commentedOn;	
	private String commentedByUserId;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the commentByUserName
	 */
	public String getCommentByUserName() {
		return commentByUserName;
	}
	/**
	 * @param commentByUserName the commentByUserName to set
	 */
	public void setCommentByUserName(String commentByUserName) {
		this.commentByUserName = commentByUserName;
	}
	/**
	 * @return the commentedOn
	 */
	public Date getCommentedOn() {
		return commentedOn;
	}
	/**
	 * @param commentedOn the commentedOn to set
	 */
	public void setCommentedOn(Date commentedOn) {
		this.commentedOn = commentedOn;
	}
	/**
	 * @return the commentedByUserId
	 */
	public String getCommentedByUserId() {
		return commentedByUserId;
	}
	/**
	 * @param commentedByUserId the commentedByUserId to set
	 */
	public void setCommentedByUserId(String commentedByUserId) {
		this.commentedByUserId = commentedByUserId;
	}

	public Comment(){
		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comment [id=" + id + ", text=" + text + ", commentByUserName="
				+ commentByUserName + ", commentedOn=" + commentedOn
				+ ", commentedByUserId=" + commentedByUserId + "]";
	}

	
}
