/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class LikeDisLikeResponse {
	private String message;
	private LikeDislikeMetadata metaData;
	private Long result;
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the metaData
	 */
	public LikeDislikeMetadata getMetaData() {
		return metaData;
	}
	/**
	 * @param metaData the metaData to set
	 */
	public void setMetaData(LikeDislikeMetadata metaData) {
		this.metaData = metaData;
	}
	/**
	 * @return the result
	 */
	public Long getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(Long result) {
		this.result = result;
	}
}
