/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class LikeDislikeRequest {
	private String offer;
	private String like;
	private String userId;
	/**
	 * @return the offer
	 */
	public String getOffer() {
		return offer;
	}
	/**
	 * @param offer the offer to set
	 */
	public void setOffer(String offer) {
		this.offer = offer;
	}
	/**
	 * @return the like
	 */
	public String getLike() {
		return like;
	}
	/**
	 * @param like the like to set
	 */
	public void setLike(String like) {
		this.like = like;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
