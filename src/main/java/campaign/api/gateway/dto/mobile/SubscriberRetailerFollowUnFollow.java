package campaign.api.gateway.dto.mobile;

public class SubscriberRetailerFollowUnFollow {
	private String id;
	private Integer subscriberId;
	private Integer retailerId;
	private Boolean unSubscribed;

	/**
	 * 
	 */
	public SubscriberRetailerFollowUnFollow() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param subscriberId
	 * @param retailerId
	 * @param unSubscribed
	 */
	public SubscriberRetailerFollowUnFollow(Integer subscriberId,
			Integer retailerId, Boolean unSubscribed) {
		super();
		this.subscriberId = subscriberId;
		this.retailerId = retailerId;
		this.unSubscribed = unSubscribed;
	}

	/**
	 * @return the subscriberId
	 */
	public Integer getSubscriberId() {
		return subscriberId;
	}

	/**
	 * @param subscriberId
	 *            the subscriberId to set
	 */
	public void setSubscriberId(Integer subscriberId) {
		this.subscriberId = subscriberId;
	}

	/**
	 * @return the retailerId
	 */
	public Integer getRetailerId() {
		return retailerId;
	}

	/**
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId) {
		this.retailerId = retailerId;
	}

	/**
	 * @return the unSubscribed
	 */
	public Boolean getUnSubscribed() {
		return unSubscribed;
	}

	/**
	 * @param unSubscribed
	 *            the unSubscribed to set
	 */
	public void setUnSubscribed(Boolean unSubscribed) {
		this.unSubscribed = unSubscribed;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SubscriberRetailerFollowUnFollow [id=" + id + ", subscriberId="
				+ subscriberId + ", retailerId=" + retailerId
				+ ", unSubscribed=" + unSubscribed + "]";
	}

}
