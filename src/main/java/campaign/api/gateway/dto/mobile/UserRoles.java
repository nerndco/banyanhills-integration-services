/**
 * 
 */
package campaign.api.gateway.dto.mobile;

import java.util.List;
import java.util.Map;

/**
 * @author Kritika.Srivastava
 *
 */
public class UserRoles {
	private String name;
	private Integer id;
	private Integer createdBy;
	private Map<String, String> permissions;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the permissions
	 */
	public Map<String, String> getPermissions() {
		return permissions;
	}
	/**
	 * @param permissions the permissions to set
	 */
	public void setPermissions(Map<String, String> permissions) {
		this.permissions = permissions;
	}
}
