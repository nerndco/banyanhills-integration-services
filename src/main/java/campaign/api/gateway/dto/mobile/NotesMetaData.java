/**
 * 
 */
package campaign.api.gateway.dto.mobile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author kritika.srivastava
 *
 */
public class NotesMetaData {
	private List<Offer> offers = new ArrayList<Offer>();
	private Map<String, NotesData> notes;
	/**
	 * @return the offers
	 */
	public List<Offer> getOffers() {
		return offers;
	}
	/**
	 * @param offers the offers to set
	 */
	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}
	/**
	 * @return the notes
	 */
	public Map<String, NotesData> getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(Map<String, NotesData> notes) {
		this.notes = notes;
	}
	
}
