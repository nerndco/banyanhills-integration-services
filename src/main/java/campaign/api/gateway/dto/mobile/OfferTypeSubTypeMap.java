/**
 * 
 */
package campaign.api.gateway.dto.mobile;


/**
 * @author kritika.srivastava
 *
 */
public class OfferTypeSubTypeMap {
	OfferType offerType;

	/**
	 * @return the offerType
	 */
	public OfferType getOfferType() {
		return offerType;
	}

	/**
	 * @param offerType the offerType to set
	 */
	public void setOfferType(OfferType offerType) {
		this.offerType = offerType;
	}
}
