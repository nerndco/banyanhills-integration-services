package campaign.api.gateway.dto.mobile;

import java.util.List;

public class BrandMetadata {
	private List<Brand> retailers;

	/**
	 * @return the retailers
	 */
	public List<Brand> getRetailers() {
		return retailers;
	}

	/**
	 * @param retailers the retailers to set
	 */
	public void setRetailers(List<Brand> retailers) {
		this.retailers = retailers;
	}
	
}
