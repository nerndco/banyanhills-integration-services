/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class LikeDislikeMetadata {
	private Long id;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
