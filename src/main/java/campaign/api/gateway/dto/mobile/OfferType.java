/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author kritika.srivastava
 *
 */
public class OfferType {
	private Integer offerTypeId;
	private String offerTypeName;
	private String offerTypeDescription;
	/**
	 * @return the offerTypeId
	 */
	public Integer getOfferTypeId() {
		return offerTypeId;
	}
	/**
	 * @param offerTypeId the offerTypeId to set
	 */
	public void setOfferTypeId(Integer offerTypeId) {
		this.offerTypeId = offerTypeId;
	}
	/**
	 * @return the offerTypeName
	 */
	public String getOfferTypeName() {
		return offerTypeName;
	}
	/**
	 * @param offerTypeName the offerTypeName to set
	 */
	public void setOfferTypeName(String offerTypeName) {
		this.offerTypeName = offerTypeName;
	}
	/**
	 * @return the offerTypeDescription
	 */
	public String getOfferTypeDescription() {
		return offerTypeDescription;
	}
	/**
	 * @param offerTypeDescription the offerTypeDescription to set
	 */
	public void setOfferTypeDescription(String offerTypeDescription) {
		this.offerTypeDescription = offerTypeDescription;
	}
}
