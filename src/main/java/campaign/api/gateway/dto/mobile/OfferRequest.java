/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author kritika.srivastava
 *
 */
public class OfferRequest {
	private String id;
	private String userId;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
