/**
 * 
 */
package campaign.api.gateway.dto.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class Offer {
	private Long id;
	private String name;
	private String description;
	private Date validFrom;
	private Date validTo;
	private Long timeToLive;
	private String offerLogoURL;
	private String barCodeURL;
	private Boolean isDeleted;
	private List<OfferTypeSubTypeMap> offerTypeSubTypeMap = new ArrayList<OfferTypeSubTypeMap>();
	private Status status;
	private List<OfferMedia> offerMedia;
	private Integer likeCount;
	private String createdByUserId;
	private String createdByUserName;
	private Boolean isLiked=false;
	private Retailer retailer;
	private List<Location> locations;
	private Boolean fnf;

    
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}
	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}
	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	/**
	 * @return the timeToLive
	 */
	public Long getTimeToLive() {
		return timeToLive;
	}
	/**
	 * @param timeToLive the timeToLive to set
	 */
	public void setTimeToLive(Long timeToLive) {
		this.timeToLive = timeToLive;
	}
	/**
	 * @return the offerLogoURL
	 */
	public String getOfferLogoURL() {
		return offerLogoURL;
	}
	/**
	 * @param offerLogoURL the offerLogoURL to set
	 */
	public void setOfferLogoURL(String offerLogoURL) {
		this.offerLogoURL = offerLogoURL;
	}
	/**
	 * @return the barCodeURL
	 */
	public String getBarCodeURL() {
		return barCodeURL;
	}
	/**
	 * @param barCodeURL the barCodeURL to set
	 */
	public void setBarCodeURL(String barCodeURL) {
		this.barCodeURL = barCodeURL;
	}
	/**
	 * @return the isDeleted
	 */
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
	/**
	 * @return the offerMedia
	 */
	public List<OfferMedia> getOfferMedia() {
		return offerMedia;
	}
	/**
	 * @param offerMedia the offerMedia to set
	 */
	public void setOfferMedia(List<OfferMedia> offerMedia) {
		this.offerMedia = offerMedia;
	}
	/**
	 * @return the likeCount
	 */
	public Integer getLikeCount() {
		return likeCount;
	}
	/**
	 * @param likeCount the likeCount to set
	 */
	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}
	/**
	 * @return the offerTypeSubTypeMap
	 */
	public List<OfferTypeSubTypeMap> getOfferTypeSubTypeMap() {
		return offerTypeSubTypeMap;
	}
	/**
	 * @param offerTypeSubTypeMap the offerTypeSubTypeMap to set
	 */
	public void setOfferTypeSubTypeMap(List<OfferTypeSubTypeMap> offerTypeSubTypeMap) {
		this.offerTypeSubTypeMap = offerTypeSubTypeMap;
	}
	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}
	/**
	 * @param createdByUserId the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}
	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}
	/**
	 * @param createdByUserName the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}
	/**
	 * @return the isLiked
	 */
	public Boolean getIsLiked() {
		return isLiked;
	}
	/**
	 * @param isLiked the isLiked to set
	 */
	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}
	/**
	 * @return the retailer
	 */
	public Retailer getRetailer() {
		return retailer;
	}
	/**
	 * @param retailer the retailer to set
	 */
	public void setRetailer(Retailer retailer) {
		this.retailer = retailer;
	}
	/**
	 * @return the locations
	 */
	public List<Location> getLocations() {
		return locations;
	}
	/**
	 * @param locations the locations to set
	 */
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	/**
	 * @return the fnf
	 */
	public Boolean getFnf() {
		return fnf;
	}
	/**
	 * @param fnf the fnf to set
	 */
	public void setFnf(Boolean fnf) {
		this.fnf = fnf;
	}
}
