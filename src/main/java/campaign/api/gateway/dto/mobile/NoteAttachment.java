/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author kritika.srivastava
 *
 */
public class NoteAttachment {
	private Long id;
    private String mediaURL;
    private String mediaType;
    private Long streamId;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the mediaURL
	 */
	public String getMediaURL() {
		return mediaURL;
	}
	/**
	 * @param mediaURL the mediaURL to set
	 */
	public void setMediaURL(String mediaURL) {
		this.mediaURL = mediaURL;
	}
	/**
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}
	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	/**
	 * @return the streamId
	 */
	public Long getStreamId() {
		return streamId;
	}
	/**
	 * @param streamId the streamId to set
	 */
	public void setStreamId(Long streamId) {
		this.streamId = streamId;
	}
}
