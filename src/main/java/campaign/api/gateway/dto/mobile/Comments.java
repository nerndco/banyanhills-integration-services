/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class Comments {
	private Long id;
	private String text;
	private String commentedDate;
	private CommentedBy commentedBy;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the commentedDate
	 */
	public String getCommentedDate() {
		return commentedDate;
	}

	/**
	 * @param commentedDate
	 *            the commentedDate to set
	 */
	public void setCommentedDate(String commentedDate) {
		this.commentedDate = commentedDate;
	}

	/**
	 * @return the commentedBy
	 */
	public CommentedBy getCommentedBy() {
		return commentedBy;
	}

	/**
	 * @param commentedBy
	 *            the commentedBy to set
	 */
	public void setCommentedBy(CommentedBy commentedBy) {
		this.commentedBy = commentedBy;
	}
}
