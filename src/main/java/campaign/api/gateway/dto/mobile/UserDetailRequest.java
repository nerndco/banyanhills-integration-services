/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class UserDetailRequest {
	private String id;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
