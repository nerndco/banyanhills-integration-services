/**
 * 
 */
package campaign.api.gateway.dto.mobile;

import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class OfferMetaData {
	private Boolean isLiked=false;
	private List<Likers> likers;
	private List<Comments> comments;
	private Integer commentCount = 0;
	private Offer offer;
	/**
	 * @return the isLiked
	 */
	public Boolean getIsLiked() {
		return isLiked;
	}
	/**
	 * @param isLiked the isLiked to set
	 */
	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}
	/**
	 * @return the likers
	 */
	public List<Likers> getLikers() {
		return likers;
	}
	/**
	 * @param likers the likers to set
	 */
	public void setLikers(List<Likers> likers) {
		this.likers = likers;
	}
	/**
	 * @return the offer
	 */
	public Offer getOffer() {
		return offer;
	}
	/**
	 * @param offer the offer to set
	 */
	public void setOffer(Offer offer) {
		this.offer = offer;
	}
	/**
	 * @return the comments
	 */
	public List<Comments> getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(List<Comments> comments) {
		this.comments = comments;
	}
	/**
	 * @return the commentCount
	 */
	public Integer getCommentCount() {
		return commentCount;
	}
	/**
	 * @param commentCount the commentCount to set
	 */
	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
}
