/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author kritika.srivastava
 *
 */
public class NotesLocation {
	private Double y;
	private Double x;
	/**
	 * @return the y
	 */
	public Double getY() {
		return y;
	}
	/**
	 * @param y the y to set
	 */
	public void setY(Double y) {
		this.y = y;
	}
	/**
	 * @return the x
	 */
	public Double getX() {
		return x;
	}
	/**
	 * @param x the x to set
	 */
	public void setX(Double x) {
		this.x = x;
	}
}
