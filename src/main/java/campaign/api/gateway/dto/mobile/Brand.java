package campaign.api.gateway.dto.mobile;

public class Brand {
	private Long id;
	private String retailerName;
    private String retailerCode;
    private String phone;
    private String mobile;
    private String contactPerson;
    private String fax;
    private String email;
    private Boolean accountLocked;
    private String logoUrl;
    private String webUrl;
    private String city;
    private String addressLine1;
    private String addressLine2;
    private String countryCode;
    private String pinCode;
    private String state;
    private Double radius;
    private Boolean fuf;
    
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the retailerName
	 */
	public String getRetailerName() {
		return retailerName;
	}
	/**
	 * @param retailerName the retailerName to set
	 */
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	/**
	 * @return the retailerCode
	 */
	public String getRetailerCode() {
		return retailerCode;
	}
	/**
	 * @param retailerCode the retailerCode to set
	 */
	public void setRetailerCode(String retailerCode) {
		this.retailerCode = retailerCode;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the contactPerson
	 */
	public String getContactPerson() {
		return contactPerson;
	}
	/**
	 * @param contactPerson the contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the accountLocked
	 */
	public Boolean getAccountLocked() {
		return accountLocked;
	}
	/**
	 * @param accountLocked the accountLocked to set
	 */
	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}
	/**
	 * @return the logoUrl
	 */
	public String getLogoUrl() {
		return logoUrl;
	}
	/**
	 * @param logoUrl the logoUrl to set
	 */
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	/**
	 * @return the webUrl
	 */
	public String getWebUrl() {
		return webUrl;
	}
	/**
	 * @param webUrl the webUrl to set
	 */
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}
	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}
	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the pinCode
	 */
	public String getPinCode() {
		return pinCode;
	}
	/**
	 * @param pinCode the pinCode to set
	 */
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the radius
	 */
	public Double getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(Double radius) {
		this.radius = radius;
	}
	/**
	 * @return the fuf
	 */
	public Boolean getFuf() {
		return fuf;
	}
	/**
	 * @param fuf the fuf to set
	 */
	public void setFuf(Boolean fuf) {
		this.fuf = fuf;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Brand [id=" + id + ", retailerName=" + retailerName
				+ ", retailerCode=" + retailerCode + ", phone=" + phone
				+ ", mobile=" + mobile + ", contactPerson=" + contactPerson
				+ ", fax=" + fax + ", email=" + email + ", accountLocked="
				+ accountLocked + ", logoUrl=" + logoUrl + ", webUrl=" + webUrl
				+ ", city=" + city + ", addressLine1=" + addressLine1
				+ ", addressLine2=" + addressLine2 + ", countryCode="
				+ countryCode + ", pinCode=" + pinCode + ", state=" + state
				+ ", radius=" + radius + ", fuf=" + fuf + "]";
	}
	
}
