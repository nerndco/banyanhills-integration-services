/**
 * 
 */
package campaign.api.gateway.dto.mobile;


/**
 * @author Kritika.Srivastava
 *
 */
public class BrandsMetaData {
	private String message;
	private BrandMetadata metaData;
	private Integer result;
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the metaData
	 */
	public BrandMetadata getMetaData() {
		return metaData;
	}
	/**
	 * @param metaData the metaData to set
	 */
	public void setMetaData(BrandMetadata metaData) {
		this.metaData = metaData;
	}
	/**
	 * @return the result
	 */
	public Integer getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(Integer result) {
		this.result = result;
	}
}
