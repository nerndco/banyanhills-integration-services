/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author kritika.srivastava
 *
 */
public class NotesResponse {
	private String message;
	private NotesMetaData metaData;
	private Long result;
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the metaData
	 */
	public NotesMetaData getMetaData() {
		return metaData;
	}
	/**
	 * @param metaData the metaData to set
	 */
	public void setMetaData(NotesMetaData metaData) {
		this.metaData = metaData;
	}
	/**
	 * @return the result
	 */
	public Long getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(Long result) {
		this.result = result;
	}
}
