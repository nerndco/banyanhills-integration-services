/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class OfferMedia {
	Long id;
	String mediaURL;
	String mediaTypeId;
	String streamId;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the mediaURL
	 */
	public String getMediaURL() {
		return mediaURL;
	}
	/**
	 * @param mediaURL the mediaURL to set
	 */
	public void setMediaURL(String mediaURL) {
		this.mediaURL = mediaURL;
	}
	/**
	 * @return the mediaTypeId
	 */
	public String getMediaTypeId() {
		return mediaTypeId;
	}
	/**
	 * @param mediaTypeId the mediaTypeId to set
	 */
	public void setMediaTypeId(String mediaTypeId) {
		this.mediaTypeId = mediaTypeId;
	}
	/**
	 * @return the streamId
	 */
	public String getStreamId() {
		return streamId;
	}
	/**
	 * @param streamId the streamId to set
	 */
	public void setStreamId(String streamId) {
		this.streamId = streamId;
	}
}
