/**
 * 
 */
package campaign.api.gateway.dto.mobile;

/**
 * @author Kritika.Srivastava
 *
 */
public class OfferCommentsMetaData {
	private Long id;
    private String text;
    private String commentDate;
    private CommentedBy commentedBy;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the commentDate
	 */
	public String getCommentDate() {
		return commentDate;
	}
	/**
	 * @param commentDate the commentDate to set
	 */
	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}
	/**
	 * @return the commentedBy
	 */
	public CommentedBy getCommentedBy() {
		return commentedBy;
	}
	/**
	 * @param commentedBy the commentedBy to set
	 */
	public void setCommentedBy(CommentedBy commentedBy) {
		this.commentedBy = commentedBy;
	}
}
