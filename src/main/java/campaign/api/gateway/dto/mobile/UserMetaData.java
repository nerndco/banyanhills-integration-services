/**
 * 
 */
package campaign.api.gateway.dto.mobile;

import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class UserMetaData {
	private Integer id;
    private String username;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private Long point;
    private String title;
    private Boolean accountExpired;
    private Boolean accountLocked;
    private Boolean passwordExpired;
    private String phone;
    private String mobile;
    private String fax;
    private String notificationFlag;
    private String profilePicUrl;
    private Integer userType;
    private List<UserRoles> roles;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the point
	 */
	public Long getPoint() {
		return point;
	}
	/**
	 * @param point the point to set
	 */
	public void setPoint(Long point) {
		this.point = point;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the accountExpired
	 */
	public Boolean getAccountExpired() {
		return accountExpired;
	}
	/**
	 * @param accountExpired the accountExpired to set
	 */
	public void setAccountExpired(Boolean accountExpired) {
		this.accountExpired = accountExpired;
	}
	/**
	 * @return the accountLocked
	 */
	public Boolean getAccountLocked() {
		return accountLocked;
	}
	/**
	 * @param accountLocked the accountLocked to set
	 */
	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}
	/**
	 * @return the passwordExpired
	 */
	public Boolean getPasswordExpired() {
		return passwordExpired;
	}
	/**
	 * @param passwordExpired the passwordExpired to set
	 */
	public void setPasswordExpired(Boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return the notificationFlag
	 */
	public String getNotificationFlag() {
		return notificationFlag;
	}
	/**
	 * @param notificationFlag the notificationFlag to set
	 */
	public void setNotificationFlag(String notificationFlag) {
		this.notificationFlag = notificationFlag;
	}
	/**
	 * @return the profilePicUrl
	 */
	public String getProfilePicUrl() {
		return profilePicUrl;
	}
	/**
	 * @param profilePicUrl the profilePicUrl to set
	 */
	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}
	/**
	 * @return the userType
	 */
	public Integer getUserType() {
		return userType;
	}
	/**
	 * @param userType the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	/**
	 * @return the roles
	 */
	public List<UserRoles> getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<UserRoles> roles) {
		this.roles = roles;
	}
}
