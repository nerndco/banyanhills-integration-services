/**
 * 
 */
package campaign.api.gateway.dto.mobile;

import java.util.List;

/**
 * @author kritika.srivastava
 *
 */
public class NotesData {
	private Long id;
	private String description;
	private Long createdById;
	private String createdByFirstName;
	private String createdByLastName;
	private String createdByProfilePicUrl;
	private String createdOn;
	private String lastUpdatedOn;
	private Long notificationType;
	private Boolean fnf;
	private Boolean isLiked;
	private NotesLocation location;
	private List<Likers> likers;
	private List<NoteAttachment> noteAttachment;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the createdById
	 */
	public Long getCreatedById() {
		return createdById;
	}
	/**
	 * @param createdById the createdById to set
	 */
	public void setCreatedById(Long createdById) {
		this.createdById = createdById;
	}
	/**
	 * @return the createdByFirstName
	 */
	public String getCreatedByFirstName() {
		return createdByFirstName;
	}
	/**
	 * @param createdByFirstName the createdByFirstName to set
	 */
	public void setCreatedByFirstName(String createdByFirstName) {
		this.createdByFirstName = createdByFirstName;
	}
	/**
	 * @return the createdByLastName
	 */
	public String getCreatedByLastName() {
		return createdByLastName;
	}
	/**
	 * @param createdByLastName the createdByLastName to set
	 */
	public void setCreatedByLastName(String createdByLastName) {
		this.createdByLastName = createdByLastName;
	}
	/**
	 * @return the createdByProfilePicUrl
	 */
	public String getCreatedByProfilePicUrl() {
		return createdByProfilePicUrl;
	}
	/**
	 * @param createdByProfilePicUrl the createdByProfilePicUrl to set
	 */
	public void setCreatedByProfilePicUrl(String createdByProfilePicUrl) {
		this.createdByProfilePicUrl = createdByProfilePicUrl;
	}
	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the lastUpdatedOn
	 */
	public String getLastUpdatedOn() {
		return lastUpdatedOn;
	}
	/**
	 * @param lastUpdatedOn the lastUpdatedOn to set
	 */
	public void setLastUpdatedOn(String lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}
	/**
	 * @return the notificationType
	 */
	public Long getNotificationType() {
		return notificationType;
	}
	/**
	 * @param notificationType the notificationType to set
	 */
	public void setNotificationType(Long notificationType) {
		this.notificationType = notificationType;
	}
	/**
	 * @return the fnf
	 */
	public Boolean getFnf() {
		return fnf;
	}
	/**
	 * @param fnf the fnf to set
	 */
	public void setFnf(Boolean fnf) {
		this.fnf = fnf;
	}
	/**
	 * @return the isLiked
	 */
	public Boolean getIsLiked() {
		return isLiked;
	}
	/**
	 * @param isLiked the isLiked to set
	 */
	public void setIsLiked(Boolean isLiked) {
		this.isLiked = isLiked;
	}
	/**
	 * @return the location
	 */
	public NotesLocation getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(NotesLocation location) {
		this.location = location;
	}
	/**
	 * @return the likers
	 */
	public List<Likers> getLikers() {
		return likers;
	}
	/**
	 * @param likers the likers to set
	 */
	public void setLikers(List<Likers> likers) {
		this.likers = likers;
	}
	/**
	 * @return the noteAttachment
	 */
	public List<NoteAttachment> getNoteAttachment() {
		return noteAttachment;
	}
	/**
	 * @param noteAttachment the noteAttachment to set
	 */
	public void setNoteAttachment(List<NoteAttachment> noteAttachment) {
		this.noteAttachment = noteAttachment;
	}
	
}
