package campaign.api.gateway.dto;

public class Text {
	private Long playListId;
	private Long widgetId;
	private String ta_text;
	private String name;
	private String effect = "none";
	private String speed;
	private String duration;
	private String xmds = "true";
	private String backgroundColor;
	private String javaScript = "";
	
	public Long getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}
	public Long getPlayListId() {
		return playListId;
	}
	public void setPlayListId(Long playListId) {
		this.playListId = playListId;
	}
	public String getTa_text() {
		return ta_text;
	}
	public void setTa_text(String ta_text) {
		this.ta_text = ta_text;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEffect() {
		return effect;
	}
	public void setEffect(String effect) {
		this.effect = effect;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	public String getJavaScript() {
		return javaScript;
	}
	public void setJavaScript(String javaScript) {
		this.javaScript = javaScript;
	}
	public String getXmds() {
		return xmds;
	}
	public void setXmds(String xmds) {
		this.xmds = xmds;
	}
	
	
}
