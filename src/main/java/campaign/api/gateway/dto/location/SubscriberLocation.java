package campaign.api.gateway.dto.location;

import java.io.Serializable;
import java.util.List;

import rx.Subscription;
import campaign.api.gateway.dto.campaign.Location;

public class SubscriberLocation {
	/**
	 * 
	 */
	private String id;
	private String locationName;
	private Location position;
	private String city;
	private String addressLine1;
	private String addressLine2;
	private String countryCode;
	private String pinCode;
	private String state;
	private String email;
	private String phone;
	private Integer tenantId;
	private String subscriberRefId;
	private String locationRefId;
	/**
	 * 
	 */
	public SubscriberLocation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param locationName
	 * @param position
	 * @param city
	 * @param addressLine1
	 * @param addressLine2
	 * @param countryCode
	 * @param pinCode
	 * @param state
	 * @param email
	 * @param phone
	 * @param deviceType
	 * @param subscriptions
	 * @param tenantId
	 * @param referenceId
	 */
	public SubscriberLocation(String locationName, Location position,
			String city, String addressLine1, String addressLine2,
			String countryCode, String pinCode, String state, String email,
			String phone,
			List<Subscription> subscriptions, Integer tenantId, String subscriberRefId) {
		super();
		this.locationName = locationName;
		this.position = position;
		this.city = city;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.countryCode = countryCode;
		this.pinCode = pinCode;
		this.state = state;
		this.email = email;
		this.phone = phone;
		this.tenantId = tenantId;
		this.subscriberRefId = subscriberRefId;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return the position
	 */
	public Location getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Location position) {
		this.position = position;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the addressLine1
	 */
	public String getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public String getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the pinCode
	 */
	public String getPinCode() {
		return pinCode;
	}

	/**
	 * @param pinCode the pinCode to set
	 */
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}


	
	/**
	 * @return the tenantId
	 */
	public Integer getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SubscriberLocation [id=" + id + ", locationName="
				+ locationName + ", position=" + position + ", city=" + city
				+ ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", countryCode=" + countryCode + ", pinCode="
				+ pinCode + ", state=" + state + ", email=" + email
				+ ", phone=" + phone + ", tenantId=" + tenantId
				+ ", referenceId=" + subscriberRefId + "]";
	}

	/**
	 * @return the subscriberRefId
	 */
	public String getSubscriberRefId() {
		return subscriberRefId;
	}

	/**
	 * @param subscriberRefId the subscriberRefId to set
	 */
	public void setSubscriberRefId(String subscriberRefId) {
		this.subscriberRefId = subscriberRefId;
	}

	/**
	 * @return the locationRefId
	 */
	public String getLocationRefId() {
		return locationRefId;
	}

	/**
	 * @param locationRefId the locationRefId to set
	 */
	public void setLocationRefId(String locationRefId) {
		this.locationRefId = locationRefId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	
}
