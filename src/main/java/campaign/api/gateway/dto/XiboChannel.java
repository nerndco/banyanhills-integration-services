package campaign.api.gateway.dto;

import javax.persistence.Entity;

public class XiboChannel extends Channel {
	private String clientId;
	private String clientSecret;
	private String host;
	private String grantType;

	/**
	 * 
	 */
	public XiboChannel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param clientId
	 * @param clientSecret
	 * @param host
	 * @param grantType
	 */
	public XiboChannel(String clientId, String clientSecret, String host,
			String grantType) {
		super();
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.host = host;
		this.grantType = grantType;
	}

	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * @param clientId
	 *            the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * @return the clientSecret
	 */
	public String getClientSecret() {
		return clientSecret;
	}

	/**
	 * @param clientSecret
	 *            the clientSecret to set
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host
	 *            the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the grantType
	 */
	public String getGrantType() {
		return grantType;
	}

	/**
	 * @param grantType
	 *            the grantType to set
	 */
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "XiboChannel [clientId=" + clientId + ", clientSecret="
				+ clientSecret + ", host=" + host + ", grantType=" + grantType
				+ ", toString()=" + super.toString() + "]";
	}

}
