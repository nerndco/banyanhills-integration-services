/**
 * 
 */
package campaign.api.gateway.dto;

/**
 * @author Kritika.Srivastava
 *
 */
public class EventsMapping {
	 private Long MeetingID;
	 private Long id;
	 private Integer RoomID;
	 private String Title;
	 private String Description;
	 private String StartTimezone;
	 private String Start;
	 private String End;
	 private String RecurrenceRule;
	 private Integer RecurrenceID;
	 private Boolean IsAllDay;
	/**
	 * @return the meetingID
	 */
	public Long getMeetingID() {
		return MeetingID;
	}
	/**
	 * @param meetingID the meetingID to set
	 */
	public void setMeetingID(Long meetingID) {
		MeetingID = meetingID;
	}
	/**
	 * @return the roomID
	 */
	public Integer getRoomID() {
		return RoomID;
	}
	/**
	 * @param roomID the roomID to set
	 */
	public void setRoomID(Integer roomID) {
		RoomID = roomID;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return Title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		Title = title;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}
	/**
	 * @return the startTimezone
	 */
	public String getStartTimezone() {
		return StartTimezone;
	}
	/**
	 * @param startTimezone the startTimezone to set
	 */
	public void setStartTimezone(String startTimezone) {
		StartTimezone = startTimezone;
	}
	/**
	 * @return the start
	 */
	public String getStart() {
		return Start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(String start) {
		Start = start;
	}
	/**
	 * @return the end
	 */
	public String getEnd() {
		return End;
	}
	/**
	 * @param end the end to set
	 */
	public void setEnd(String end) {
		End = end;
	}
	/**
	 * @return the recurrenceRule
	 */
	public String getRecurrenceRule() {
		return RecurrenceRule;
	}
	/**
	 * @param recurrenceRule the recurrenceRule to set
	 */
	public void setRecurrenceRule(String recurrenceRule) {
		RecurrenceRule = recurrenceRule;
	}
	/**
	 * @return the recurrenceID
	 */
	public Integer getRecurrenceID() {
		return RecurrenceID;
	}
	/**
	 * @param recurrenceID the recurrenceID to set
	 */
	public void setRecurrenceID(Integer recurrenceID) {
		RecurrenceID = recurrenceID;
	}
	/**
	 * @return the isAllDay
	 */
	public Boolean getIsAllDay() {
		return IsAllDay;
	}
	/**
	 * @param isAllDay the isAllDay to set
	 */
	public void setIsAllDay(Boolean isAllDay) {
		IsAllDay = isAllDay;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
