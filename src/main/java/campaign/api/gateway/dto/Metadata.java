package campaign.api.gateway.dto;

import java.util.List;
import java.util.Map;

public class Metadata {
	private Map<String, List<Campaign>> campaignMap;

	/**
	 * @return the campaignMap
	 */
	public Map<String, List<Campaign>> getCampaignMap() {
		return campaignMap;
	}

	/**
	 * @param campaignMap the campaignMap to set
	 */
	public void setCampaignMap(Map<String, List<Campaign>> campaignMap) {
		this.campaignMap = campaignMap;
	}
}
