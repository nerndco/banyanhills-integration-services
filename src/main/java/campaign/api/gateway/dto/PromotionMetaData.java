/**
 * 
 */
package campaign.api.gateway.dto;

import java.util.List;

import campaign.api.gateway.dto.mobile.Approver;

/**
 * @author kritika.srivastava
 *
 */
public class PromotionMetaData {
	private List<Categories> promotionTypes;
	private List<State> promotionStates;
	private List<ApproverData> approver;

	/**
	 * @return the campaignTypes
	 */
	public List<Categories> getPromotionTypes() {
		return promotionTypes;
	}

	/**
	 * @param promotionTypes
	 *            the promotionTypes to set
	 */
	public void setPromotionTypes(List<Categories> promotionTypes) {
		this.promotionTypes = promotionTypes;
	}

	/**
	 * @return the campaignStates
	 */
	public List<State> getPromotionStates() {
		return promotionStates;
	}

	/**
	 * @param campaignStates
	 *            the campaignStates to set
	 */
	public void setPromotionStates(List<State> promotionStates) {
		this.promotionStates = promotionStates;
	}

	/**
	 * @return the approver
	 */
	public List<ApproverData> getApprover() {
		return approver;
	}

	/**
	 * @param approver
	 *            the approver to set
	 */
	public void setApprover(List<ApproverData> approver) {
		this.approver = approver;
	}
}
