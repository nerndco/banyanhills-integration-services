package campaign.api.gateway.dto;

import java.util.List;

import campaign.api.gateway.dto.xibo.Layout;


public class Channel {
	private String type;
	private Integer id;
	private String name;
	private String channelType;
	private String loginName;
	private String loginCredential;
	private List<Layout> layouts;


	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the channelType
	 */
	public String getChannelType() {
		return channelType;
	}

	/**
	 * @param channelType
	 *            the channelType to set
	 */
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName
	 *            the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the loginCredential
	 */
	public String getLoginCredential() {
		return loginCredential;
	}

	/**
	 * @param loginCredential
	 *            the loginCredential to set
	 */
	public void setLoginCredential(String loginCredential) {
		this.loginCredential = loginCredential;
	}

	public Channel() {

	}

	/**
	 * @param name
	 * @param channelType
	 * @param loginName
	 * @param loginCredential
	 */
	public Channel(String name, String channelType, String loginName,
			String loginCredential) {

		this.name = name;
		this.channelType = channelType;
		this.loginName = loginName;
		this.loginCredential = loginCredential;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Channel [id=" + id + ", name=" + name + ", channelType="
				+ channelType + ", loginName=" + loginName
				+ ", loginCredential=" + loginCredential + "]";
	}

	/**
	 * @return the layouts
	 */
	public List<Layout> getLayouts() {
		return layouts;
	}

	/**
	 * @param layouts the layouts to set
	 */
	public void setLayouts(List<Layout> layouts) {
		this.layouts = layouts;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}
