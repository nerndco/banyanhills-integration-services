/**
 * @author Kritika.Srivastava
 *
 */
package campaign.api.gateway.dto;


public class SegmentParam {

	private Long id;
	private String paramName;
	private String expression;

	/**
	 * @return the paramName
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * @param paramName
	 *            the paramName to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	/**
	 * @return the expression
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * @param expression
	 *            the expression to set
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 */
	public SegmentParam() {
	}

	/**
	 * @param paramName
	 * @param expression
	 */
	public SegmentParam(String paramName, String expression) {
		super();
		this.paramName = paramName;
		this.expression = expression;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SegmentParam [id=" + id + ", paramName=" + paramName
				+ ", expression=" + expression + "]";
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
