package campaign.api.gateway.dto.segment;
import java.io.Serializable;

public class Parameter  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4263835258670212060L;
	private String id;
	private Integer seqNum;
	private String type;
	private String name;
	private String expression;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the seqNum
	 */
	public Integer getSeqNum() {
		return seqNum;
	}
	/**
	 * @param seqNum the seqNum to set
	 */
	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the expression
	 */
	public String getExpression() {
		return expression;
	}
	/**
	 * @param exprssion the expression to set
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}

	public Parameter(){
		
	}
	/**
	 * @param id
	 * @param seqNum
	 * @param type
	 * @param name
	 * @param expression
	 */
	public Parameter(Integer seqNum, String type, String name,
			String expression) {
		this.seqNum = seqNum;
		this.type = type;
		this.name = name;
		this.expression = expression;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Parameter [id=" + id + ", seqNum=" + seqNum + ", type=" + type
				+ ", name=" + name + ", expression=" + expression + "]";
	}
	
	
	
}
