package campaign.api.gateway.dto.segment;

import java.io.Serializable;
import java.util.List;

public class Segment  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9031485070394659112L;

	private String id;
	private String name;	
	private String details;
	private List<String> tags;
	private String type;
	private String criteriaTemplate;
	private List<Parameter> params;
	private String tenantId;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return the tags
	 */
	public List<String> getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the criteriaTemplate
	 */
	public String getCriteriaTemplate() {
		return criteriaTemplate;
	}
	/**
	 * @param criteriaTemplate the criteriaTemplate to set
	 */
	public void setCriteriaTemplate(String criteriaTemplate) {
		this.criteriaTemplate = criteriaTemplate;
	}
	/**
	 * @return the params
	 */
	public List<Parameter> getParams() {
		return params;
	}
	/**
	 * @param params the params to set
	 */
	public void setParams(List<Parameter> params) {
		this.params = params;
	}
	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}
	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * 
	 */
	public Segment() {
	}

	/**
	 * @param name
	 * @param details
	 * @param tags
	 * @param type
	 * @param criteriaTemplate
	 * @param params
	 * @param tenantId
	 */
	public Segment(String name, String details, List<String> tags, String type,
			String criteriaTemplate, List<Parameter> params, String tenantId) {
		super();
		this.name = name;
		this.details = details;
		this.tags = tags;
		this.type = type;
		this.criteriaTemplate = criteriaTemplate;
		this.params = params;
		this.tenantId = tenantId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Segment [id=" + id + ", name=" + name + ", details=" + details
				+ ", tags=" + tags + ", type=" + type + ", criteriaTemplate="
				+ criteriaTemplate + ", params=" + params + ", tenantId="
				+ tenantId + "]";
	}

	
}