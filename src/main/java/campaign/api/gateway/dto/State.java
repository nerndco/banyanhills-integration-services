/**
 * 
 */
package campaign.api.gateway.dto;


/**
 * @author kritika.srivastava
 *
 */
public class State {
	Integer id;
	String name;
	String details;
		
	public Integer getId() {
		return id;
	}
	/**
	 * @param state_id the state_id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "State [state_id=" + id + ", name=" + name + ", details=" + details
				+ "]";
	}
}
