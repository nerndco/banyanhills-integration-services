/**
 * 
 */
package campaign.api.gateway.dto;



/**
 * @author kritika.srivastava
 *
 */
public class CampaignCategory {
	
	private Integer id;
	private String type;
	private String details;

	/**
	 * @return the type_id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param type_id the type_id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CampaignCategory [type_id=" + id + ", type=" + type + ", details="
				+ details + "]";
	}
	
}
