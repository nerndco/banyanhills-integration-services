/**
 * 
 */
package campaign.api.gateway.dto;


/**
 * @author Kritika.Srivastava
 *
 */

public class ApproverMapping {
	private Long id;
	private Long approverId;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the approverId
	 */
	public Long getApproverId() {
		return approverId;
	}
	/**
	 * @param approverId the approverId to set
	 */
	public void setApproverId(Long approverId) {
		this.approverId = approverId;
	}
}
