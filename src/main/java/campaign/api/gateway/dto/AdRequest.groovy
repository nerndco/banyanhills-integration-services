package campaign.api.gateway.dto;

import java.io.Serializable;
import java.util.List;


public class AdRequest implements Serializable {

	String layoutId
	String adTagUrl
	String fromDate
	String toDate
	List<String> devices
	String deviceId
	
	AdProvider adProvider
	String adProviderAccount  // i.e. "perpetualmedia"
	
//	public String getFromDate() {
//		return fromDate;
//	}
//	public void setFromDate(String fromDate) {
//		this.fromDate = fromDate;
//	}
//	public String getToDate() {
//		return toDate;
//	}
//	public void setToDate(String toDate) {
//		this.toDate = toDate;
//	}	
//	public String getLayoutId() {
//		return layoutId;
//	}
//	public void setLayoutId(String layoutId) {
//		this.layoutId = layoutId;
//	}
//	public String getAdTagUrl() {
//		return adTagUrl;
//	}
//	public void setAdTagUrl(String adTagUrl) {
//		this.adTagUrl = adTagUrl;
//	}
//	public List<String> getDevices() {
//		return devices;
//	}
//	public void setDevices(List<String> devices) {
//		this.devices = devices;
//	}
//	public String getDeviceId() {
//		return deviceId;
//	}
//	public void setDeviceId(String deviceId) {
//		this.deviceId = deviceId;
//	}
//	public Source getSource() {
//		return source;
//	}
//	public void setSource(Source source) {
//		this.source = source;
//	}	
}
