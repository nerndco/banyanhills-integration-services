/**
 * 
 */
package campaign.api.gateway.dto.xibo;

/**
 * @author Kritika.Srivastava
 *
 */
public class EventData {
	 private Long eventId;
     private Long eventTypeId;
     private Long campaignId;
     private Long commandId;
     private Long userId;
     private String fromDt;
     private String toDt;
     private Long isPriority;
     private Integer recurrenceDetail;
     private String recurrenceRange;
     private String recurrenceType;
	/**
	 * @return the eventId
	 */
	public Long getEventId() {
		return eventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}
	/**
	 * @return the eventTypeId
	 */
	public Long getEventTypeId() {
		return eventTypeId;
	}
	/**
	 * @param eventTypeId the eventTypeId to set
	 */
	public void setEventTypeId(Long eventTypeId) {
		this.eventTypeId = eventTypeId;
	}
	/**
	 * @return the campaignId
	 */
	public Long getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the commandId
	 */
	public Long getCommandId() {
		return commandId;
	}
	/**
	 * @param commandId the commandId to set
	 */
	public void setCommandId(Long commandId) {
		this.commandId = commandId;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the fromDt
	 */
	public String getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the toDt
	 */
	public String getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(String toDt) {
		this.toDt = toDt;
	}
	/**
	 * @return the isPriority
	 */
	public Long getIsPriority() {
		return isPriority;
	}
	/**
	 * @param isPriority the isPriority to set
	 */
	public void setIsPriority(Long isPriority) {
		this.isPriority = isPriority;
	}
	/**
	 * @return the recurrenceDetail
	 */
	public Integer getRecurrenceDetail() {
		return recurrenceDetail;
	}
	/**
	 * @param recurrenceDetail the recurrenceDetail to set
	 */
	public void setRecurrenceDetail(Integer recurrenceDetail) {
		this.recurrenceDetail = recurrenceDetail;
	}
	/**
	 * @return the recurrenceRange
	 */
	public String getRecurrenceRange() {
		return recurrenceRange;
	}
	/**
	 * @param recurrenceRange the recurrenceRange to set
	 */
	public void setRecurrenceRange(String recurrenceRange) {
		this.recurrenceRange = recurrenceRange;
	}
	/**
	 * @return the recurrenceType
	 */
	public String getRecurrenceType() {
		return recurrenceType;
	}
	/**
	 * @param recurrenceType the recurrenceType to set
	 */
	public void setRecurrenceType(String recurrenceType) {
		this.recurrenceType = recurrenceType;
	}
   
}
