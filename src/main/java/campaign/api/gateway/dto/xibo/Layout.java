/**
 *
 */
package campaign.api.gateway.dto.xibo;

import campaign.api.gateway.util.LayoutType;

import java.util.Date;
import java.util.List;

public class Layout {
    private Long layoutId;
    private Long ownerId;
    private Long campaignId;
    private Long backgroundImageId;
    private Long schemaVersion;
    private String layout;
    private String description;
    private String backgroundColor;
    private Long status;
    private Long retired;
    private Long width;
    private Long height;
    private String compiledXlf;
    private Integer backgroundzIndex;
    private LayoutType type;
    private List<Regions> regions;
    private Long templateId;
    private Date createdDt;

    public LayoutType getType() {
        return type;
    }

    public void setType(LayoutType type) {
        this.type = type;
    }

    public String getCompiledXlf() {
        return compiledXlf;
    }

    public void setCompiledXlf(String compiledXlf) {
        this.compiledXlf = compiledXlf;
    }

    /**
     * @return the layoutId
     */
    public Long getLayoutId() {
        return layoutId;
    }

    /**
     * @param layoutId the layoutId to set
     */
    public void setLayoutId(Long layoutId) {
        this.layoutId = layoutId;
    }

    /**
     * @return the ownerId
     */
    public Long getOwnerId() {
        return ownerId;
    }

    /**
     * @param ownerId the ownerId to set
     */
    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * @return the campaignId
     */
    public Long getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the backgroundImageId
     */
    public Long getBackgroundImageId() {
        return backgroundImageId;
    }

    /**
     * @param backgroundImageId the backgroundImageId to set
     */
    public void setBackgroundImageId(Long backgroundImageId) {
        this.backgroundImageId = backgroundImageId;
    }

    /**
     * @return the schemaVersion
     */
    public Long getSchemaVersion() {
        return schemaVersion;
    }

    /**
     * @param schemaVersion the schemaVersion to set
     */
    public void setSchemaVersion(Long schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    /**
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * @param layout the layout to set
     */
    public void setLayout(String layout) {
        this.layout = layout;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the backgroundColor
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * @param backgroundColor the backgroundColor to set
     */
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * @return the status
     */
    public Long getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Long status) {
        this.status = status;
    }

    /**
     * @return the retired
     */
    public Long getRetired() {
        return retired;
    }

    /**
     * @param retired the retired to set
     */
    public void setRetired(Long retired) {
        this.retired = retired;
    }

    /**
     * @return the width
     */
    public Long getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Long width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Long getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Long height) {
        this.height = height;
    }

    public Integer getBackgroundzIndex() {
        return backgroundzIndex;
    }

    public void setBackgroundzIndex(Integer backgroundzIndex) {
        this.backgroundzIndex = backgroundzIndex;
    }

    public List<Regions> getRegions() {
        return regions;
    }

    public void setRegions(List<Regions> regions) {
        this.regions = regions;
    }

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }
}
