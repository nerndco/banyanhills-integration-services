/**
 * 
 */
package campaign.api.gateway.dto.xibo;

/**
 * @author Kritika.Srivastava
 *
 */
public class UploadMediaRequest {
	private Long playListId;
	private Long widgetId;
	/**
	 * @return the playListId
	 */
	public Long getPlayListId() {
		return playListId;
	}
	/**
	 * @param playListId the playListId to set
	 */
	public void setPlayListId(Long playListId) {
		this.playListId = playListId;
	}
	/**
	 * @return the widgetId
	 */
	public Long getWidgetId() {
		return widgetId;
	}
	/**
	 * @param widgetId the widgetId to set
	 */
	public void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}
}
