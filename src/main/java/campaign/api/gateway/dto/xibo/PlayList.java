/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class PlayList {
	private Long playlistId;
    private Long ownerId;
    private String name;
    private List<Widget> widgets;
	/**
	 * @return the playlistId
	 */
	public Long getPlaylistId() {
		return playlistId;
	}
	/**
	 * @param playlistId the playlistId to set
	 */
	public void setPlaylistId(Long playlistId) {
		this.playlistId = playlistId;
	}
	/**
	 * @return the ownerId
	 */
	public Long getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the widgets
	 */
	public List<Widget> getWidgets() {
		return widgets;
	}
	/**
	 * @param widgets the widgets to set
	 */
	public void setWidgets(List<Widget> widgets) {
		this.widgets = widgets;
	}
}
