/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class Regions {
	 private Long regionId;
	 private Long layoutId;
	 private Long ownerId;
	 private String name;
	 private String width;
	 private  String height;
	 private String top;

	private String leftPos;
     private Double zIndex;
     private List<PlayList> playlists;
     /*"playlists": [
       {
         "playlistId": 15,
         "ownerId": 1,
         "name": "campaign-2",
         "tags": [
           
         ],
         "widgets": [
           
         ],
         "permissions": [
           
         ],
         "displayOrder": "1",
         "dateService": null
       }
     ],
     "regionOptions": [
       
     ],
     "permissions": [
       
     ],*/
     private String displayOrder;
     private Double duration;
     private Long tempId;
 	 private Long rowNum;

	/**
	 * @return the regionId
	 */
	public Long getRegionId() {
		return regionId;
	}
	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return the layoutId
	 */
	public Long getLayoutId() {
		return layoutId;
	}
	/**
	 * @param layoutId the layoutId to set
	 */
	public void setLayoutId(Long layoutId) {
		this.layoutId = layoutId;
	}
	/**
	 * @return the ownerId
	 */
	public Long getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}
	/**
	 * @param width the width to set
	 */
	public void setWidth(String width) {
		this.width = width;
	}
	/**
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}
	/**
	 * @return the top
	 */
	public String getTop() {
		return top;
	}
	/**
	 * @param top the top to set
	 */
	public void setTop(String top) {
		this.top = top;
	}
	
	/**
	 * @return the zIndex
	 */
	public Double getzIndex() {
		return zIndex;
	}
	/**
	 * @param zIndex the zIndex to set
	 */
	public void setzIndex(Double zIndex) {
		this.zIndex = zIndex;
	}
	/**
	 * @return the displayOrder
	 */
	public String getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the duration
	 */
	public Double getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Double duration) {
		this.duration = duration;
	}
	/**
	 * @return the tempId
	 */
	public Long getTempId() {
		return tempId;
	}
	/**
	 * @param tempId the tempId to set
	 */
	public void setTempId(Long tempId) {
		this.tempId = tempId;
	}
	/**
	 * @return the playlists
	 */
	public List<PlayList> getPlaylists() {
		return playlists;
	}
	/**
	 * @param playlists the playlists to set
	 */
	public void setPlaylists(List<PlayList> playlists) {
		this.playlists = playlists;
	}
	
	 public String getLeftPos() {
		return leftPos;
	}
	public void setLeftPos(String leftPos) {
		this.leftPos = leftPos;
	}
	public Long getRowNum() {
		return rowNum;
	}
	public void setRowNum(Long rowNum) {
		this.rowNum = rowNum;
	}
	
	
}
