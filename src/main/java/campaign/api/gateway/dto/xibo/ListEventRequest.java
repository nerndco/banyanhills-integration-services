/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.Date;
import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class ListEventRequest {
	private List<String> displayGroupIds;
	private Date from;
	private Date to;
	/**
	 * @return the displayGroupIds
	 */
	public List<String> getDisplayGroupIds() {
		return displayGroupIds;
	}
	/**
	 * @param displayGroupIds the displayGroupIds to set
	 */
	public void setDisplayGroupIds(List<String> displayGroupIds) {
		this.displayGroupIds = displayGroupIds;
	}
	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}
}
