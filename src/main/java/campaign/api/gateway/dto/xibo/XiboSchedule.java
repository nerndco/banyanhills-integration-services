/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.List;

/**
 * @author kritika.srivastava
 *
 */
public class XiboSchedule {
	private Long id;
	private String eventId;
	private String eventTypeId;
	private List<String> displayGroupIds;
	private String fromDt;
	private String fromDtLink;
	private String toDt;
	private String toDtLink;
	private Long campaignId;
	private Integer dayPartId=0;
	private String recurrenceType;
	private String recurrenceDetail;
	private String recurrenceRange;
	private String recurrenceRangeLink;
	private Boolean isRecurring;
	private Integer isPriority=0;
	private Integer displayOrder=0;
	private String eventName;
	/**
	 * @return the eventTypeId
	 */
	public String getEventTypeId() {
		return eventTypeId;
	}
	/**
	 * @param eventTypeId the eventTypeId to set
	 */
	public void setEventTypeId(String eventTypeId) {
		this.eventTypeId = eventTypeId;
	}
	/**
	 * @return the displayGroupIds
	 */
	public List<String> getDisplayGroupIds() {
		return displayGroupIds;
	}
	/**
	 * @param displayGroupIds the displayGroupIds to set
	 */
	public void setDisplayGroupIds(List<String> displayGroupIds) {
		this.displayGroupIds = displayGroupIds;
	}
	/**
	 * @return the fromDt
	 */
	public String getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the fromDtLink
	 */
	public String getFromDtLink() {
		return fromDtLink;
	}
	/**
	 * @param fromDtLink the fromDtLink to set
	 */
	public void setFromDtLink(String fromDtLink) {
		this.fromDtLink = fromDtLink;
	}
	/**
	 * @return the toDt
	 */
	public String getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(String toDt) {
		this.toDt = toDt;
	}
	/**
	 * @return the toDtLink
	 */
	public String getToDtLink() {
		return toDtLink;
	}
	/**
	 * @param toDtLink the toDtLink to set
	 */
	public void setToDtLink(String toDtLink) {
		this.toDtLink = toDtLink;
	}
	/**
	 * @return the campaignId
	 */
	public Long getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the dayPartId
	 */
	public Integer getDayPartId() {
		return dayPartId;
	}
	/**
	 * @param dayPartId the dayPartId to set
	 */
	public void setDayPartId(Integer dayPartId) {
		this.dayPartId = dayPartId;
	}
	/**
	 * @return the recurrenceType
	 */
	public String getRecurrenceType() {
		return recurrenceType;
	}
	/**
	 * @param recurrenceType the recurrenceType to set
	 */
	public void setRecurrenceType(String recurrenceType) {
		this.recurrenceType = recurrenceType;
	}
	/**
	 * @return the recurrenceDetail
	 */
	public String getRecurrenceDetail() {
		return recurrenceDetail;
	}
	/**
	 * @param recurrenceDetail the recurrenceDetail to set
	 */
	public void setRecurrenceDetail(String recurrenceDetail) {
		this.recurrenceDetail = recurrenceDetail;
	}
	/**
	 * @return the recurrenceRange
	 */
	public String getRecurrenceRange() {
		return recurrenceRange;
	}
	/**
	 * @param recurrenceRange the recurrenceRange to set
	 */
	public void setRecurrenceRange(String recurrenceRange) {
		this.recurrenceRange = recurrenceRange;
	}
	/**
	 * @return the recurrenceRangeLink
	 */
	public String getRecurrenceRangeLink() {
		return recurrenceRangeLink;
	}
	/**
	 * @param recurrenceRangeLink the recurrenceRangeLink to set
	 */
	public void setRecurrenceRangeLink(String recurrenceRangeLink) {
		this.recurrenceRangeLink = recurrenceRangeLink;
	}
	/**
	 * @return the isRecurring
	 */
	public Boolean getIsRecurring() {
		return isRecurring;
	}
	/**
	 * @param isRecurring the isRecurring to set
	 */
	public void setIsRecurring(Boolean isRecurring) {
		this.isRecurring = isRecurring;
	}
	/**
	 * @return the eventId
	 */
	public String getEventId() {
		return eventId;
	}
	/**
	 * @param eventId the eventId to set
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	/**
	 * @return the isPriority
	 */
	public Integer getIsPriority() {
		return isPriority;
	}
	/**
	 * @param isPriority the isPriority to set
	 */
	public void setIsPriority(Integer isPriority) {
		this.isPriority = isPriority;
	}
	/**
	 * @return the displayOrder
	 */
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
}
