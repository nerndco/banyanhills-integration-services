/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class Events {
	private Integer success;
	private List<EventsResponse> result;
	/**
	 * @return the success
	 */
	public Integer getSuccess() {
		return success;
	}
	/**
	 * @param success the success to set
	 */
	public void setSuccess(Integer success) {
		this.success = success;
	}
	/**
	 * @return the result
	 */
	public List<EventsResponse> getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(List<EventsResponse> result) {
		this.result = result;
	}
}
