/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */
public class UploadResponse {
	private List<File> files;

	/**
	 * @return the files
	 */
	public List<File> getFiles() {
		return files;
	}

	/**
	 * @param files the files to set
	 */
	public void setFiles(List<File> files) {
		this.files = files;
	}
}
