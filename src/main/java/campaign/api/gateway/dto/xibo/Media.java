/**
 * 
 */
package campaign.api.gateway.dto.xibo;

/**
 * @author Kritika.Srivastava
 *
 */
public class Media {
	private Long mediaId;
    private Long ownerId;
    private Long parentId;
    private String name;
    private String mediaType;
    private String storedAs;
    private String fileName;
    private String tags;
    private String fileSize;
    private Double duration;
    private Long valid;
    private String moduleSystemFile;
    private String expires;
    private String retired;
    private String isEdited;
    private String md5;
    private String owner;
    private String groupsWithPermissions;
    private Long released;
    private String apiRef;
    private String streamUrl;
    private String thumbUrl;
    
    public String getStreamUrl() {
		return streamUrl;
	}
	public void setStreamUrl(String streamUrl) {
		this.streamUrl = streamUrl;
	}
	public String getContentRefId() {
		return contentRefId;
	}
	public void setContentRefId(String contentRefId) {
		this.contentRefId = contentRefId;
	}
	private String content;
    private String contentRefId;
    /*"force": null,
    "isRemote": null,
    "cloned": false,
    "newExpiry": null,*/
    private Boolean alwaysCopy;
	/**
	 * @return the mediaId
	 */
	public Long getMediaId() {
		return mediaId;
	}
	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(Long mediaId) {
		this.mediaId = mediaId;
	}
	/**
	 * @return the ownerId
	 */
	public Long getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}
	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	/**
	 * @return the storedAs
	 */
	public String getStoredAs() {
		return storedAs;
	}
	/**
	 * @param storedAs the storedAs to set
	 */
	public void setStoredAs(String storedAs) {
		this.storedAs = storedAs;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}
	/**
	 * @return the fileSize
	 */
	public String getFileSize() {
		return fileSize;
	}
	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	/**
	 * @return the duration
	 */
	public Double getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Double duration) {
		this.duration = duration;
	}
	/**
	 * @return the valid
	 */
	public Long getValid() {
		return valid;
	}
	/**
	 * @param valid the valid to set
	 */
	public void setValid(Long valid) {
		this.valid = valid;
	}
	/**
	 * @return the moduleSystemFile
	 */
	public String getModuleSystemFile() {
		return moduleSystemFile;
	}
	/**
	 * @param moduleSystemFile the moduleSystemFile to set
	 */
	public void setModuleSystemFile(String moduleSystemFile) {
		this.moduleSystemFile = moduleSystemFile;
	}
	/**
	 * @return the expires
	 */
	public String getExpires() {
		return expires;
	}
	/**
	 * @param expires the expires to set
	 */
	public void setExpires(String expires) {
		this.expires = expires;
	}
	/**
	 * @return the retired
	 */
	public String getRetired() {
		return retired;
	}
	/**
	 * @param retired the retired to set
	 */
	public void setRetired(String retired) {
		this.retired = retired;
	}
	/**
	 * @return the isEdited
	 */
	public String getIsEdited() {
		return isEdited;
	}
	/**
	 * @param isEdited the isEdited to set
	 */
	public void setIsEdited(String isEdited) {
		this.isEdited = isEdited;
	}
	/**
	 * @return the md5
	 */
	public String getMd5() {
		return md5;
	}
	/**
	 * @param md5 the md5 to set
	 */
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	 * @return the groupsWithPermissions
	 */
	public String getGroupsWithPermissions() {
		return groupsWithPermissions;
	}
	/**
	 * @param groupsWithPermissions the groupsWithPermissions to set
	 */
	public void setGroupsWithPermissions(String groupsWithPermissions) {
		this.groupsWithPermissions = groupsWithPermissions;
	}
	/**
	 * @return the released
	 */
	public Long getReleased() {
		return released;
	}
	/**
	 * @param released the released to set
	 */
	public void setReleased(Long released) {
		this.released = released;
	}
	/**
	 * @return the apiRef
	 */
	public String getApiRef() {
		return apiRef;
	}
	/**
	 * @param apiRef the apiRef to set
	 */
	public void setApiRef(String apiRef) {
		this.apiRef = apiRef;
	}
	/**
	 * @return the alwaysCopy
	 */
	public Boolean getAlwaysCopy() {
		return alwaysCopy;
	}
	/**
	 * @param alwaysCopy the alwaysCopy to set
	 */
	public void setAlwaysCopy(Boolean alwaysCopy) {
		this.alwaysCopy = alwaysCopy;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	public String getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}
}
