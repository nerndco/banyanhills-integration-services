/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.List;

public class Widget {
	private Long widgetId;
	private Long playlistId;
	private Long ownerId;
	private String type;
	private Integer duration;
	private String displayOrder;
	private Integer useDuration;
	private Integer calculatedDuration;
	private String streamUrl;
	private String thumbUrl;
	private String html;
	
	private List<Media> media;
	private List<String> mediaIds;
	private List<WidgetOption> widgetOptions;

	/**
	 * @return the widgetId
	 */
	public Long getWidgetId() {
		return widgetId;
	}
	/**
	 * @param widgetId the widgetId to set
	 */
	public void setWidgetId(Long widgetId) {
		this.widgetId = widgetId;
	}
	/**
	 * @return the playlistId
	 */
	public Long getPlaylistId() {
		return playlistId;
	}
	/**
	 * @param playlistId the playlistId to set
	 */
	public void setPlaylistId(Long playlistId) {
		this.playlistId = playlistId;
	}
	/**
	 * @return the ownerId
	 */
	public Long getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the duration
	 */
	public Integer getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	/**
	 * @return the displayOrder
	 */
	public String getDisplayOrder() {
		return displayOrder;
	}
	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}
	/**
	 * @return the media
	 */
	public List<Media> getMedia() {
		return media;
	}
	/**
	 * @param media the media to set
	 */
	public void setMedia(List<Media> media) {
		this.media = media;
	}
	/**
	 * @return the mediaIds
	 */
	public List<String> getMediaIds() {
		return mediaIds;
	}
	/**
	 * @param mediaIds the mediaIds to set
	 */
	public void setMediaIds(List<String> mediaIds) {
		this.mediaIds = mediaIds;
	}
	
	public Integer getUseDuration() {
		return useDuration;
	}
	public void setUseDuration(Integer useDuration) {
		this.useDuration = useDuration;
	}
	public Integer getCalculatedDuration() {
		return calculatedDuration;
	}
	public void setCalculatedDuration(Integer calculatedDuration) {
		this.calculatedDuration = calculatedDuration;
	}
	public String getStreamUrl() {
		return streamUrl;
	}
	public void setStreamUrl(String streamUrl) {
		this.streamUrl = streamUrl;
	}
	public String getThumbUrl() {
		return thumbUrl;
	}
	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}

	public List<WidgetOption> getWidgetOptions() {
		return widgetOptions;
	}
	public void setWidgetOptions(List<WidgetOption> widgetOptions) {
		this.widgetOptions = widgetOptions;
	}
}
