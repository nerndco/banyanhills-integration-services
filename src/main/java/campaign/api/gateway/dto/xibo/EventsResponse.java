/**
 * 
 */
package campaign.api.gateway.dto.xibo;

import java.util.Date;

/**
 * @author Kritika.Srivastava
 *
 */
public class EventsResponse {
	private Integer id;
    private String title;
    private String url;
    private Long start;
    private Long end;
    private Boolean sameDay;
    private Boolean editable;
    private EventData event;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the start
	 */
	public Long getStart() {
		return start;
	}
	/**
	 * @param start the start to set
	 */
	public void setStart(Long start) {
		this.start = start;
	}
	/**
	 * @return the end
	 */
	public Long getEnd() {
		return end;
	}
	/**
	 * @param end the end to set
	 */
	public void setEnd(Long end) {
		this.end = end;
	}
	/**
	 * @return the sameDay
	 */
	public Boolean getSameDay() {
		return sameDay;
	}
	/**
	 * @param sameDay the sameDay to set
	 */
	public void setSameDay(Boolean sameDay) {
		this.sameDay = sameDay;
	}
	/**
	 * @return the editable
	 */
	public Boolean getEditable() {
		return editable;
	}
	/**
	 * @param editable the editable to set
	 */
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}
	/**
	 * @return the event
	 */
	public EventData getEvent() {
		return event;
	}
	/**
	 * @param event the event to set
	 */
	public void setEvent(EventData event) {
		this.event = event;
	}
}
