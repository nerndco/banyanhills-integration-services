package campaign.api.gateway.dto.xibo;

public class File {
	private String name;
	private Long size;
	private String type;
	private String url;
	private String storedas;
	private String delete_url;
	private String delete_type;
	private String error;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the size
	 */
	public Long getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(Long size) {
		this.size = size;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the storedas
	 */
	public String getStoredas() {
		return storedas;
	}
	/**
	 * @param storedas the storedas to set
	 */
	public void setStoredas(String storedas) {
		this.storedas = storedas;
	}
	/**
	 * @return the delete_url
	 */
	public String getDelete_url() {
		return delete_url;
	}
	/**
	 * @param delete_url the delete_url to set
	 */
	public void setDelete_url(String delete_url) {
		this.delete_url = delete_url;
	}
	/**
	 * @return the delete_type
	 */
	public String getDelete_type() {
		return delete_type;
	}
	/**
	 * @param delete_type the delete_type to set
	 */
	public void setDelete_type(String delete_type) {
		this.delete_type = delete_type;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
}
