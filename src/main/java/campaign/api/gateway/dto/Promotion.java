/**
 * 
 */
package campaign.api.gateway.dto;

import java.util.Date;
import java.util.List;

/**
 * @author Kritika.Srivastava
 *
 */

public class Promotion {
	public Promotion() {
		super();
		// TODO Auto-generated constructor stub
	}

	private Long id;
	private String name;
	private String description;
	private String contents[];
	private Date validFrom;
	private Date validTo;
	private State state;
	private boolean isDeleted = false;
	private List<Coupon> coupons;
	private Long createdBy;
	private String createdByUserId;
	private String createdByUserName;
	private Date createdByOn;
	private String modifiedByUserId;
	private String modifiedByUserName;
	private Date modifiedByOn;
	private int tenantId;
	private String tenantName;
	private String hostAppName;
	private Integer seqNum;
	private List<Categories> categories;
	private List<PromotionLikers> likers;
	private List<PromotionComments> promotionComments;


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the isDeleted
	 */
	public boolean isDeleted() {
		return isDeleted;
	}

	/**
	 * @param isDeleted
	 *            the isDeleted to set
	 */
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * @return the createdBy
	 */
	public Long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}

	/**
	 * @param createdByUserId
	 *            the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}

	/**
	 * @param createdByUserName
	 *            the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	/**
	 * @return the createdByOn
	 */
	public Date getCreatedByOn() {
		return createdByOn;
	}

	/**
	 * @param createdByOn
	 *            the createdByOn to set
	 */
	public void setCreatedByOn(Date createdByOn) {
		this.createdByOn = createdByOn;
	}

	/**
	 * @return the modifiedByUserId
	 */
	public String getModifiedByUserId() {
		return modifiedByUserId;
	}

	/**
	 * @param modifiedByUserId
	 *            the modifiedByUserId to set
	 */
	public void setModifiedByUserId(String modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}

	/**
	 * @return the modifiedByUserName
	 */
	public String getModifiedByUserName() {
		return modifiedByUserName;
	}

	/**
	 * @param modifiedByUserName
	 *            the modifiedByUserName to set
	 */
	public void setModifiedByUserName(String modifiedByUserName) {
		this.modifiedByUserName = modifiedByUserName;
	}

	/**
	 * @return the modifiedByOn
	 */
	public Date getModifiedByOn() {
		return modifiedByOn;
	}

	/**
	 * @param modifiedByOn
	 *            the modifiedByOn to set
	 */
	public void setModifiedByOn(Date modifiedByOn) {
		this.modifiedByOn = modifiedByOn;
	}

	/**
	 * @return the tenantId
	 */
	public int getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId
	 *            the tenantId to set
	 */
	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * @return the tenantName
	 */
	public String getTenantName() {
		return tenantName;
	}

	/**
	 * @param tenantName
	 *            the tenantName to set
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	
	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the coupons
	 */
	public List<Coupon> getCoupons() {
		return coupons;
	}

	/**
	 * @param coupons the coupons to set
	 */
	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}

	/**
	 * @return the hostAppName
	 */
	public String getHostAppName() {
		return hostAppName;
	}

	/**
	 * @param hostAppName the hostAppName to set
	 */
	public void setHostAppName(String hostAppName) {
		this.hostAppName = hostAppName;
	}

	/**
	 * @return the seqNum
	 */
	public Integer getSeqNum() {
		return seqNum;
	}

	/**
	 * @param seqNum the seqNum to set
	 */
	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

	/**
	 * @return the categories
	 */
	public List<Categories> getCategories() {
		return categories;
	}

	/**
	 * @param categories the categories to set
	 */
	public void setCategories(List<Categories> categories) {
		this.categories = categories;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Promotion [id=" + id + ", name=" + name + ", description="
				+ description + ", contents=" + contents + ", validFrom="
				+ validFrom + ", validTo=" + validTo + ", state=" + state
				+ ", isDeleted=" + isDeleted + ", coupons=" + coupons
				+ ", createdBy=" + createdBy + ", createdByUserId="
				+ createdByUserId + ", createdByUserName=" + createdByUserName
				+ ", createdByOn=" + createdByOn + ", modifiedByUserId="
				+ modifiedByUserId + ", modifiedByUserName="
				+ modifiedByUserName + ", modifiedByOn=" + modifiedByOn
				+ ", tenantId=" + tenantId + ", tenantName=" + tenantName
				+ ", hostAppName=" + hostAppName + ", seqNum=" + seqNum
				+ ", categories=" + categories + "]";
	}

	/**
	 * @return the contents
	 */
	public String[] getContents() {
		return contents;
	}

	/**
	 * @param contents the contents to set
	 */
	public void setContents(String[] contents) {
		this.contents = contents;
	}

	/**
	 * @return the likers
	 */
	public List<PromotionLikers> getLikers() {
		return likers;
	}

	/**
	 * @param likers the likers to set
	 */
	public void setLikers(List<PromotionLikers> likers) {
		this.likers = likers;
	}

	public Promotion(String name, String description, String[] contents,
			Date validFrom, Date validTo, State state, boolean isDeleted,
			List<Coupon> coupons, Long createdBy, String createdByUserId,
			String createdByUserName, Date createdByOn,
			String modifiedByUserId, String modifiedByUserName,
			Date modifiedByOn, int tenantId, String tenantName,
			String hostAppName, Integer seqNum, List<Categories> categories,
			List<PromotionLikers> likers) {
		super();
		this.name = name;
		this.description = description;
		this.contents = contents;
		this.validFrom = validFrom;
		this.validTo = validTo;
		this.state = state;
		this.isDeleted = isDeleted;
		this.coupons = coupons;
		this.createdBy = createdBy;
		this.createdByUserId = createdByUserId;
		this.createdByUserName = createdByUserName;
		this.createdByOn = createdByOn;
		this.modifiedByUserId = modifiedByUserId;
		this.modifiedByUserName = modifiedByUserName;
		this.modifiedByOn = modifiedByOn;
		this.tenantId = tenantId;
		this.tenantName = tenantName;
		this.hostAppName = hostAppName;
		this.seqNum = seqNum;
		this.categories = categories;
		this.likers = likers;
	}

	/**
	 * @return the promotionComments
	 */
	public List<PromotionComments> getPromotionComments() {
		return promotionComments;
	}

	/**
	 * @param promotionComments the promotionComments to set
	 */
	public void setPromotionComments(List<PromotionComments> promotionComments) {
		this.promotionComments = promotionComments;
	}
}