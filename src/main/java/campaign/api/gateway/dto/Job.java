package campaign.api.gateway.dto;

import java.util.Date;
import java.util.List;

public class Job {

	private Long id;
	private String name;
	private String jobType;
	private String executableServiceURL;
	private String executablePath;
	private List<Parameter> params;

	private String createdByUserId;
	private String createdByUserName;
	private Date createdByOn;
	private String modifiedByUserId;
	private String modifiedByUserName;
	private Date modifiedByOn;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the jobType
	 */
	public String getJobType() {
		return jobType;
	}

	/**
	 * @param jobType
	 *            the jobType to set
	 */
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	/**
	 * @return the executableServiceURL
	 */
	public String getExecutableServiceURL() {
		return executableServiceURL;
	}

	/**
	 * @param executableServiceURL
	 *            the executableServiceURL to set
	 */
	public void setExecutableServiceURL(String executableServiceURL) {
		this.executableServiceURL = executableServiceURL;
	}

	/**
	 * @return the executablePath
	 */
	public String getExecutablePath() {
		return executablePath;
	}

	/**
	 * @param executablePath
	 *            the executablePath to set
	 */
	public void setExecutablePath(String executablePath) {
		this.executablePath = executablePath;
	}

	/**
	 * @return the params
	 */
	public List<Parameter> getParams() {
		return params;
	}

	/**
	 * @param params
	 *            the params to set
	 */
	public void setParams(List<Parameter> params) {
		this.params = params;
	}

	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}

	/**
	 * @param createdByUserId
	 *            the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}

	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}

	/**
	 * @param createdByUserName
	 *            the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	/**
	 * @return the createdByOn
	 */
	public Date getCreatedByOn() {
		return createdByOn;
	}

	/**
	 * @param createdByOn
	 *            the createdByOn to set
	 */
	public void setCreatedByOn(Date createdByOn) {
		this.createdByOn = createdByOn;
	}

	/**
	 * @return the modifiedByUserId
	 */
	public String getModifiedByUserId() {
		return modifiedByUserId;
	}

	/**
	 * @param modifiedByUserId
	 *            the modifiedByUserId to set
	 */
	public void setModifiedByUserId(String modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}

	/**
	 * @return the modifiedByUserName
	 */
	public String getModifiedByUserName() {
		return modifiedByUserName;
	}

	/**
	 * @param modifiedByUserName
	 *            the modifiedByUserName to set
	 */
	public void setModifiedByUserName(String modifiedByUserName) {
		this.modifiedByUserName = modifiedByUserName;
	}

	/**
	 * @return the modifiedByOn
	 */
	public Date getModifiedByOn() {
		return modifiedByOn;
	}

	/**
	 * @param modifiedByOn
	 *            the modifiedByOn to set
	 */
	public void setModifiedByOn(Date modifiedByOn) {
		this.modifiedByOn = modifiedByOn;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 */
	public Job() {
	}

	/**
	 * @param name
	 * @param jobType
	 * @param executableServiceURL
	 * @param executablePath
	 * @param params
	 * @param createdByUserId
	 * @param createdByUserName
	 * @param createdByOn
	 * @param modifiedByUserId
	 * @param modifiedByUserName
	 * @param modifiedByOn
	 */
	public Job(String name, String jobType, String executableServiceURL,
			String executablePath, List<Parameter> params,
			String createdByUserId, String createdByUserName, Date createdByOn,
			String modifiedByUserId, String modifiedByUserName,
			Date modifiedByOn) {
		super();
		this.name = name;
		this.jobType = jobType;
		this.executableServiceURL = executableServiceURL;
		this.executablePath = executablePath;
		this.params = params;
		this.createdByUserId = createdByUserId;
		this.createdByUserName = createdByUserName;
		this.createdByOn = createdByOn;
		this.modifiedByUserId = modifiedByUserId;
		this.modifiedByUserName = modifiedByUserName;
		this.modifiedByOn = modifiedByOn;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Job [id=" + id + ", name=" + name + ", jobType=" + jobType
				+ ", executableServiceURL=" + executableServiceURL
				+ ", executablePath=" + executablePath + ", params=" + params
				+ ", createdByUserId=" + createdByUserId
				+ ", createdByUserName=" + createdByUserName + ", createdByOn="
				+ createdByOn + ", modifiedByUserId=" + modifiedByUserId
				+ ", modifiedByUserName=" + modifiedByUserName
				+ ", modifiedByOn=" + modifiedByOn + "]";
	}

}