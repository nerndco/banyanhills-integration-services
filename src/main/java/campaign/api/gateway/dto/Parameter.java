package campaign.api.gateway.dto;

import java.io.Serializable;

public class Parameter implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4263835258670212060L;

	private Integer id;
	private Integer seqNum;
	private String type;
	private String name;
	private String exprssion;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the seqNum
	 */
	public Integer getSeqNum() {
		return seqNum;
	}

	/**
	 * @param seqNum
	 *            the seqNum to set
	 */
	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the exprssion
	 */
	public String getExprssion() {
		return exprssion;
	}

	/**
	 * @param exprssion
	 *            the exprssion to set
	 */
	public void setExprssion(String exprssion) {
		this.exprssion = exprssion;
	}

	public Parameter() {

	}

	/**
	 * @param id
	 * @param seqNum
	 * @param type
	 * @param name
	 * @param exprssion
	 */
	public Parameter(Integer seqNum, String type, String name, String exprssion) {
		this.seqNum = seqNum;
		this.type = type;
		this.name = name;
		this.exprssion = exprssion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Parameter [id=" + id + ", seqNum=" + seqNum + ", type=" + type
				+ ", name=" + name + ", exprssion=" + exprssion + "]";
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}
