/**
 * 
 */
package campaign.api.gateway.dto;

import java.util.List;

import campaign.api.gateway.dto.segment.Segment;


/**
 * @author Kritika.Srivastava
 *
 */
public class Machine {
	private String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private String id;
	private String name;
	private List<Segment> segments;
	private Boolean isSubscriptiOn;
	private List<String> channelTypePreferences;
	private List<Integer> notInterestedInCampaingTypeRef;
	private List<Integer> notInterestedInPromotionTypeRef;

	private String sourceCMS;
	private Integer alertTimeout;
	private Integer auditingUntil;
	private String broadCastAddress;
	private String cidr;
	private String clientAddress;
	private Integer clientCode;
	private String clientType;
	private String clientVersion;
	private String currentLayout;
	private Integer currentLayoutId;
	private String defaultLayout;
	private Integer defaultLayoutId;
	private String description;
	private String display;
	private Integer referenceId;
	private Integer displayProfileId;
	private Integer displayGroupId;
	private Integer emailAlert;
	private Integer incSchedule;
	private Integer lastAccessed;
	private Integer lastChanged;
	private Integer lastCommandSuccess;
	private Integer lastWakeOnLanCommandSent;
	private Double latitude;
	private String license;
	private Integer licensed;
	private Integer loggedIn;
	private Double longitude;
	private String macAddress;
	private Integer mediaInventoryStatus;
	private Integer numberOfMacAddressChanges;
	private Integer screenShotRequested;
	private String secureOn;
	private Integer storageAvailableSpace;
	private Integer storageTotalSpace;
	private String versionInstructions;
	private Integer wakeOnLanEnabled;
	private String wakeOnLanTime;
	private String xmrChannel;
	private String xmrPubKey;
	private String serverKey;
	private String hardwareKey;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the segments
	 */
	public List<Segment> getSegments() {
		return segments;
	}
	/**
	 * @param segments the segments to set
	 */
	public void setSegments(List<Segment> segments) {
		this.segments = segments;
	}
	/**
	 * @return the isSubscriptiOn
	 */
	public Boolean getIsSubscriptiOn() {
		return isSubscriptiOn;
	}
	/**
	 * @param isSubscriptiOn the isSubscriptiOn to set
	 */
	public void setIsSubscriptiOn(Boolean isSubscriptiOn) {
		this.isSubscriptiOn = isSubscriptiOn;
	}
	/**
	 * @return the channelTypePreferences
	 */
	public List<String> getChannelTypePreferences() {
		return channelTypePreferences;
	}
	/**
	 * @param channelTypePreferences the channelTypePreferences to set
	 */
	public void setChannelTypePreferences(List<String> channelTypePreferences) {
		this.channelTypePreferences = channelTypePreferences;
	}
	/**
	 * @return the notInterestedInCampaingTypeRef
	 */
	public List<Integer> getNotInterestedInCampaingTypeRef() {
		return notInterestedInCampaingTypeRef;
	}
	/**
	 * @param notInterestedInCampaingTypeRef the notInterestedInCampaingTypeRef to set
	 */
	public void setNotInterestedInCampaingTypeRef(
			List<Integer> notInterestedInCampaingTypeRef) {
		this.notInterestedInCampaingTypeRef = notInterestedInCampaingTypeRef;
	}
	/**
	 * @return the notInterestedInPromotionTypeRef
	 */
	public List<Integer> getNotInterestedInPromotionTypeRef() {
		return notInterestedInPromotionTypeRef;
	}
	/**
	 * @param notInterestedInPromotionTypeRef the notInterestedInPromotionTypeRef to set
	 */
	public void setNotInterestedInPromotionTypeRef(
			List<Integer> notInterestedInPromotionTypeRef) {
		this.notInterestedInPromotionTypeRef = notInterestedInPromotionTypeRef;
	}
	/**
	 * @return the sourceCMS
	 */
	public String getSourceCMS() {
		return sourceCMS;
	}
	/**
	 * @param sourceCMS the sourceCMS to set
	 */
	public void setSourceCMS(String sourceCMS) {
		this.sourceCMS = sourceCMS;
	}
	/**
	 * @return the alertTimeout
	 */
	public Integer getAlertTimeout() {
		return alertTimeout;
	}
	/**
	 * @param alertTimeout the alertTimeout to set
	 */
	public void setAlertTimeout(Integer alertTimeout) {
		this.alertTimeout = alertTimeout;
	}
	/**
	 * @return the auditingUntil
	 */
	public Integer getAuditingUntil() {
		return auditingUntil;
	}
	/**
	 * @param auditingUntil the auditingUntil to set
	 */
	public void setAuditingUntil(Integer auditingUntil) {
		this.auditingUntil = auditingUntil;
	}
	/**
	 * @return the broadCastAddress
	 */
	public String getBroadCastAddress() {
		return broadCastAddress;
	}
	/**
	 * @param broadCastAddress the broadCastAddress to set
	 */
	public void setBroadCastAddress(String broadCastAddress) {
		this.broadCastAddress = broadCastAddress;
	}
	/**
	 * @return the cidr
	 */
	public String getCidr() {
		return cidr;
	}
	/**
	 * @param cidr the cidr to set
	 */
	public void setCidr(String cidr) {
		this.cidr = cidr;
	}
	/**
	 * @return the clientAddress
	 */
	public String getClientAddress() {
		return clientAddress;
	}
	/**
	 * @param clientAddress the clientAddress to set
	 */
	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}
	/**
	 * @return the clientCode
	 */
	public Integer getClientCode() {
		return clientCode;
	}
	/**
	 * @param clientCode the clientCode to set
	 */
	public void setClientCode(Integer clientCode) {
		this.clientCode = clientCode;
	}
	/**
	 * @return the clientType
	 */
	public String getClientType() {
		return clientType;
	}
	/**
	 * @param clientType the clientType to set
	 */
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	/**
	 * @return the clientVersion
	 */
	public String getClientVersion() {
		return clientVersion;
	}
	/**
	 * @param clientVersion the clientVersion to set
	 */
	public void setClientVersion(String clientVersion) {
		this.clientVersion = clientVersion;
	}
	/**
	 * @return the currentLayout
	 */
	public String getCurrentLayout() {
		return currentLayout;
	}
	/**
	 * @param currentLayout the currentLayout to set
	 */
	public void setCurrentLayout(String currentLayout) {
		this.currentLayout = currentLayout;
	}
	/**
	 * @return the currentLayoutId
	 */
	public Integer getCurrentLayoutId() {
		return currentLayoutId;
	}
	/**
	 * @param currentLayoutId the currentLayoutId to set
	 */
	public void setCurrentLayoutId(Integer currentLayoutId) {
		this.currentLayoutId = currentLayoutId;
	}
	/**
	 * @return the defaultLayout
	 */
	public String getDefaultLayout() {
		return defaultLayout;
	}
	/**
	 * @param defaultLayout the defaultLayout to set
	 */
	public void setDefaultLayout(String defaultLayout) {
		this.defaultLayout = defaultLayout;
	}
	/**
	 * @return the defaultLayoutId
	 */
	public Integer getDefaultLayoutId() {
		return defaultLayoutId;
	}
	/**
	 * @param defaultLayoutId the defaultLayoutId to set
	 */
	public void setDefaultLayoutId(Integer defaultLayoutId) {
		this.defaultLayoutId = defaultLayoutId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the display
	 */
	public String getDisplay() {
		return display;
	}
	/**
	 * @param display the display to set
	 */
	public void setDisplay(String display) {
		this.display = display;
	}
	/**
	 * @return the referenceId
	 */
	public Integer getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the displayProfileId
	 */
	public Integer getDisplayProfileId() {
		return displayProfileId;
	}
	/**
	 * @param displayProfileId the displayProfileId to set
	 */
	public void setDisplayProfileId(Integer displayProfileId) {
		this.displayProfileId = displayProfileId;
	}
	/**
	 * @return the displayGroupId
	 */
	public Integer getDisplayGroupId() {
		return displayGroupId;
	}
	/**
	 * @param displayGroupId the displayGroupId to set
	 */
	public void setDisplayGroupId(Integer displayGroupId) {
		this.displayGroupId = displayGroupId;
	}
	/**
	 * @return the emailAlert
	 */
	public Integer getEmailAlert() {
		return emailAlert;
	}
	/**
	 * @param emailAlert the emailAlert to set
	 */
	public void setEmailAlert(Integer emailAlert) {
		this.emailAlert = emailAlert;
	}
	/**
	 * @return the incSchedule
	 */
	public Integer getIncSchedule() {
		return incSchedule;
	}
	/**
	 * @param incSchedule the incSchedule to set
	 */
	public void setIncSchedule(Integer incSchedule) {
		this.incSchedule = incSchedule;
	}
	/**
	 * @return the lastAccessed
	 */
	public Integer getLastAccessed() {
		return lastAccessed;
	}
	/**
	 * @param lastAccessed the lastAccessed to set
	 */
	public void setLastAccessed(Integer lastAccessed) {
		this.lastAccessed = lastAccessed;
	}
	/**
	 * @return the lastChanged
	 */
	public Integer getLastChanged() {
		return lastChanged;
	}
	/**
	 * @param lastChanged the lastChanged to set
	 */
	public void setLastChanged(Integer lastChanged) {
		this.lastChanged = lastChanged;
	}
	/**
	 * @return the lastCommandSuccess
	 */
	public Integer getLastCommandSuccess() {
		return lastCommandSuccess;
	}
	/**
	 * @param lastCommandSuccess the lastCommandSuccess to set
	 */
	public void setLastCommandSuccess(Integer lastCommandSuccess) {
		this.lastCommandSuccess = lastCommandSuccess;
	}
	/**
	 * @return the lastWakeOnLanCommandSent
	 */
	public Integer getLastWakeOnLanCommandSent() {
		return lastWakeOnLanCommandSent;
	}
	/**
	 * @param lastWakeOnLanCommandSent the lastWakeOnLanCommandSent to set
	 */
	public void setLastWakeOnLanCommandSent(Integer lastWakeOnLanCommandSent) {
		this.lastWakeOnLanCommandSent = lastWakeOnLanCommandSent;
	}
	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the license
	 */
	public String getLicense() {
		return license;
	}
	/**
	 * @param license the license to set
	 */
	public void setLicense(String license) {
		this.license = license;
	}
	/**
	 * @return the licensed
	 */
	public Integer getLicensed() {
		return licensed;
	}
	/**
	 * @param licensed the licensed to set
	 */
	public void setLicensed(Integer licensed) {
		this.licensed = licensed;
	}
	/**
	 * @return the loggedIn
	 */
	public Integer getLoggedIn() {
		return loggedIn;
	}
	/**
	 * @param loggedIn the loggedIn to set
	 */
	public void setLoggedIn(Integer loggedIn) {
		this.loggedIn = loggedIn;
	}
	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the macAddress
	 */
	public String getMacAddress() {
		return macAddress;
	}
	/**
	 * @param macAddress the macAddress to set
	 */
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	/**
	 * @return the mediaInventoryStatus
	 */
	public Integer getMediaInventoryStatus() {
		return mediaInventoryStatus;
	}
	/**
	 * @param mediaInventoryStatus the mediaInventoryStatus to set
	 */
	public void setMediaInventoryStatus(Integer mediaInventoryStatus) {
		this.mediaInventoryStatus = mediaInventoryStatus;
	}
	/**
	 * @return the numberOfMacAddressChanges
	 */
	public Integer getNumberOfMacAddressChanges() {
		return numberOfMacAddressChanges;
	}
	/**
	 * @param numberOfMacAddressChanges the numberOfMacAddressChanges to set
	 */
	public void setNumberOfMacAddressChanges(Integer numberOfMacAddressChanges) {
		this.numberOfMacAddressChanges = numberOfMacAddressChanges;
	}
	/**
	 * @return the screenShotRequested
	 */
	public Integer getScreenShotRequested() {
		return screenShotRequested;
	}
	/**
	 * @param screenShotRequested the screenShotRequested to set
	 */
	public void setScreenShotRequested(Integer screenShotRequested) {
		this.screenShotRequested = screenShotRequested;
	}
	/**
	 * @return the secureOn
	 */
	public String getSecureOn() {
		return secureOn;
	}
	/**
	 * @param secureOn the secureOn to set
	 */
	public void setSecureOn(String secureOn) {
		this.secureOn = secureOn;
	}
	/**
	 * @return the storageAvailableSpace
	 */
	public Integer getStorageAvailableSpace() {
		return storageAvailableSpace;
	}
	/**
	 * @param storageAvailableSpace the storageAvailableSpace to set
	 */
	public void setStorageAvailableSpace(Integer storageAvailableSpace) {
		this.storageAvailableSpace = storageAvailableSpace;
	}
	/**
	 * @return the storageTotalSpace
	 */
	public Integer getStorageTotalSpace() {
		return storageTotalSpace;
	}
	/**
	 * @param storageTotalSpace the storageTotalSpace to set
	 */
	public void setStorageTotalSpace(Integer storageTotalSpace) {
		this.storageTotalSpace = storageTotalSpace;
	}
	/**
	 * @return the versionInstructions
	 */
	public String getVersionInstructions() {
		return versionInstructions;
	}
	/**
	 * @param versionInstructions the versionInstructions to set
	 */
	public void setVersionInstructions(String versionInstructions) {
		this.versionInstructions = versionInstructions;
	}
	/**
	 * @return the wakeOnLanEnabled
	 */
	public Integer getWakeOnLanEnabled() {
		return wakeOnLanEnabled;
	}
	/**
	 * @param wakeOnLanEnabled the wakeOnLanEnabled to set
	 */
	public void setWakeOnLanEnabled(Integer wakeOnLanEnabled) {
		this.wakeOnLanEnabled = wakeOnLanEnabled;
	}
	/**
	 * @return the wakeOnLanTime
	 */
	public String getWakeOnLanTime() {
		return wakeOnLanTime;
	}
	/**
	 * @param wakeOnLanTime the wakeOnLanTime to set
	 */
	public void setWakeOnLanTime(String wakeOnLanTime) {
		this.wakeOnLanTime = wakeOnLanTime;
	}
	/**
	 * @return the xmrChannel
	 */
	public String getXmrChannel() {
		return xmrChannel;
	}
	/**
	 * @param xmrChannel the xmrChannel to set
	 */
	public void setXmrChannel(String xmrChannel) {
		this.xmrChannel = xmrChannel;
	}
	/**
	 * @return the xmrPubKey
	 */
	public String getXmrPubKey() {
		return xmrPubKey;
	}
	/**
	 * @param xmrPubKey the xmrPubKey to set
	 */
	public void setXmrPubKey(String xmrPubKey) {
		this.xmrPubKey = xmrPubKey;
	}
	public String getServerKey() {
		return serverKey;
	}
	public void setServerKey(String serverKey) {
		this.serverKey = serverKey;
	}
	public String getHardwareKey() {
		return hardwareKey;
	}
	public void setHardwareKey(String hardwareKey) {
		this.hardwareKey = hardwareKey;
	}

	
}
