/**
 * 
 */
package campaign.api.gateway.dto.campaign;

import java.util.Date;
import java.util.List;

import campaign.api.gateway.dto.ApproverMapping;
import campaign.api.gateway.dto.CampaignCategory;
import campaign.api.gateway.dto.PromotionMapping;
import campaign.api.gateway.dto.State;
import campaign.api.gateway.dto.xibo.Layout;

/**
 * @author Kritika.Srivastava
 *
 */
public class CreateCampaignData {
	private Long id;
	private String name;
	private String description;
	private Date startDate;
	private Date endDate;
	private State state;
	private List<ApproverMapping> approverMappings;
	private String campaignLogoURL;
	private boolean isDeleted;
	String createdByUserId;
	String createdByUserName;
	Date createdByOn;
	String modifiedByUserId;
	String modifiedByUserName;
	Date modifiedByOn;
	private String tenantName;
	private String hostAppName;
	private List<CampaignCategory> campaignCategories;
	private List<PromotionMapping> promotionMappings;
	private Layout layout;
	public Layout getLayout() {
		return layout;
	}
	public void setLayout(Layout layout) {
		this.layout = layout;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}
	/**
	 * @return the approverMappings
	 */
	public List<ApproverMapping> getApproverMappings() {
		return approverMappings;
	}
	/**
	 * @param approverMappings the approverMappings to set
	 */
	public void setApproverMappings(List<ApproverMapping> approverMappings) {
		this.approverMappings = approverMappings;
	}
	/**
	 * @return the campaignLogoURL
	 */
	public String getCampaignLogoURL() {
		return campaignLogoURL;
	}
	/**
	 * @param campaignLogoURL the campaignLogoURL to set
	 */
	public void setCampaignLogoURL(String campaignLogoURL) {
		this.campaignLogoURL = campaignLogoURL;
	}
	/**
	 * @return the isDeleted
	 */
	public boolean isDeleted() {
		return isDeleted;
	}
	/**
	 * @param isDeleted the isDeleted to set
	 */
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	/**
	 * @return the createdByUserId
	 */
	public String getCreatedByUserId() {
		return createdByUserId;
	}
	/**
	 * @param createdByUserId the createdByUserId to set
	 */
	public void setCreatedByUserId(String createdByUserId) {
		this.createdByUserId = createdByUserId;
	}
	/**
	 * @return the createdByUserName
	 */
	public String getCreatedByUserName() {
		return createdByUserName;
	}
	/**
	 * @param createdByUserName the createdByUserName to set
	 */
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}
	/**
	 * @return the createdByOn
	 */
	public Date getCreatedByOn() {
		return createdByOn;
	}
	/**
	 * @param createdByOn the createdByOn to set
	 */
	public void setCreatedByOn(Date createdByOn) {
		this.createdByOn = createdByOn;
	}
	/**
	 * @return the modifiedByUserId
	 */
	public String getModifiedByUserId() {
		return modifiedByUserId;
	}
	/**
	 * @param modifiedByUserId the modifiedByUserId to set
	 */
	public void setModifiedByUserId(String modifiedByUserId) {
		this.modifiedByUserId = modifiedByUserId;
	}
	/**
	 * @return the modifiedByUserName
	 */
	public String getModifiedByUserName() {
		return modifiedByUserName;
	}
	/**
	 * @param modifiedByUserName the modifiedByUserName to set
	 */
	public void setModifiedByUserName(String modifiedByUserName) {
		this.modifiedByUserName = modifiedByUserName;
	}
	/**
	 * @return the modifiedByOn
	 */
	public Date getModifiedByOn() {
		return modifiedByOn;
	}
	/**
	 * @param modifiedByOn the modifiedByOn to set
	 */
	public void setModifiedByOn(Date modifiedByOn) {
		this.modifiedByOn = modifiedByOn;
	}

	/**
	 * @return the tenantName
	 */
	public String getTenantName() {
		return tenantName;
	}
	/**
	 * @param tenantName the tenantName to set
	 */
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	/**
	 * @return the hostAppName
	 */
	public String getHostAppName() {
		return hostAppName;
	}
	/**
	 * @param hostAppName the hostAppName to set
	 */
	public void setHostAppName(String hostAppName) {
		this.hostAppName = hostAppName;
	}
	/**
	 * @return the campaignCategories
	 */
	public List<CampaignCategory> getCampaignCategories() {
		return campaignCategories;
	}
	/**
	 * @param campaignCategories the campaignCategories to set
	 */
	public void setCampaignCategories(List<CampaignCategory> campaignCategories) {
		this.campaignCategories = campaignCategories;
	}
	/**
	 * @return the promotionMappings
	 */
	public List<PromotionMapping> getPromotionMappings() {
		return promotionMappings;
	}
	/**
	 * @param promotionMappings the promotionMappings to set
	 */
	public void setPromotionMappings(List<PromotionMapping> promotionMappings) {
		this.promotionMappings = promotionMappings;
	}
}
