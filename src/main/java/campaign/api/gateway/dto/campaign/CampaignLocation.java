package campaign.api.gateway.dto.campaign;

import java.io.Serializable;
import java.util.Date;

public class CampaignLocation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 348742287093237260L;
	private String id;
	private Long campaignRefId;
	private Location position;
	
	private Double radius;
	private Date startDate;
	private Date endDate;
	private Integer status;
	/**
	 * 
	 */
	public CampaignLocation() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param campaignRefId
	 * @param position
	 * @param radius
	 * @param startDate
	 * @param endDate
	 * @param status
	 * @param deliverySegmentId
	 */
	public CampaignLocation(Long campaignRefId, Location position,
			Double radius, Date startDate, Date endDate, Integer status) {
		super();
		this.campaignRefId = campaignRefId;
		this.position = position;
		this.radius = radius;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = status;
	}
	/**
	 * @return the campaignRefId
	 */
	public Long getCampaignRefId() {
		return campaignRefId;
	}
	/**
	 * @param campaignRefId the campaignRefId to set
	 */
	public void setCampaignRefId(Long campaignRefId) {
		this.campaignRefId = campaignRefId;
	}
	/**
	 * @return the position
	 */
	public Location getPosition() {
		return position;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(Location position) {
		this.position = position;
	}
	/**
	 * @return the radius
	 */
	public Double getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(Double radius) {
		this.radius = radius;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
}
