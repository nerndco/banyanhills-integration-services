/**
 * 
 */
package campaign.api.gateway.dto.campaign;


/**
 * @author Kritika.Srivastava
 *
 */
public class DeleteCampaignResponse {
	private String response;
	private String message;
	/**
	 * @return the id
	 */
	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}
	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	}
