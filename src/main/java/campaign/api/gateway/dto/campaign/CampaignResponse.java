package campaign.api.gateway.dto.campaign;

import java.util.List;

import campaign.api.gateway.dto.DistributionInfo;

public class CampaignResponse {
	private Long campaignId;
	private Long statusCode;
	private String messgae;
	private List<DistributionInfo> distributionInfoId;
	/**
	 * @return the campaignId
	 */
	public Long getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the statusCode
	 */
	public Long getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Long statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the messgae
	 */
	public String getMessgae() {
		return messgae;
	}
	/**
	 * @param messgae the messgae to set
	 */
	public void setMessgae(String messgae) {
		this.messgae = messgae;
	}
	/**
	 * @return the distributionInfoId
	 */
	public List<DistributionInfo> getDistributionInfoId() {
		return distributionInfoId;
	}
	/**
	 * @param distributionInfoId the distributionInfoId to set
	 */
	public void setDistributionInfoId(List<DistributionInfo> distributionInfoId) {
		this.distributionInfoId = distributionInfoId;
	}
}
