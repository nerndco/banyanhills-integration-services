/**
 * 
 */
package campaign.api.gateway.dto.campaign;

import java.util.List;

import campaign.api.gateway.dto.Campaign;
import campaign.api.gateway.dto.DistributionInfo;

/**
 * @author Kritika.Srivastava
 *
 */
public class CreateCampaignRequest {
	CreateCampaignData campaign;
	List<DistributionInfo> distributionInfo;
	/**
	 * @return the campaign
	 */
	public CreateCampaignData getCampaign() {
		return campaign;
	}
	public CreateCampaignRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param campaign the campaign to set
	 */
	public void setCampaign(CreateCampaignData campaign) {
		this.campaign = campaign;
	}
	/**
	 * @return the distributionInfo
	 */
	public List<DistributionInfo> getDistributionInfo() {
		return distributionInfo;
	}
	/**
	 * @param distributionInfo the distributionInfo to set
	 */
	public void setDistributionInfo(List<DistributionInfo> distributionInfo) {
		this.distributionInfo = distributionInfo;
	}
	public CreateCampaignRequest(CreateCampaignData campaign,
			List<DistributionInfo> distributionInfo) {
		super();
		this.campaign = campaign;
		this.distributionInfo = distributionInfo;
	}
}
