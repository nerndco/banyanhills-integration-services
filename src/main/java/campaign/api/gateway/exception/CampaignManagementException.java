package campaign.api.gateway.exception;


public class CampaignManagementException extends Exception {

	private static final long serialVersionUID = 1L;

	public CampaignManagementException(String message) {
		super(message);
	}

	public CampaignManagementException(String message, Throwable t) {
		super(message, t);
	}

}
