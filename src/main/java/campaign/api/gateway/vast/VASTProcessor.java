package campaign.api.gateway.vast;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class VASTProcessor {
	static final Logger logger = LoggerFactory.getLogger(VASTProcessor.class);
	
	public  String readURLContent(String urlStr) throws IOException{
		URL url;
		BufferedReader br = null;
		BufferedWriter bw = null;
		StringBuilder response = new StringBuilder();
		try {
			// get URL content
			url = new URL(urlStr);
			URLConnection conn = url.openConnection();

			// open the stream and put it into BufferedReader
			br = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));

			String inputLine = null;

			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							conn.getInputStream()));

			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
			}		
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		finally{
			if(bw != null){
				bw.close();
			}
			if(br != null){
				br.close();
			}
		}
		return response!=null?response.toString():null;
	}


	public Map<String,Object> processVastUrl(String url) throws JAXBException, IOException, XMLStreamException, InterruptedException {
		String xmlResponse = readURLContent(url);
		JAXBContext jaxbContext = JAXBContext.newInstance(VAST.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		StringReader stringReader = new StringReader(xmlResponse);

		XMLInputFactory xif = XMLInputFactory.newInstance();
		XMLStreamReader xsr = xif.createXMLStreamReader(stringReader);
		VAST que = null;
		String media = null;
		if(xsr!=null){
			que = (VAST) unmarshaller.unmarshal(xsr);  
			logger.debug("Media Files Value"+que.ad.inLine.creatives.creative.linear.mediaFiles.getMediaFile().value);
			media = que.ad.inLine.creatives.creative.linear.mediaFiles.getMediaFile().value;
		}
		else{
			media = url;
		}

		URL _url = new URL(media);
		ReadableByteChannel rbc = Channels.newChannel(_url.openStream());

		String rootPath = System.getProperty("catalina.home");
		String file = extractFileName(media);

		FileOutputStream baos = new FileOutputStream(rootPath + "/" + file);
		baos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		baos.close();

		Map<String,Object> map = new HashMap<String,Object>();
		//		map.put("content_type", que.ad.inLine.creatives.creative.linear.mediaFiles.getMediaFile().type);
		map.put("url", _url);
		map.put("fileName", file);
		map.put("vast", que);
		return map;
	}

	public String sendGet(String url) throws Exception {
		HttpURLConnection con = null;
		BufferedReader in = null;
		StringBuffer response = null;
		try{
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			logger.info("\nSending 'GET' request to URL : " + url);
			logger.info("Response Code : " + responseCode);


			in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		}catch(IOException e){
			e.printStackTrace();
			logger.error("Error in sendGet",e);
		}
		finally{
			if(in!=null){
				in.close();
			}
		}
		if(response!=null){
			logger.info("response recieved: "+response);
		}else{
			logger.info("response is null");
		}
		return response!=null?response.toString():null;	
	}



	private String extractFileName(String str){
		String fileName = str;
		if(str != null){
			String[] tokens = str.split("[/]");
			if(tokens!=null && tokens.length > 0){
				fileName = tokens[tokens.length-1];
				if(fileName!=null){
					fileName = fileName.trim();
				}
			}
		}
		return fileName;
	}
}


